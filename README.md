# Protein Domain Neighborhood Construction and pattern recognition

This application constructs operon-like structures around one or more protein domains from genomic data and discovers patterns in the present protein domains. Genome annotation files are expected to be provided in HDT format, which can be obtained using [SAPP](https://www.ncbi.nlm.nih.gov/pubmed/29186322) (1). The application can perform Frequent Pattern Mining and Latent Dirichlet Allocation (LDA) Clustering .

Frequent Pattern mining is applied to the domain list of the operons to return a list of protein domains that are frequently present in the operons. The minimum support value for a pattern to be considered frequent is a parameter (when a domain is present in 3 out of 5 operons the support value is 0.6).

The operons can be clustered using Latent Dirichlet Allocation, to find whether the operons surrounding the domains of interest could represent multiple biological functions. LDA looks for hidden 'topics' is in the operons based on the words / protein domains used. The number of expected clusters needs to be provided and the algorithm will cluster the operons based on the domain content. The domains will be assigned a weight for how important they are in for a topic.

To select the vocabulary / protein domains on which will be used for clustering, either the domains found by the frequent pattern mining or by performing the LDA twice. Here the second time the top *parameter x* most important domains per topic will be selected.  

More detailed description and explanation of the code can be found at [code set up file](DocumentationFiles/Code set up.md).



## References

1. Jasper J Koehorst, Jesse C J van Dam, Edoardo Saccenti, Vitor A P Martins dos Santos, Maria Suarez-Diez, Peter J Schaap, SAPP: functional genome annotation and analysis through a semantic framework using FAIR principles, *Bioinformatics*, Volume 34, Issue 8, 15 April 2018, Pages 1401–1403, https://doi.org/10.1093/bioinformatics/btx767






# iRODS stuff

irods_user_name - ...
irods_zone_name - tempZone
irods_port - 1247
irods_host - wurk-icat.irods.surfsara.nl