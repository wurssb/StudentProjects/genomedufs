package nl.wur.ssb.DomainPatternRecognition.sparql;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.RegionExtractor;
import nl.wur.ssb.DomainPatternRecognition.Util;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.objects.Gene;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.objects.Region;
import org.apache.commons.io.FileUtils;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;
import scala.Tuple2;
import scala.Tuple6;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Pattern;

public class InputQuerier {
    private static final Logger LOGGER = Logger.getLogger(InputQuerier.class.getName());
    private JavaSparkContext sc;
    private CommandOptions commandOptions;
    private HashMap<String, HashMap<String, String>> domainFrequencies = new HashMap<>();
    private File domainFrequencyHashMapFile;

    private static String domainToQuery;
    private static boolean queryThoroughly;
    private static boolean operonHashMapPresent = false;
    private static boolean domainFrequenciesPresent = false;
    private static String regionDirectory = "";
    private static Tuple6<Boolean, Integer, Integer, Integer, String, String> operonVariables;
    private static String regionQuery;
    private static String graph;
    private static List<String> otherDomains;
    private static HashSet<String> excludedDomains;


    /**
     * Root function for the query interface
     * @param domain protein domain of interest
     * @param queryThoroughly only operons or also jaccard index, copy number etc...
     * @param sc spark interface
     * @param commandOptions command line arguments
     * @param regionDir working directory for the regions
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public InputQuerier(String domain, boolean queryThoroughly, JavaSparkContext sc, CommandOptions commandOptions, String regionDir) throws IOException, NoSuchAlgorithmException {
        InputQuerier.queryThoroughly = queryThoroughly;
        InputQuerier.domainToQuery = domain;
        InputQuerier.otherDomains = commandOptions.additionalDomains;
        InputQuerier.excludedDomains = new HashSet<>(commandOptions.excludedDomains);
        this.sc = sc;
        this.commandOptions = commandOptions;
        InputQuerier.operonVariables = new Tuple6<>(
                commandOptions.bidirectional,
                commandOptions.geneGap,
                commandOptions.bidirectionalGapMax,
                commandOptions.bidirectionalGapMin,
                commandOptions.db,
                commandOptions.originProv);
        InputQuerier.regionDirectory = regionDir;
        domainFrequencyHashMapFile = generateDomainFrequenciesName();
    }

    /**
     * The main method that guides the querying of the input data and returns a HashMap with Operons
     *
     * @return HashMap with keys the initial gene of the operon and an Operon object as value
     * @throws Exception among others: file not found, and NoSuchAlgorithm
     */
    public HashMap<String, Operon> queryInputData() throws Exception {
        // Get the necessary SPARQL query
        regionQuery = createQueryFromFile("queries/GenesAroundDomain.sparql", commandOptions.db,
                domainToQuery, commandOptions.width, commandOptions.originProv);

        // Get all the files that need to be queried
        JavaRDD<File> files = getAllInputFiles();

        // Create the right file names for the operon hashmap and domain frequencies hashmap
        // These names are dependent on the input data and search parameters.
        HashMap<String, Operon> operonHashMap;
        String operonHashMapFileName = DomainPatternRecognition.generateHashedOperonHashMapName(commandOptions, domainToQuery);
        File operonHashMapFile = new File(regionDirectory + "/" + operonHashMapFileName);
        File domainFrequencyHashMapFile = generateDomainFrequenciesName();

        // There are four options of which data is already available from previous runs

        if (operonHashMapFile.exists() && domainFrequencyHashMapFile.exists()) {
            // All the necessary data to run the rest of the code is already present. Just loading things in.
            LOGGER.info("Operons already extracted for this input data. Operons loaded from: " + operonHashMapFile.getAbsolutePath());
            LOGGER.info("All input data already queried for frequencies of domains, loaded from: " + domainFrequencyHashMapFile.getAbsolutePath());

            operonHashMap = deserializeOperonHashMap(operonHashMapFile);
            // Set global operonRDD
            DomainPatternRecognition.operonRDD = sc.parallelize(new ArrayList<>(operonHashMap.values()));

            operonHashMapPresent = true;
            domainFrequenciesPresent = true;
            domainFrequencies = deserializeDomainFrequencyHashMap(domainFrequencyHashMapFile);


        } else if (!operonHashMapFile.exists() && domainFrequencyHashMapFile.exists()) {
            LOGGER.info("All input data already queried for frequencies of domains, loaded from: " + domainFrequencyHashMapFile.getAbsolutePath());
            LOGGER.info("Querying the input data to extract operons");
            // Only need to extract the operons
            domainFrequencies = deserializeDomainFrequencyHashMap(domainFrequencyHashMapFile);
            domainFrequenciesPresent = true;
            JavaRDD<Operon> operonJavaRDD = files.flatMap(InputQuerier::queryOneInputFile);

            // Set global operonRDD
            DomainPatternRecognition.operonRDD = operonJavaRDD;

            operonHashMap = new HashMap<>(operonJavaRDD.mapToPair(operon -> new Tuple2<>(operon.getInitialGene().getGeneID(), operon)).collectAsMap());
            serializeOperonHashMap(domainToQuery, operonHashMap);

        } else if (operonHashMapFile.exists() && !domainFrequencyHashMapFile.exists()) {
            LOGGER.info("Operons already extracted for this input data. Operons loaded from: " + operonHashMapFile.getAbsolutePath());
            LOGGER.info("Counting frequencies of domains in all input data");
            operonHashMap = deserializeOperonHashMap(operonHashMapFile);
            operonHashMapPresent = true;

            // Now we need to trigger these lazy operations to acutally work.
            JavaRDD<Operon> operonJavaRDD = files.flatMap(InputQuerier::queryOneInputFile);
            // Set global operonRDD
            DomainPatternRecognition.operonRDD = operonJavaRDD;

            if (operonJavaRDD.isEmpty()) {
                LOGGER.info("Finished querying input data");
            }
            calculateDomainFrequencies(files);

        } else { // Everything needs to be done from scratch
            LOGGER.info("Querying the input files. This will take some time.");
            JavaRDD<Operon> operonJavaRDD = files.flatMap(InputQuerier::queryOneInputFile);
            // Set global operonRDD
            DomainPatternRecognition.operonRDD = operonJavaRDD;

            // key = initial gene id, value = operon object
            operonHashMap = new HashMap<>(operonJavaRDD.mapToPair(operon -> new Tuple2<>(operon.getInitialGene().getGeneID(), operon)).collectAsMap());
            serializeOperonHashMap(domainToQuery, operonHashMap);
            calculateDomainFrequencies(files);
        }
        return operonHashMap;
    }

    /**
     * Method to sum up how often a domain occurs in all the input data
     *
     * @param files RDD with all the input files
     * @throws IOException              when reading in the query output
     * @throws NoSuchAlgorithmException for creating hashed file names of operon HashMap and domain frequency hashmap
     */
    private void calculateDomainFrequencies(JavaRDD<File> files) throws IOException, NoSuchAlgorithmException {
        LOGGER.info("Calculating domain frequencies");
        JavaPairRDD<String, HashMap<String, String>> domainFrequenciesRDD = files.flatMapToPair(InputQuerier::getFrequenciesPerDomain);

        // Merge the domain frequency hashmaps from every file into one big one.
        // JavaPairRDD<StringDomain, HashMap<StringKey(columnName), String(Value,DomainCount/OrganisnName/...)>>
        // TODO create local protein domain object containing the domain and its information
        JavaPairRDD<String, HashMap<String, String>> mergingDomainFrequenciesRDD = domainFrequenciesRDD.reduceByKey(InputQuerier::reduceHashMaps);
        JavaPairRDD<String, HashMap<String, String>> mergedDomainFrequenciesRDD = mergingDomainFrequenciesRDD.mapToPair(InputQuerier::appendAllKeys);

        // todo: this step below needs to be optimized!!!

        Iterator<Tuple2<String, HashMap<String, String>>> mergedDomainIterator = mergedDomainFrequenciesRDD.toLocalIterator();
        while (mergedDomainIterator.hasNext()) {
            Tuple2<String, HashMap<String, String>> domainInfo = mergedDomainIterator.next();

            domainFrequencies.put(domainInfo._1, domainInfo._2);
        }

//        domainFrequencies = new HashMap<>(mergedDomainFrequenciesRDD.collectAsMap()); // todo: this collection is going wrong
        // todo: the task is to big for a collect, so I should do this a bit differently now it's fixed by the step above
        //  but that one is definitely not ideal.
        serializeDomainFrequencies();
    }


    /**
     * this is the method to perform the queries on one file
     *
     * @param file .hdt file
     * @return Iterator with the operons from every genome file
     */
    private static Iterator<Operon> queryOneInputFile(File file) throws Exception {

        // this is the file where the regions from this file will be saved and might already be saved
        File fileRegionsInGenome = new File(regionDirectory + "/regions_" + file.getName() + ".tsv");
        File fileGenomeLookUp = new File(DomainPatternRecognition.lookUpDirectory + "/" + file.getName() + ".tsv");

        HashSet<Operon> operons;

        try {
            if (!operonHashMapPresent) {
                // These are the resources needed to query.
                // todo: here a check can be build in. When the RegionInGenome file already exists and the GenomeLookUp
                //  is also not necessary, the HDT stuff should have to be mounted.
                HDT hdt = HDTManager.loadIndexedHDT(file.getAbsolutePath(), null);
                HDTGraph graph = new HDTGraph(hdt);
                Model model = ModelFactory.createModelForGraph(graph);

                operons = getOperons(fileRegionsInGenome, model);

                // Get the domain frequencies over all if that file does not yet exist.
                if (queryThoroughly && !fileGenomeLookUp.exists()) {
                    queryGenomeLookUpFile(model, fileGenomeLookUp);
                }
                // Free up the resources once the query is done.
                model.close();
                graph.close();
                hdt.close();
                return operons.iterator();
            } else {
                // The operon hashmap does exist. So, the regions don't have to be queried, but still need to check
                // whether the domain frequencies are present
                if (queryThoroughly && !fileGenomeLookUp.exists()) {
                    // These are the resources needed to query.
                    HDT hdt = HDTManager.loadIndexedHDT(file.getAbsolutePath(), null);
                    HDTGraph graph = new HDTGraph(hdt);
                    Model model = ModelFactory.createModelForGraph(graph);

                    queryGenomeLookUpFile(model, fileGenomeLookUp);

                    model.close();
                    graph.close();
                    hdt.close();
                }
            }
        } catch (IllegalArgumentException e) {
            File errorsFile = new File(DomainPatternRecognition.outputPath + "/Unprocessed_genomes.txt");

            FileWriter fileWriter = new FileWriter(errorsFile);
            fileWriter.write(file.getAbsolutePath() + "/n");
            fileWriter.close();
        }

        // Return an empty iterator of operons
        operons = new HashSet<>();
        return operons.iterator();
    }


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~ METHOD TO GET THE OPERONS FROM THE REGIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    /**
     * Method to the operons from one file
     *
     * @param fileRegionsInGenome file containing the results of the extract regions queries
     * @param model               hdt model
     * @return HashSet of Operons
     */
    private static HashSet<Operon> getOperons(File fileRegionsInGenome, Model model) throws Exception {
        HashSet<Operon> operons = new HashSet<>();
        createRegionQueryResults(fileRegionsInGenome, model);
        HashMap<String, Region> regionsInGenome = processRegionsToHashMap(fileRegionsInGenome);
        HashMap<String, Operon> operonsFromGenome = extractOperons(regionsInGenome);
        HashMap<String, Operon> operonHashMap = extractDomainsFromOperons(model, operonsFromGenome);
        operons.addAll(operonHashMap.values());
        return operons;
    }

    /**
     * A method to perform the SPARQL query that will get the raw query output which will give the regions
     *
     * @param fileRegionsInGenome file object to which the query output will be written
     * @param model               the hdt model on which the queries are based.
     */
    private static void createRegionQueryResults(File fileRegionsInGenome, Model model) {
        if (!fileRegionsInGenome.exists()) {
            PrintStream outputStream = null;
            try {
                outputStream = new PrintStream(fileRegionsInGenome);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Query query = QueryFactory.create(regionQuery);
            QueryExecution qe = QueryExecutionFactory.create(query, model);
            ResultSet results = qe.execSelect();

            setFormat("tsv", outputStream, results);
        }
    }

    /**
     * Method to process the results from GenesAroundDomain.sparql to put it into the region hashmap.
     *
     * @param fileRegionsInGenome file containing the results of the query
     * @throws FileNotFoundException -> from the Scanner object
     */
    private static HashMap<String, Region> processRegionsToHashMap(File fileRegionsInGenome) throws IOException {
        HashMap<String, Region> regionHashMap = new HashMap<>();
        Scanner scanner = new Scanner(fileRegionsInGenome);

        if (scanner.hasNext()) {
            scanner.nextLine(); // skip the first line with the parameter definition.
        }

        while (scanner.hasNextLine()) {
            String resultLine = scanner.nextLine();
            String[] line = resultLine.split("\t");

            if (!regionHashMap.containsKey(line[0])) { // initiate a region around the seed gene with domain of interest
                Gene seedGene = new Gene(line[0]);
                seedGene.addGeneLocationInfo(line[2], Integer.parseInt(line[4]), Integer.parseInt(line[6]));
                Region region = new Region(seedGene, line[8], line[9]);
                region.setScientificName(line[10]);
                regionHashMap.put(line[0], region);
            }

            // Add the neighbouring gene to the region.
            Region region = regionHashMap.get(line[0]);
            Gene neighbourGene = new Gene(line[1]);
            neighbourGene.addGeneLocationInfo(line[3], Integer.parseInt(line[5]), Integer.parseInt(line[7]));
            region.addGene(neighbourGene);
            regionHashMap.put(line[0], region);
        }
        scanner.close();
        // Delete the raw query output of the files.

        return regionHashMap;
    }

    /**
     * Method to guide the extraction of the operons from the regions
     *
     * @param regionHashMap A HashMap containing the raw regions
     * @return HashMap with String keys and Operon values.
     */
    private static HashMap<String, Operon> extractOperons(HashMap<String, Region> regionHashMap) {
        RegionExtractor regionExtractor = new RegionExtractor(operonVariables._1(), operonVariables._2(), operonVariables._3(), operonVariables._4());
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();
        regionExtractor.mergeOverlappingOperons();
        return regionExtractor.getOperonHashMap();
    }

    /**
     * Method to get the domains from every operon and store in the operon hashmap
     *
     * @param model apache jena model for querying the data.
     * @throws Exception due to querying.
     */
    private static HashMap<String, Operon> extractDomainsFromOperons(Model model, HashMap<String, Operon> operonsInGenome) throws Exception {
        HashSet<String> keysToRemove = new HashSet<>();

        for (String operonKey : operonsInGenome.keySet()) {
            Operon operon = operonsInGenome.get(operonKey);
            for (Gene geneInOperon : operon.getGenes()) {

                String geneID = geneInOperon.getGeneID();
                String sparqlQuery = createQueryFromFile("queries/ProteinDomainsInGene.sparql", geneID, operonVariables._5());

                Query query = QueryFactory.create(sparqlQuery);
                QueryExecution qe = QueryExecutionFactory.create(query, model);
                ResultSet results = qe.execSelect();

                while (results.hasNext()) {
                    QuerySolution querySolution = results.next();
                    operon.getDomains().add(String.valueOf(querySolution.get("PfamID")));
                }
            }
            HashSet<String> uniqueOperonDomains = new HashSet<>(operon.getDomains());
            // Check to see whether there are also additional domains that need to be in the operon.
            if (otherDomains.size() != 0) {
                // HashSet look ups have a better time complexity that the arraylist look up.
//                HashSet<String> operonDomains = new HashSet<>(operon.getDomains());
                HashSet<String> additionalDomains = new HashSet<>(otherDomains);
                // Remove the operon from the list if it does not contain all the additional domains
                if (!uniqueOperonDomains.containsAll(additionalDomains)) {
                    keysToRemove.add(operonKey);
                }
            }

            if (excludedDomains.size() != 0) {
//                HashSet<String> operonDomains = new HashSet<>(operon.getDomains());
                // remove any operons that contain one of the exluded domains.
                for (String excludedDomain : excludedDomains) {
                    if (uniqueOperonDomains.contains(excludedDomain)) {
                        keysToRemove.add(operonKey);
                    }
                }
            }

//            String domainsString = StringUtils.join(uniqueOperonDomains, " ");
            operon.setUniqueDomainString(uniqueOperonDomains);
        }
        operonsInGenome.keySet().removeAll(keysToRemove);
        return operonsInGenome;
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~`METHOD TO GET THE FREQUENCIES OF ALL DOMAINS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Method to create the look up files / hashmaps that contain the domain descriptions and counts how often a
     * domain occurs within the regions and surrounding the regions etc.
     *
     * @param model Used for the querying.
     */
    private static void queryGenomeLookUpFile(Model model, File output) throws Exception {
        PrintStream outputStream = null;
        try {
            outputStream = new PrintStream(output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String sparqlQuery = createQueryFromFile("queries/QueryAllDomainInfoForGenome.sparql", operonVariables._5(), operonVariables._6());
        // _5 is the dataBase to look into _6 is the provenance of origin to look into.

        org.apache.jena.query.Query query = QueryFactory.create(sparqlQuery);
        QueryExecution qe = QueryExecutionFactory.create(query, model);
        ResultSet results = qe.execSelect();


        // Output query results
        setFormat("tsv", outputStream, results); // here the file is written.

        // Important - free up resources used running the query
        qe.close();
        outputStream.close();

    }


    /**
     * A method to make sure that all the keys are present for every domain in the domain frequency hashmap
     *
     * @param stringHashMapTuple2
     * @return
     */
    private static Tuple2<String, HashMap<String, String>> appendAllKeys(Tuple2<String, HashMap<String, String>> stringHashMapTuple2) {
        HashSet<String> keysToMerge = new HashSet<>(Arrays.asList("domainCountWith", "domainCountWithout", "sampleOrganismWith", "sampleOrganismWithout"));

        for (String key : keysToMerge) {
            if (!stringHashMapTuple2._2.containsKey(key)) { // The key is not in the hashmap
                if (key.startsWith("sample")) {
                    stringHashMapTuple2._2.put(key, ""); // add an empty string
                } else { // the key is a domain count
                    stringHashMapTuple2._2.put(key, "0");
                }
            }
        }
        return stringHashMapTuple2;
    }

    /**
     * A method to merge the domain frequency hashmaps per domain into one hashmap containing all info for one domain
     * TODO turn hashmap (domain, {hashmap{domaincountwith:"value"}} into object of domains = {"PF0000":[domainCountWith:int value]}
     * @param map1 String, String HashMap
     * @param map2 String, String HashMap
     * @return String String HashMap map 1 with all the values added.
     */
    private static HashMap<String, String> reduceHashMaps(HashMap<String, String> map1, HashMap<String, String> map2) {
        // These are all the keys that need to be looked after
        HashSet<String> keysToMerge = new HashSet<>(Arrays.asList("domainCountWith", "domainCountWithout", "sampleOrganismWith", "sampleOrganismWithout"));
        // domain descriptions is already present in map1 so no need to do anything about that one.

        // It is possible that the counts or organisms with(out) are not present in the hashmaps.
        for (String key : keysToMerge) {
            if (!map1.containsKey(key) && map2.containsKey(key)) {
                // Just add the value from the second hashmap
                map1.put(key, map2.get(key));
            } else if (map1.containsKey(key) && map2.containsKey(key)) {
                if (key.equals("domainCountWith") || key.equals("domainCountWithout")) {
                    // Add up the two values and put them back as a String
                    String newValue = String.valueOf(Integer.parseInt(map1.get(key)) + Integer.parseInt(map2.get(key)));
                    map1.put(key, newValue);
                } else { // The key is sampleOrganismWith(out)
                    String newValue = map1.get(key) + ";" + map2.get(key);
                    map1.put(key, newValue);
                }
            }
            // if the key is in neither of the HashMap nothing has to be done
            // if the key is just not in the second HashMap, again nothing has to be done.
        }

        return map1;
    }


    /**
     * Method to read all the domain frequency files and to return info on each domain.
     *
     * @param file raw output file of the query
     * @return Iterator with tuple containing the domain and a hashmap with corresponding counts
     * @throws IOException when raw query file is not found
     */
    private static Iterator<Tuple2<String, HashMap<String, String>>> getFrequenciesPerDomain(File file) throws IOException {
        // from the original genome file name, get the look up file name.
        File fileGenomeLookUp = new File(DomainPatternRecognition.lookUpDirectory + "/" + file.getName() + ".tsv");
        HashMap<String, Tuple2<String, Integer>> rawParsed = parseGenomeLookUpFile(fileGenomeLookUp);
        ArrayList<Tuple2<String, HashMap<String, String>>> output = new ArrayList<>();

        if (rawParsed.containsKey(domainToQuery)) { // this genome also contains the domain of interest
            for (String domain : rawParsed.keySet()) {
                Tuple2<String, Integer> rawInfo = rawParsed.get(domain);
                HashMap<String, String> domainInfo = new HashMap<>();
                domainInfo.put("domainDescription", rawInfo._1);
                domainInfo.put("sampleOrganismWith", rawParsed.get("Organism")._1);
                domainInfo.put("domainCountWith", String.valueOf(rawInfo._2));
                output.add(new Tuple2<>(domain, domainInfo));
            }

        } else { // this genome does not contain the domain of interest
            for (String domain : rawParsed.keySet()) {
                Tuple2<String, Integer> rawInfo = rawParsed.get(domain);
                HashMap<String, String> domainInfo = new HashMap<>();
                domainInfo.put("domainDescription", rawInfo._1);
                domainInfo.put("sampleOrganismWithout", rawParsed.get("Organism")._1);
                domainInfo.put("domainCountWithout", String.valueOf(rawInfo._2));
                output.add(new Tuple2<>(domain, domainInfo));
            }
        }
        return output.iterator();
    }

    /**
     * Method to parse the info from one raw query fileGenomeLookUp file
     *
     * @param fileGenomeLookUp file object
     * @return HashMap<String, Tuple2 < String, Integer>> key = domain, tuple with domain descriptions and frequency.
     * @throws IOException when the file genome look up cannot be found.
     */
    private static HashMap<String, Tuple2<String, Integer>> parseGenomeLookUpFile(File fileGenomeLookUp) throws IOException {
        HashMap<String, Tuple2<String, Integer>> queryResults = new HashMap<>();
        Scanner scanner = new Scanner(fileGenomeLookUp);

        if (scanner.hasNextLine()) { // skip the first line
            scanner.nextLine();
        }

        // TODO merge this hasnextline with the while loop
        // Reading first line can be done with for example a boolean check...

        if (scanner.hasNextLine()) {
            // get the first line and enter the organism name just once.
            String[] firstLine = scanner.nextLine().split("\t");
            if (!firstLine[3].equals("0")) { // It is possible that there is an empty line and this needs to be skipped
                queryResults.put(firstLine[0].substring(1, firstLine[0].length() - 1), new Tuple2<String, Integer>(firstLine[2], Integer.valueOf(firstLine[3])));
                queryResults.put("Organism", new Tuple2<String, Integer>(firstLine[1], -1));
            }
        }

        // put the rest of the results in.
        while (scanner.hasNextLine()) {
            String[] line = scanner.nextLine().split("\t");
            queryResults.put(line[0].substring(1, line[0].length() - 1), new Tuple2<String, Integer>(line[2], Integer.valueOf(line[3])));
        }

        scanner.close();
        return queryResults;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GENERAL METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Method to get all the files from the input directory that need to be queried.
     * It also checks whether the given input is a file or a directory.
     *
     * @return JavaRDD with the files
     */
    private JavaRDD<File> getAllInputFiles() {

        List<File> files = new ArrayList<>();

        LOGGER.info("Listing files recursively: " + commandOptions.recursiveInput);
        for (String directoryName : commandOptions.inputDirectory) {
            File directory = new File(directoryName);
            files.addAll(FileUtils.listFiles(directory,null,commandOptions.recursiveInput));
            LOGGER.info("Listing files from: " + directory.getAbsolutePath());
        }

        LOGGER.info("Found: " + files.size() + " files ");

        // Need to split it up in more partitions than the number of jobs as a single job can be
        // finished sooner than others
        int partitions = Math.round(files.size() / commandOptions.jobs);
        if (partitions < 1) partitions = 1;
        JavaRDD<File> allFilesJavaRDD = sc.parallelize(files, partitions);
        // Filter to keep only the HDT files themselves.
        return allFilesJavaRDD.filter(file -> !file.getName().endsWith("index.v1-1") && !file.getName().endsWith(".index") && DomainPatternRecognition.checkHDT(file));
    }

    /**
     * Method from RDFSimplecon class to give the SPARQL query.
     *
     * @param queryFile filename of the query
     * @param args      possible arguments to be used in the query
     * @return query as a string with the provided variables included.
     * @throws Exception
     */
    private static String createQueryFromFile(String queryFile, Object... args) throws Exception {
        String header = Util.readFile("queries/header.txt");
        String content = Util.readFile(queryFile);
        String queryString = header + content;
        Pattern path = Pattern.compile("^((FRoM)|(WITH)|(USING))\\s+<\\%\\d+\\$S>.*$", 74);
        if (path.matcher(queryString).find()) {
            Object[] toPass = new Object[args.length + 1];
            System.arraycopy(args, 0, toPass, 1, args.length);
            toPass[0] = graph;
            args = toPass;
            if (graph == null) {
                queryString = path.matcher(queryString).replaceAll("");
            }
        }

        queryString = String.format(queryString, args);
        return queryString;
    }


    private static void setFormat(String format, PrintStream tempStream, ResultSet results) {
        if (format.toLowerCase().matches("csv")) {
            ResultSetFormatter.outputAsCSV(tempStream, results);
        } else if (format.toLowerCase().matches("tsv")) {
            ResultSetFormatter.outputAsTSV(tempStream, results);
        } else if (format.toLowerCase().matches("json")) {
            ResultSetFormatter.outputAsJSON(tempStream, results);
        } else if (format.toLowerCase().matches("xml")) {
            ResultSetFormatter.outputAsXML(tempStream, results);
        } else {
            LOGGER.error("Output format unknown: " + format);
        }
    }

    public HashMap<String, HashMap<String, String>> getDomainFrequencies() {
        return domainFrequencies;
    }

    /**
     * method to store the generated operon hashmap as a serialized object in a file.
     */
    private void serializeOperonHashMap(String domainToQuery, HashMap<String, Operon> operonHashMap) throws IOException, NoSuchAlgorithmException {
        String operonHashName = DomainPatternRecognition.generateHashedOperonHashMapName(commandOptions, domainToQuery);
        File operonHashMapFile = new File(regionDirectory + "/" + operonHashName);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(operonHashMapFile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(operonHashMap);
            objectOutputStream.close();
            fileOutputStream.close();
            LOGGER.info("Successfully serialized the operon hashmap to: " + operonHashMapFile.getAbsolutePath());
        } catch (IOException ioe) {
            LOGGER.error(">>> FAILED: failed to serialize the operon HashMap");
            ioe.printStackTrace();
        }
    }

    /**
     * Method to store the domain frequency hashmap as a serialized object
     *
     * @throws IOException              when the output file cannot be generated
     * @throws NoSuchAlgorithmException when the hashed file name cannot be generated.
     */
    private void serializeDomainFrequencies() throws IOException, NoSuchAlgorithmException {
        File domainFrequenciesFile = generateDomainFrequenciesName();

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(domainFrequenciesFile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(domainFrequencies);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException ioe) {
            LOGGER.error(">>> FAILED: failed to serialize the domain frequencies HashMap");
            ioe.printStackTrace();
        }
    }

    /**
     * Method to generate a hashed (unique) name for the domain frequency hashmap depending on the specific input files
     * Saves the domain copy number etc.. domain statistics, which species / samples etc...
     * @return A file object with the right name
     * @throws IOException              due to reading all the files
     * @throws NoSuchAlgorithmException due to hashing
     */
    private File generateDomainFrequenciesName() throws IOException, NoSuchAlgorithmException {
        List<File> files = new ArrayList<>();
        for (String directoryName : commandOptions.inputDirectory) {
            File directory = new File(directoryName);
            files.addAll(FileUtils.listFiles(directory,null,commandOptions.recursiveInput));
        }

//        List<File> files = new ArrayList<>(FileUtils.listFiles(new File(commandOptions.inputDirectory), null, commandOptions.recursiveInput));
        ArrayList<String> inputHDTFiles = new ArrayList<>();

        for (File file : files) {
            if (!file.getName().endsWith("index.v1-1") && DomainPatternRecognition.checkHDT(file)) {
                inputHDTFiles.add(file.getAbsolutePath());
            }
        }
        String content = domainToQuery + "\n" + String.join("\n", inputHDTFiles);
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(content.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        String name = sb.toString() + ".ser";
        return new File(DomainPatternRecognition.lookUpDirectory + "/" + name);
    }

    /**
     * Method to load the domain frequencies hashmap or return an empty one when it is not present
     *
     * @param domainFrequenciesFile File object of the domain frequencies
     * @return domainFrequency hashmap
     */
    private HashMap<String, HashMap<String, String>> deserializeDomainFrequencyHashMap(File domainFrequenciesFile) {
        if (domainFrequenciesFile.exists()) {
            try {
                HashMap domainFrequencies = null;
                FileInputStream fileInputStream = new FileInputStream(domainFrequenciesFile);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                domainFrequencies = (HashMap) objectInputStream.readObject();
                objectInputStream.close();
                fileInputStream.close();
                return domainFrequencies;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return new HashMap<>();
    }

    /**
     * Method to deserialzie the operon hashmap file if it is present
     *
     * @param operonFile File object of the operon
     * @return boolean, necessary to requery the input data?
     */
    private HashMap<String, Operon> deserializeOperonHashMap(File operonFile) {
        try {
            HashMap<String, Operon> operonHash = new HashMap();
            FileInputStream fileInputStream = new FileInputStream(operonFile);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            operonHash = (HashMap<String, Operon>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return operonHash;

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            LOGGER.error(">>>FAILED: unable to load the Operon HashMap stored at: " + operonFile.getAbsolutePath());
            return new HashMap<>();
        }
    }

    public File getDomainFrequencyHashMapFileName() {
        return domainFrequencyHashMapFile;
    }
}
