package nl.wur.ssb.DomainPatternRecognition;

import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.fpm.FPGrowth;
import scala.Tuple2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class MachineLearningParallel {

    private static final Logger LOGGER = Logger.getLogger(MachineLearningParallel.class.getName());
    protected static CommandOptions commandOptions;
    protected static JavaSparkContext sc;
    protected HashMap<String, HashMap<String, String>> domainFrequencies = new HashMap<>();
    protected int numberOfInputFiles = 0;
    protected JavaRDD<FPGrowth.FreqItemset<String>> loneDomains;


    /**
     * Method to count the occurrence of a domain in one sample
     *
     * @param domain              String with domain ID to be counted. one of the patterns from the frequency pattern output.
     *                            Domain is only counted when there is one domain in the pattern
     * @param completeGenomeCount HashMap with the domain frequencies of all domains present in one sample.
     */

    protected static HashMap<String, HashMap<String, String>> countDomainsOneGenome(HashMap<String, HashMap<String, String>> frequencyHashMap, String domain, HashMap<String, Tuple2<String, Integer>> completeGenomeCount) {
        if (completeGenomeCount.containsKey(commandOptions.domain)) {
            if (completeGenomeCount.containsKey(domain)) {
                if (frequencyHashMap.containsKey(domain)) {
                    HashMap<String, String> domainInfo = frequencyHashMap.get(domain);

                    int updateCount = Integer.parseInt(domainInfo.get("domainCountWith")) +
                            completeGenomeCount.get(domain)._2;
                    domainInfo.put("domainCountWith", String.valueOf(updateCount));

                    // Adding the organism name to the whole string. Can be used for counting in how
                    // many strains of the same species the domain occurs.
                    String organismString = domainInfo.get("sampleOrganismWith") + ";" + completeGenomeCount.get("Organism")._1;
                    domainInfo.put("sampleOrganismWith", organismString);
                } else {

                    HashMap<String, String> domainInfo = new HashMap<>();
                    // Counting the domain when domain of interest is also present in the sample

                    domainInfo.put("domainDescription", completeGenomeCount.get(domain)._1);
                    domainInfo.put("sampleOrganismWith", completeGenomeCount.get("Organism")._1);
                    domainInfo.put("sampleOrganismWithout", "");
                    domainInfo.put("domainCountWith", String.valueOf(completeGenomeCount.get(domain)._2));
                    domainInfo.put("domainCountWithout", String.valueOf(0));

                    frequencyHashMap.put(domain, domainInfo);
                }
            }
        } else {
            if (completeGenomeCount.containsKey(domain)) {
                if (frequencyHashMap.containsKey(domain)) {
                    HashMap<String, String> domainInfo = frequencyHashMap.get(domain);
                    int updateCount = Integer.parseInt(domainInfo.get("domainCountWithout")) +
                            completeGenomeCount.get(domain)._2;
                    domainInfo.put("domainCountWithout", String.valueOf(updateCount));
                    String organismString = domainInfo.get("sampleOrganismWithout") + ";" + completeGenomeCount.get("Organism")._1;
                    domainInfo.put("sampleOrganismWithout", organismString);

                } else { // domain not yet present in the domainFrequencies hashmap
                    HashMap<String, String> domainInfo = new HashMap<>();
                    domainInfo.put("domainDescription", completeGenomeCount.get(domain)._1);
                    domainInfo.put("sampleOrganismWith", "");
                    domainInfo.put("sampleOrganismWithout", completeGenomeCount.get("Organism")._1);
                    domainInfo.put("domainCountWith", String.valueOf(0));
                    domainInfo.put("domainCountWithout", String.valueOf(completeGenomeCount.get(domain)._2));
                    frequencyHashMap.put(domain, domainInfo);
                }
            }
        }
        return frequencyHashMap;
    }

    /**
     * Method to count the frequency of domain occurrence by loading in the look up tables from the input files.
     *
     * @throws IOException due to generateHashedFileName
     */
    public void countDomainOccurrence(HashSet<String> domains) throws IOException {
        ArrayList<File> inputFiles = new ArrayList<>();
        for (String directoryName : commandOptions.inputDirectory) {
            File directory = new File(directoryName);
            inputFiles.addAll(FileUtils.listFiles(directory,null,commandOptions.recursiveInput));
        }

        // Create through all input files to see which lookup files need to be searched through.
        ArrayList<String> lookUpFileNames = new ArrayList<>();
        for (File inputFile : inputFiles) {
            if (!inputFile.getName().endsWith("index.v1-1") && DomainPatternRecognition.checkHDT(inputFile)) {
                lookUpFileNames.add(DomainPatternRecognition.retrieveLookUpFileName(inputFile));
            }
        }

        this.numberOfInputFiles = lookUpFileNames.size();

        ArrayList<File> files = new ArrayList<File>(FileUtils.listFiles(new File(DomainPatternRecognition.lookUpDirectory), null, commandOptions.recursiveInput));

        for (File file : files) {
            if (lookUpFileNames.contains(file.getName())) {

                HashMap<String, Tuple2<String, Integer>> completeGenomeCount = null;

                try {
                    FileInputStream fileInputStream = new FileInputStream(file);
                    ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
                    completeGenomeCount = (HashMap) objectInputStream.readObject();
                    objectInputStream.close();
                    fileInputStream.close();

                } catch (IOException | ClassNotFoundException e) {
                    System.out.println(">>>> FAILED: loading " + file.getName());
                    e.printStackTrace();
                    continue; // ensures the code does not continue when the HashMap is null.
                }

                for (String domain : domains) {
                    countDomainsOneGenome(this.domainFrequencies, domain, completeGenomeCount);
                }
            }
        }
    }


    /**
     * Method to get the descriptions of an arrayList of domainIDs
     *
     * @param domains ArrayList of string with domain IDs
     * @return The functional description of the domains in an array ordered the same as the input.
     */

    protected ArrayList<String> getDescriptionsOfDomains(ArrayList<String> domains) {
        ArrayList<String> domainDescriptions = new ArrayList<>();
        try {
            FileInputStream fileInputStream = new FileInputStream(DomainPatternRecognition.filenameDomainFrequencies);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            domainFrequencies = (HashMap) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


        for (String domainID : domains) {
            domainDescriptions.add(domainFrequencies.get(domainID).get("domainDescription"));
        }
        return domainDescriptions;
    }


    /**
     * Method to calculate the support value copy number and number of organisms for one domain.
     *
     * @param domainInfo The hashmap from domain frequencies for one  domain.
     * @return the updated version of domainInfo with the calculated counts added.
     */

    protected HashMap<String, String> calculateCopyAndOrganismNumber(HashMap<String, String> domainInfo) {
        int organismsWithCount;
        double copyNumberWith;

        if (domainInfo.get("sampleOrganismWith").length() == 0) {
            organismsWithCount = 0;
            copyNumberWith = 0;

        } else {
            organismsWithCount = Arrays.asList(domainInfo.get("sampleOrganismWith").split(";")).size();
            copyNumberWith = Double.parseDouble(domainInfo.get("domainCountWith")) / organismsWithCount;
        }


        int organismsWithoutCount;
        double copyNumberWithout;

        if (domainInfo.get("sampleOrganismWithout").length() == 0) {
            organismsWithoutCount = 0;
            copyNumberWithout = 0;
        } else {
            organismsWithoutCount = Arrays.asList(domainInfo.get("sampleOrganismWithout").split(";")).size();
            copyNumberWithout = Double.parseDouble(domainInfo.get("domainCountWithout")) / organismsWithoutCount;
        }
        domainInfo.put("organismWithoutCount", String.valueOf(organismsWithoutCount));
        domainInfo.put("organismWithCount", String.valueOf(organismsWithCount));
        domainInfo.put("copyNumberWith", String.valueOf(copyNumberWith));
        domainInfo.put("copyNumberWithout", String.valueOf(copyNumberWithout));

        return domainInfo;
    }

    /**
     * Method to check whether all domains in domains are present in to domainFrequencies and add them if they are not.
     *
     * @param domains ArrayList of String with all domains (by ID) domainfrequencies should contain afterwards.
     */
    public void complementDomainFrequencies(ArrayList<String> domains) throws IOException {

        HashSet<String> domainsToBeAdded = new HashSet<>();

        for (String domain : domains) {
            if (!this.domainFrequencies.containsKey(domain)) {
                domainsToBeAdded.add(domain);
            }
        }
        countDomainOccurrence(domainsToBeAdded);
        for (String domainKey : this.domainFrequencies.keySet()) {
            HashMap<String, String> domainInfo = calculateCopyAndOrganismNumber(this.domainFrequencies.get(domainKey));
            this.domainFrequencies.put(domainKey, domainInfo);
        }
    }
}
