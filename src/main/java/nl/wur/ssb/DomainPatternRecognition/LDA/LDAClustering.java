package nl.wur.ssb.DomainPatternRecognition.LDA;

import nl.wur.ssb.DomainPatternRecognition.MachineLearningParallel;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.clustering.LDA;
import org.apache.spark.mllib.clustering.LDAModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;

import java.util.*;

public class LDAClustering extends MachineLearningParallel {

    protected HashMap<String, Operon> operonHashMap ;
    protected static List<String> domainsToClusterOn;

    /**
     * Method to generate an LDA model object
     *
     * @param k           the number of clusters
     * @param countString List of string with counts of domain occurrences.
     * @return LDA model.
     */
    protected LDAModel generateLDAModel(int k, List<String> countString) {
        JavaRDD<String> domainData = sc.parallelize(countString);

        JavaRDD<Vector> parsedDomainData = domainData.map(s -> {
            String[] sarray = s.trim().split(" ");
            double[] values = new double[sarray.length];
            for (int i = 0; i < sarray.length; i++) {
                values[i] = Double.parseDouble(sarray[i]);
            }
            return Vectors.dense(values);
        });


        JavaPairRDD<Long, Vector> corpusDomain =
                JavaPairRDD.fromJavaRDD(parsedDomainData.zipWithIndex().map(Tuple2::swap));
        corpusDomain.cache();

        return new LDA().setK(k).run(corpusDomain);
    }

    /**
     * Method to create the count string that is needed as input for the LDA
     *
     * @param operon an operon object containing a filled domain list
     * @return String containing the counts of the lone domains for each operon.
     */
    protected static Iterator<String> createLDAInput(Operon operon) {
        ArrayList<String> countsList = new ArrayList<>();
        ArrayList<String> domainsInOperon = operon.getDomains();

        // Getting the frequency of every domain
        for (String domain : domainsToClusterOn) {
            int occurrenceDomain = Collections.frequency(domainsInOperon, domain);
            countsList.add(String.valueOf(occurrenceDomain));
        }
        // Creating the output of this function
        String countString = String.join(" ", countsList);
        ArrayList<String> countStrings = new ArrayList<>();
        countStrings.add(countString);


        return countStrings.iterator();
    }
}
