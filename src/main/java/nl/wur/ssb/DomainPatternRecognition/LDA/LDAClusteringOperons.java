package nl.wur.ssb.DomainPatternRecognition.LDA;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.MachineLearningParallel;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.clustering.LDAModel;
import scala.Tuple2;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class LDAClusteringOperons extends LDAClustering {
    private static final Logger LOGGER = Logger.getLogger(LDAClusteringOperons.class.getName());
    private static ArrayList<String> referenceDomainList;


    public LDAClusteringOperons(JavaSparkContext sc, CommandOptions commandOptions, HashMap<String, Operon> operonHashMap) {
        MachineLearningParallel.sc = sc;
        MachineLearningParallel.commandOptions = commandOptions;
        this.operonHashMap = operonHashMap;
        LOGGER.info("Starting LDA clustering the operons");
    }

    /**
     * Method to run the LDA clustering on the all operons without any previous filtering.
     */
    public void runLDAClusteringOperons() throws IOException {
        JavaRDD<Operon> operonRDD = sc.parallelize(new ArrayList<>(operonHashMap.values()));

        List<String> domainsInOperons = operonRDD.flatMap(LDAClusteringOperons::getDomainsFromOperons).collect();
        HashSet<String> uniqueDomains = new HashSet<>(domainsInOperons);
        domainsToClusterOn = new ArrayList<>(uniqueDomains);
//        referenceDomainList = new ArrayList<>(uniqueDomains);
        referenceDomainList = new ArrayList<>(domainsToClusterOn); // to make sure that the order of the two lists is the same

        List<String> domainCounts = operonRDD.flatMap(LDAClustering::createLDAInput).collect();

        // Performing the LDA clustering with the provided number of clusters

        for (int k : commandOptions.LDAClustersKOperons) {
            LOGGER.info("Working with " + k + " clusters");
            // Generate a LDA model where the clustering is performed with all domains present.
            LDAModel ldaModelBig = super.generateLDAModel(k, domainCounts);

            // Now for the different 'vocab sizes' extract the domains and redo the clustering based on the smaller sets
            for (int vocabSize : commandOptions.LDAVocabSize) {
                // Get the smaller 'vocabulary' to recluster the operons on.
                domainsToClusterOn = shrinkUsedVocab(vocabSize, ldaModelBig);
                ArrayList<String> domainDescriptions = getDescriptionsOfDomains(new ArrayList<>(domainsToClusterOn));
                List<String> smallerCounts = operonRDD.flatMap(LDAClustering::createLDAInput).collect();
                LDAModel ldaModelSmaller = super.generateLDAModel(k, smallerCounts);

                // write the output and create the charts.
                writeLdaOutput(ldaModelSmaller,k,vocabSize,domainDescriptions);
                createBarPlot(ldaModelSmaller, k, vocabSize);

            }
        }
    }

    private void createBarPlot(LDAModel ldaModel, int k, int vocabSize) throws IOException {
        String fileName = "LDA_OperonBarChart_k="+k+"_vocabSize="+vocabSize;

        // Create the data in the necessary format
        Tuple2<int[], double[]>[] describedTopics = ldaModel.describeTopics();
        ArrayList<double[]> weightArrays = new ArrayList<>();
        for (Tuple2<int[], double[]> topic : describedTopics) {
            weightArrays.add(topic._2);
        }
        BarChartLDA barChartLDAWeights = new BarChartLDA(weightArrays,new ArrayList<>(domainsToClusterOn), fileName, "LDA Operons Weights; vocab size: " + vocabSize);
    }

    private void writeLdaOutput(LDAModel ldaModel, int k, int vocabSize, ArrayList<String> domainDescriptions) throws IOException {
        String filePath = DomainPatternRecognition.outputPath + "/" + "LDAClusteringOperons_" + commandOptions.domain + "_K=" + k + "_vocabSize=" + vocabSize +".txt";
        FileWriter fileWriter = new FileWriter(filePath);

        fileWriter.write("# Input directory:\t" + commandOptions.inputDirectory + "\n");
        fileWriter.write("# Number of clusters K:\t" + k + "\n");
        fileWriter.write("# Number of domains per topic used: " + vocabSize + "\n");

        scala.Tuple2<int[], double[]>[] topics = ldaModel.describeTopics();

        // First add the domains and their description to the file
        scala.Tuple2<int[], double[]> topic1 = topics[0];
        String domainIDs;
        String descriptions;

        StringBuilder domainIDSB = new StringBuilder();
        StringBuilder descriptionSB = new StringBuilder();
        int topicSize = topic1._1.length;

        // Loop through the first topic to get the domain order and descriptions.
        for (int index : topic1._1) {

            String domainID = domainsToClusterOn.get(index);
            String domainDescription = domainDescriptions.get(index);

            domainIDSB.append(domainID).append("\t");
            descriptionSB.append(domainDescription).append("\t");
        }

        domainIDs = domainIDSB.deleteCharAt(domainIDSB.length() - 1).append("\n").toString();
        descriptions = descriptionSB.deleteCharAt(descriptionSB.length() - 1).append("\n").toString();
        fileWriter.write(domainIDs);
        fileWriter.write(descriptions);

        // Next loop through all topics and write their weights, all in the same order.

        String[] weightsInOrder = new String[topicSize];
        DecimalFormat decimalFormat = new DecimalFormat("0.00000");

        // Start writing the weights of all words for each topic.
        for (scala.Tuple2<int[], double[]> topic : topics) {

            for (int i = 0; i < topic._1.length; i++) {
                int domainIndex = topic._1[i];
                double weight = topic._2[i];
                String weightS = decimalFormat.format(weight);

                // Insert the weight at the right position.
                weightsInOrder[domainIndex] = weightS;
            }
            String weighString = String.join("\t", weightsInOrder) + "\n";
            fileWriter.write(weighString);
        }
        fileWriter.close();

    }

    /**
     * Method to shrink the vocabulary size by only taking the n = vocab size most important domains for every domain.
     *
     * @param vocabSize, int, the number of most important 'words' that are gathered from every topic.
     * @param ldaModelBig, a LDA model based on all 'words' that are present in the
     */
    private ArrayList<String> shrinkUsedVocab(int vocabSize, LDAModel ldaModelBig) {

        // First get a list with the indices of the domains that are important
        HashSet<Integer> domainIndices = new HashSet<>();

        scala.Tuple2<int[], double[]>[] topics = ldaModelBig.describeTopics();
        for (scala.Tuple2<int[], double[]> topic : topics) {
            int[] firstDomains = Arrays.copyOfRange(topic._1,0,vocabSize);
            for (int integer : firstDomains) {
                domainIndices.add(integer);
            }
        }

        // Then get the corresponding domains
        ArrayList<String> shrunkenVocab = new ArrayList<>();
        for (int index : domainIndices) {
            shrunkenVocab.add(referenceDomainList.get(index));
        }

        return shrunkenVocab;
    }

    private void printOutputLDA(LDAModel ldaModel){
        scala.Tuple2<int[], double[]>[] topics = ldaModel.describeTopics();
        for (scala.Tuple2<int[], double[]> topic : topics) {
            System.out.println("------------------------------------------------------------");
            System.out.println(Arrays.toString(topic._1));
            System.out.println(Arrays.toString(topic._2));
        }

    }


    /**
     * Method used for a flatmap() operonRDD to get all unique domains that are present in one operon
     *
     * @param operon Operon object with a filled domain list.
     * @return a String iterator containing the unique domains of the operon.
     */
    private static Iterator<String> getDomainsFromOperons(Operon operon) {
        HashSet<String> uniqueDomains = new HashSet<>();
        uniqueDomains.addAll(operon.getDomains());
        ArrayList<String> output = new ArrayList<>(uniqueDomains);
        return output.iterator();
    }




}
