package nl.wur.ssb.DomainPatternRecognition.LDA;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.MachineLearningParallel;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.clustering.LDAModel;
import org.apache.spark.mllib.fpm.FPGrowth;
import scala.Tuple2;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class LDAClusteringFrequentPatternMining extends LDAClustering {
    private static final Logger LOGGER = Logger.getLogger(LDAClusteringFrequentPatternMining.class.getName());
    private ArrayList<String> domainDescriptions;
    private double supportValue;
    private HashMap<String, HashMap<String, Double>> jaccardIndixes;

    public LDAClusteringFrequentPatternMining(JavaSparkContext sc, CommandOptions commandOptions, HashMap<String, Operon> operonHashMap, double supportValue, HashMap<String, HashMap<String, Double>> jaccardIndixes) {
        MachineLearningParallel.sc = sc;
        MachineLearningParallel.commandOptions = commandOptions;
        this.operonHashMap = operonHashMap;
        this.supportValue = supportValue;
        this.jaccardIndixes = jaccardIndixes;
    }

    /**
     * Method to run the LDA Clustering on the range of number of clusters (k) and different vocabulary sizes.
     */
    public void runLDAClusteringFP(JavaRDD<FPGrowth.FreqItemset<String>> vocabToUse) throws IOException {
        LOGGER.info("Starting LDA clustering on frequent pattern mining results");

        domainsToClusterOn = vocabToUse.flatMap(LDAClusteringFrequentPatternMining::getDomainsFromItemSetRDD).collect();

        if (domainsToClusterOn.size() != 1) {

            JavaRDD<Operon> operonRDD = sc.parallelize(new ArrayList<>(operonHashMap.values()));
            List<String> domainCounts = operonRDD.flatMap(LDAClustering::createLDAInput).collect();
            domainDescriptions = getDescriptionsOfDomains(new ArrayList<>(domainsToClusterOn));

            for (int k : commandOptions.LDAClusterKFreqPattern) {
                LOGGER.info("Clustering with " + k + " clusters");
                LDAModel ldaModel = super.generateLDAModel(k, domainCounts);
                ArrayList<double[]> correctedWeights = correctWeightsWithJaccard(ldaModel);
                writeLDAOutput(ldaModel, k, new ArrayList<>(domainsToClusterOn), domainDescriptions, correctedWeights);
                createBarPlots(ldaModel, k, supportValue, correctedWeights);
            }
        } else {
            LOGGER.info("Frequent pattern mining only found one frequent pattern (the initial seed domain). LDA clustering is not possible.");
        }
    }

    private void createBarPlots(LDAModel ldaModel, int k, double supportValue, ArrayList<double[]> correctedWeights) throws IOException {
        String fileName1 = "LDA_barChart_JaccardCorrected_fpm_support=" + this.supportValue + "_clusters=" + k;
        BarChartLDA barChartLDAJaccard = new BarChartLDA(correctedWeights, new ArrayList<>(domainsToClusterOn), fileName1, "Jaccard Corrected Weights; FPM support " + supportValue);

        String fileName2 = "LDA_barChart_fpm_support=" + this.supportValue + "_clusters=" + k;
        Tuple2<int[], double[]>[] describedTopics = ldaModel.describeTopics();
        ArrayList<double[]> weightArrays = new ArrayList<>();
        for (Tuple2<int[], double[]> topic : describedTopics) {
            weightArrays.add(topic._2);
        }
        BarChartLDA barChartLDAWeights = new BarChartLDA(weightArrays,new ArrayList<>(domainsToClusterOn), fileName2, "LDA Weights; FPM support " + supportValue);

    }


    /**
     * Method to get all the domainIDs from the domains present in a FreqItemset RDD. The method will return the first
     * domain from the itemset in the form of an iterator of a list, as this function is used in a flatmap operation
     * on a JavaRDD
     *
     * @param itemSet a frequent pattern mining item set
     * @return output as an iterator containing the first domain ID from the frequent item set.
     */
    private static Iterator<String> getDomainsFromItemSetRDD(FPGrowth.FreqItemset<String> itemSet) {
        ArrayList<String> output = new ArrayList<>();
        output.add(itemSet.javaItems().get(0));
        return output.iterator();
    }


    private ArrayList<double[]> correctWeightsWithJaccard(LDAModel ldaModel) {
        scala.Tuple2<int[], double[]>[] topics = ldaModel.describeTopics();
        ArrayList<double[]> copyNumberCorrectedWeights = new ArrayList<>();

        // Loop through all teh topics
        for (Tuple2<int[], double[]> topic : topics) {
            double[] copyNumberCorrected = new double[topic._1.length];

            // Start looping through the int[] and double[] of the topic
            for (int index = 0; index < topic._1.length; index++) {
                int domainIndex = topic._1[index];
                double rawWeight = topic._2[index];

                String domainID = domainsToClusterOn.get(domainIndex);
                double jaccardIndex = jaccardIndixes.get(domainID).get("samplesWith");

                double correctedWeight = rawWeight * jaccardIndex;
                copyNumberCorrected[domainIndex] = correctedWeight;

            }
            copyNumberCorrected = normalizeArray(copyNumberCorrected);
            copyNumberCorrectedWeights.add(copyNumberCorrected);
        }

        return copyNumberCorrectedWeights;
    }

    /**
     * Method to normalize an array of doubles to sum to 1
     *
     * @param doubleArray Array of doubles
     * @return the normalized array where all elements sum up to 1 .
     */

    private double[] normalizeArray(double[] doubleArray) {
        double[] normalized = new double[doubleArray.length];
        double sum = Arrays.stream(doubleArray).sum();
        for (int i = 0; i < doubleArray.length; i++) {
            normalized[i] = doubleArray[i] / sum;
        }
        return normalized;
    }

    /**
     * Method to write the output from the LDA model to a file for human reading.
     *
     * @param ldaModel           LDA model
     * @param k                  int: number of clusters
     * @param domainIDsList      ArrayList of String with the IDs of domains
     * @param domainDescriptions ArrayList of string with the descriptions of the domains in the same order as domainIDsList.
     * @throws IOException due to writing of the files.
     */
    private void writeLDAOutput(LDAModel ldaModel, int k, ArrayList<String> domainIDsList, ArrayList<String> domainDescriptions, ArrayList<double[]> correctedWeights) throws IOException {
        String filePath = DomainPatternRecognition.outputPath + "/" + commandOptions.domain + "_K=" + k + "_FPMSupport=" + supportValue + "_LDA.txt";
        FileWriter fileWriter = new FileWriter(filePath);
        fileWriter.write("# Input directory:\t" + commandOptions.inputDirectory + "\n");
        fileWriter.write("# Number of clusters K:\t" + k + "\n");
        fileWriter.write("# Support value frequent pattern mining :\t" + supportValue + "\n");
        fileWriter.write("#\n");

        int vocabSize = this.domainDescriptions.size();

        scala.Tuple2<int[], double[]>[] topics = ldaModel.describeTopics();

        scala.Tuple2<int[], double[]> topic1 = topics[0];
        String domainIDs;
        String descriptions;

        StringBuilder domainIDSB = new StringBuilder();
        StringBuilder descriptionSB = new StringBuilder();

        // Loop through the first topic to get the domain order and descriptions.
        for (int index : topic1._1) {

            String domainID = domainIDsList.get(index);
            String domainDescription = domainDescriptions.get(index);

            domainIDSB.append(domainID).append("\t");
            descriptionSB.append(domainDescription).append("\t");
        }

        domainIDs = domainIDSB.deleteCharAt(domainIDSB.length() - 1).append("\n").toString();
        descriptions = descriptionSB.deleteCharAt(descriptionSB.length() - 1).append("\n").toString();
        fileWriter.write(domainIDs);
        fileWriter.write(descriptions);

        String[] weightsInOrder = new String[vocabSize];
        DecimalFormat decimalFormat = new DecimalFormat("0.00000");

        // Start writing the weights of all words for each topic.
        for (scala.Tuple2<int[], double[]> topic : topics) {

            for (int i = 0; i < vocabSize; i++) {
                int domainIndex = topic._1[i];
                double weight = topic._2[i];
                String weightS = decimalFormat.format(weight);

                // Insert the weight at the right position.
                weightsInOrder[domainIndex] = weightS;
            }
            String weighString = String.join("\t", weightsInOrder) + "\n";
            fileWriter.write(weighString);
        }

        fileWriter.write("# Copy number corrected weights\n");
        for (double[] weights : correctedWeights) {
            // make a string of every double to be able to write the string.
            String[] weightsS = Arrays.stream(weights).mapToObj(Double::toString).toArray(String[]::new);
            // format all these strings to have only 5 digits.
            String[] formatWeights = Arrays.stream(weightsS).map(str -> str.substring(0, 7)).toArray(String[]::new);
            String correctedWeightString = String.join("\t", formatWeights) + "\n";
            fileWriter.write(correctedWeightString);
        }

        fileWriter.close();
    }


}
