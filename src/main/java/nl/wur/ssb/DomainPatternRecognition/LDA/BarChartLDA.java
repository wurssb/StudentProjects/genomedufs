package nl.wur.ssb.DomainPatternRecognition.LDA;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class BarChartLDA {
    private ArrayList<double[]> data;
    private ArrayList<String> columnKeys;
    private String plotFileName;
    private String plotTitle;


    public BarChartLDA(ArrayList<double[]> data, ArrayList<String> columnKeys, String plotFileName, String plotTitle ) throws IOException {
        this.data = data;
        this.columnKeys = columnKeys;
        this.plotFileName = plotFileName;
        this.plotTitle = plotTitle;
        plot();

    }

    private void plot() throws IOException {
        CategoryDataset dataSet = createDataSet();
        JFreeChart chart = createChart(dataSet, plotFileName);
        JFreeChart clearedChart = cleanUpChart(chart) ;

        int width = data.get(0).length * 30;
        if (width < 300) {
            width = 300;
        } else if (width > 1000) {
            width = 1000;
        }

        String filePath = DomainPatternRecognition.outputPath + "/" + plotFileName + ".png";
        ChartUtilities.saveChartAsPNG(new File(filePath), clearedChart, width, 300);

    }

    /**
     * Method to create the categorical data set that will be used in the plot
     *
     * @return CategoryDataset.
     */
    private CategoryDataset createDataSet() {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();

        int topicNumber = 0;
        for (double[] topic : data) {
            topicNumber += 1;
            for (int i = 0; i < topic.length; i++) {
                String rowKey = "topic " + topicNumber;
                dataSet.setValue(topic[i], rowKey, columnKeys.get(i));
            }
        }
        return dataSet;
    }

    /**
     * Method to create the bar chart
     *
     * @param dataSet CategoryDataset
     * @param title title that will be above the graph
     * @return JFreeChart barchart object.
     */
    private JFreeChart createChart(CategoryDataset dataSet, String title) {
        return ChartFactory.createBarChart(
                title,
                "",
                "Weight",
                dataSet,
                PlotOrientation.VERTICAL,
                true, true, false);
    }

    /**
     * Method to clean up the default created chart
     *
     * @param chart JFreeChart object
     * @return JFreeChart object that looks a lot better.
     */
    private JFreeChart cleanUpChart(JFreeChart chart) {
        // format the axis
        CategoryAxis axis = chart.getCategoryPlot().getDomainAxis();
        axis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);

        // clean up the background
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        plot.setOutlineVisible(false);

        // format the bars
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        ((BarRenderer) plot.getRenderer()).setShadowVisible(false);
        ((BarRenderer) plot.getRenderer()).setItemMargin(0.05);

        Paint[] colors = new Paint[]{
                new Color(0, 172, 178),
                new Color(239, 70, 55),
                new Color(85, 177, 69),
                new Color(255,215,0),
                new Color(0,139,139),
                new Color(255,105,180),
                new Color(160,82,45),
                new Color(85,107,47),
                new Color(218,165,32),
                new Color(30,144,255)};

        for (int i = 0; i < colors.length + 1; i++) {
            ((BarRenderer) plot.getRenderer()).setSeriesPaint(i, colors[i % colors.length]);
        }
        // format the legend
        chart.getLegend().setFrame(BlockBorder.NONE);

        return chart;
    }
}
