package nl.wur.ssb.DomainPatternRecognition.FPMNetworkDeprecated;

import java.io.Serializable;
import java.util.Objects;

public class FPMNetworkNode implements Serializable {
    private Long id;
    private String domainID;

    public FPMNetworkNode(String domainID) {
//        this.id = UUID.randomUUID().getMostSignificantBits();
        this.domainID = domainID;
        this.id = (long) domainID.hashCode();
    }


    // Override the comparison methods to make sure that two nodes with the same domainID are considered the same.
    @Override
    public int hashCode() {
        return Objects.hash(this.domainID);
    }

    // Overwrite the equals() method to have two traitNodes to be equal if their patterns are the same
    @Override
    public boolean equals(Object object) {
        // the object is compared to itself
        if (object == this) {
            return true;
        }
        // the object is not an not a FPMNetworkNode
        if (!(object instanceof FPMNetworkNode)) {
            return false;
        }
        // Compare the patterns here. This only happens when the object is a TraitNode.
        FPMNetworkNode node = (FPMNetworkNode) object;
        return node.getDomainID().equals(this.getDomainID()); // both have the same domain ID
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomainID() {
        return domainID;
    }

    public void setDomainID(String domainID) {
        this.domainID = domainID;
    }

    public String toString(){
        return domainID;
    }
}
