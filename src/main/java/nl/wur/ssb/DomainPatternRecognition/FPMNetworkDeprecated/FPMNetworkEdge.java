package nl.wur.ssb.DomainPatternRecognition.FPMNetworkDeprecated;

import java.io.Serializable;
import java.util.UUID;

public class FPMNetworkEdge implements Serializable {
    private String id;
    private String src;
    private String dst ;


    public FPMNetworkEdge (String src, String dst) {
        this.src = src;
        this.dst = dst ;
        this.id = UUID.randomUUID().toString();
    }


    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDst() {
        return dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String toString() {
        return this.src + " -> " + this.dst;
    }
}
