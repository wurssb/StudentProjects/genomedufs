package nl.wur.ssb.DomainPatternRecognition.FPMNetworkDeprecated;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class FPMNetwork3 implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(NetworkFromFPMGraphX.class.getName());
    private final CommandOptions commandOptions;
    private final double supportValue;
    private final SparkSession session;
    private JavaSparkContext sc;
    private HashMap<String, Double> domainsInNetwork;
    private JavaRDD<File> inputFiles;

//    private static double networkCutoff;
//    private static int regionWidth;

    public FPMNetwork3(CommandOptions commandOptions, double supportValue, JavaSparkContext sc) {
        this.commandOptions = commandOptions;
        this.supportValue = supportValue;
        this.sc = sc;
//        networkCutoff = commandOptions.networkCutOff;
//        regionWidth = commandOptions.width;

        this.session = SparkSession.builder()
                .appName("NetworkFromFPM")
                .sparkContext(sc.sc())
                .getOrCreate();
    }

    public void createNetwork() throws IOException, NoSuchAlgorithmException {
        // The way to go from here
        // How to redo this in a more simple way.
        // get all the domains from the FPM output around the domain of interest.
        // For these domains do the most simple way of querying. Create new functions that just give me the list
        // of domains that are needed. I DONT NEED ANYTHING ELSE TO RUN THE FPM AND GET THE RESULTS I WANT
        // Create a custom FPM for this specific class so that I can just immediatly have all the results and
        // that I don't have to write them to files in the mean time. Just keep everything in an RDD.

        // STEP 1: GET THE FPM RESULTS FOR THE DOMAIN OF INTEREST AT A SPECIFIC SUPPORT VALUE
        LOGGER.info("Analyzing network from the FPM results of: " + commandOptions.domain);
        // domains in network contains the String = domainID, double = support value with the domain of interest.
        domainsInNetwork = getDomainsFromFile(new File(DomainPatternRecognition.outputPath + "/" + this.commandOptions.domain + "_" + supportValue + "_frequency_patterns.txt"));

        // STEP 2: Get all the input files
        getRDDOfFiles();

        // STEP 3: For these domains, the input data needs to be queried to get a list of the domains that are present
        //  in the operons.

        HashSet<File> FPMoutputFiles = new HashSet<>();
        for (String domain : domainsInNetwork.keySet()) {
            FPMoutputFiles.add(processOneDomain(domain)) ;
        }

        System.out.println(FPMoutputFiles);
    }

    private File processOneDomain(String domain) throws IOException, NoSuchAlgorithmException {
        // First check whether the FPM result file is already present
        String domainDirectory = DomainPatternRecognition.storageDirectory + "/Regions_" + domain + "_searchWidth=" + commandOptions.width;
        File FPMoutputFile = new File(domainDirectory + "/" + domain + "_" + commandOptions.networkCutOff + "_frequency_patterns.txt");
        if (FPMoutputFile.length() != 0) { // .length gives both zero when the file is empty and when it does not exist.
            LOGGER.info("FPM output for domain " + domain + " already exists at: " + FPMoutputFile.getAbsolutePath());
            return FPMoutputFile;
        }

        LOGGER.info("Querying input data for for " + domain);
        JavaRDD<Operon> operonJavaRDD = queryInputData(domain, domainDirectory);

        Iterator<Operon> operonIterator = operonJavaRDD.toLocalIterator();
        while (operonIterator.hasNext()) {
            System.out.println(operonIterator.next().getDomainsOperonString());
        }

        return new File("dummy"); // todo: change this to the proper file once done.

    }

    private JavaRDD<Operon> queryInputData(String domain, String domainDirectory) throws IOException, NoSuchAlgorithmException {
        File operonHashMapFile = new File(domainDirectory + "/" + DomainPatternRecognition.generateHashedOperonHashMapName(commandOptions, domain));
        if (!operonHashMapFile.exists()) {
            // The operon HashMap does not yet exist for these specific search parameters, so create it.
            return this.inputFiles.flatMap(file -> queryOneFile(file, domainDirectory));

        } else { // The operon hashmap already exists so just return this one
            HashMap<String, Operon> operonHashMap = deserializeOperonHashMap(operonHashMapFile);
            return sc.parallelize(new ArrayList<>(operonHashMap.values()));
        }

    }

    private static Iterator<Operon> queryOneFile(File file, String domainDirectory) throws Exception {
        File fileRegionsInGenome = new File(domainDirectory + "/regions_" + file.getName() + ".tsv");

        // Set up the resources needed for the querying
        HDT hdt = HDTManager.loadIndexedHDT(file.getAbsolutePath(), null);
        HDTGraph graph = new HDTGraph(hdt);
        Model model = ModelFactory.createModelForGraph(graph);

//        HashSet<Operon> operons = InputQuerier.getOperonsFromGenomes(model, fileRegionsInGenome);

        HashSet<Operon> operons = new HashSet<>(); // todo: change to something
        // Freeing up the resources after the queries have been executed.
        model.close();
        graph.close();
        hdt.close();

        return operons.iterator();
    }


    /**
     * A method to get all the domains from the frequent pattern mining output which will be used to create the network
     */
    private HashMap<String, Double> getDomainsFromFile(File file) throws FileNotFoundException {
        // find the file that contains the FPM output for the domain of interest.
        // This is also used in writeFrequentPatternOutput()

        HashMap<String, Double> FPMDomains = new HashMap<>(); // hahsmap contains domain and the support value.
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            if (!line.startsWith("#")) {
                String[] lineParts = line.split("\t");
                FPMDomains.put(lineParts[0], Double.valueOf(lineParts[2]));
            }
        }
        scanner.close();
        return FPMDomains;
    }

    /**
     * A method to get the serialized file of the operon hashmap and return the hashmap from the file
     * @param file the serialized file where the operon hashmap for this specific search is kept
     * @return hashMap of string keys with Operon values.
     */
    private HashMap<String, Operon> deserializeOperonHashMap(File file) {
        try {
            HashMap<String, Operon> operonHash = new HashMap();
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            operonHash = (HashMap<String, Operon>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
            return operonHash;
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.error(">>> FAILED: Failed to deserialize an operon HashMap Object at: " + file.getAbsolutePath());
            return new HashMap<>();
        }
    }

    private void getRDDOfFiles() {
        List<File> files = new ArrayList<>();

        LOGGER.info("Listing files recursively: " + commandOptions.recursiveInput);
        for (String directoryName : commandOptions.inputDirectory) {
            File directory = new File(directoryName);
            files.addAll(FileUtils.listFiles(directory,null,commandOptions.recursiveInput));
        }

        LOGGER.info("Found: " + files.size() + " files ");


        // Need to split it up in more partitions than the number of jobs as a single job can be
        // finished sooner than others
        int partitions = Math.round(files.size() / commandOptions.jobs);
        if (partitions < 1) partitions = 1;
        this.inputFiles =  sc.parallelize(files, partitions);
    }





}
