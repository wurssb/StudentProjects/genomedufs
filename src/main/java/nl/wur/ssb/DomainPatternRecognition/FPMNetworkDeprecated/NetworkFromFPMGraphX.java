package nl.wur.ssb.DomainPatternRecognition.FPMNetworkDeprecated;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.graphx.Edge;
import org.apache.spark.sql.SparkSession;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.*;

public class NetworkFromFPMGraphX implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(NetworkFromFPMGraphX.class.getName());
    private final CommandOptions commandOptions;
    private final double supportValue;
    private final SparkSession session;
    private JavaSparkContext sc;
    private HashMap<String, Double> domainsInNetwork;

    public NetworkFromFPMGraphX(CommandOptions commandOptions, double supportValue, JavaSparkContext sc) {
        this.commandOptions = commandOptions;
        this.supportValue = supportValue;
        this.sc = sc;

        // todo: look whether I really need this one. and look at what a spark session really is.
        this.session = SparkSession.builder()
                .appName("NetworkFromFPM")
                .sparkContext(sc.sc())
                .getOrCreate();
    }

    public void runFPMNetworkAnalysis () throws Exception {
        LOGGER.info("Analyzing network from FPM results of " + commandOptions.domain);
        // Get all the domains that are found in the FPM results of the DOI and the support value of the pattern
        domainsInNetwork = getDomainsFromFile(new File(DomainPatternRecognition.outputPath + "/" + this.commandOptions.domain + "_" + supportValue + "_frequency_patterns.txt"), commandOptions.domain);

        // Get the Frequent pattern mining output of these domains
        // todo: let the program check whether the necessary files are already there.
        String[] extensions = new String[] {"txt"} ;
//        HashSet<File> filesAlreadyPresent = new HashSet<>(FileUtils.listFiles(new File(commandOptions.outputPath).g, extensions, true));
//        System.out.println("\nFiles: " +filesAlreadyPresent);
        for (String domain : domainsInNetwork.keySet()) {
            queryAroundDomain(domain);
        }

//        JavaRDD<String> domainsAroundRDD = sc.parallelize(new ArrayList<>(domainsInNetwork.keySet())); //First get all the domains into an RDD
//        JavaRDD<FPMNetworkNode> domainNodesRDD = domainsAroundRDD.map(FPMNetworkNode::new); // Next create node objects
//        JavaRDD<Tuple2<Object, String>> vertexRDD = domainNodesRDD.map(node -> new Tuple2<>(node.getId(), node.getDomainID())); // creating the vertices
//
//        // next create the edges. The ID of an vertex depends on the domainID, so every vertex with the same domain ID should
//        // generate the same long ID
//
//
//        // Get all the right files
//
//        List<File> files = new ArrayList<>(FileUtils.listFiles(new File(commandOptions.outputPath), extensions, true));
//        JavaRDD<File> filesRDD = sc.parallelize(files) ;
//        // process all the files and create all the edges.
//        JavaRDD<Edge<Integer>> edgeRDD = createEdgesFromFiles(filesRDD, commandOptions.networkCutOff, domainsInNetwork.keySet());
//
//        // Create class tags to send along with the graph // todo: figure out what class tags precisely are.
//        ClassTag<String> stringClassTag = ClassTag$.MODULE$.apply(String.class);
//        ClassTag<Integer> integerClassTag = ClassTag$.MODULE$.apply(Integer.class);
//
//        Graph<String, Integer> graph = Graph.apply(vertexRDD.rdd(), edgeRDD.rdd(), "", StorageLevel.MEMORY_ONLY(), StorageLevel.MEMORY_ONLY(), stringClassTag, integerClassTag);
//        graph.vertices().toJavaRDD().collect().forEach(System.out::println);
//        graph.edges().toJavaRDD().collect().forEach(System.out::println);
//
//        Graph<Object, Integer> stronglyConnected = graph.ops().stronglyConnectedComponents(5);
//        JavaRDD<Tuple2<Object, Object>> verticesStrongly = stronglyConnected.vertices().toJavaRDD();

    }

    private static JavaRDD<Edge<Integer>> createEdgesFromFiles (JavaRDD<File> filesJavaRDD, double networkCutOff, Set<String> domainsSet) {
        HashSet<String> domains = new HashSet<>(domainsSet);
        return filesJavaRDD.flatMap(file -> createEdges(file, networkCutOff, domains));
    }

    private static <U> Iterator<Edge<Integer>> createEdges(File file, double networkCutOff, HashSet<String> domainsAround) throws FileNotFoundException {
        ArrayList<Edge<Integer>> edges = new ArrayList<>();

        if (file.getName().endsWith(networkCutOff + "_frequency_patterns.txt")) {
            String[] fileNameParsed = file.getName().split("_");
            String domain = fileNameParsed[0]; // This is the domain around which the file is build
            if (domainsAround.contains(domain)) {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (!line.startsWith("#")) {
                        String[] lineParts = line.split("\t");
                        String domain2 = lineParts[0];

                        if(domainsAround.contains(domain2) && !domain2.equals(domain)) {
                            // the domain needs to be in the output around the domain of interest and it should not be the domain of interest
                            FPMNetworkNode node1 = new FPMNetworkNode(domain);
                            FPMNetworkNode node2 = new FPMNetworkNode(domain2);
                            Edge<Integer> edge = new Edge(node1.getId(), node2.getId(), 1);
                            edges.add(edge);
                        }
                    }
                }
                scanner.close();
            }
        }

        return edges.iterator();
    }

    /**
     * A method to get all the domains from the frequent pattern mining output which will be used to create the network
     */
    private HashMap<String, Double> getDomainsFromFile(File file, String domain) throws FileNotFoundException {
        // find the file that contains the FPM output for the domain of interest.
        // This is also used in writeFrequentPatternOutput()

        HashMap<String, Double> FPMDomains = new HashMap<>();
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()){
            String line = scanner.nextLine();
            if (!line.startsWith("#")) {
                String[] lineParts = line.split("\t");
                FPMDomains.put(lineParts[0], Double.valueOf(lineParts[2]));
            }
        }

        scanner.close();
//        FPMDomains.remove(domain); // remove the domain of around this was build itself.

        return FPMDomains;
    }

    /**
     * A method to gather the FMP results for one domain.
     */
    private void queryAroundDomain(String domain) throws Exception {
        // First check whether the FPM output for this domain is already present
        File outputFile = new File(domain + "_" + commandOptions.networkCutOff + "_frequency_patterns.txt");
        LOGGER.info("REMOVE LATER: " + domain + " " + outputFile.getName());

        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions,sc); // todo do something with the query thoroughly
        LOGGER.warn("!!!!!! FIX THE QUERY THOROUGHLY PARAMETER HERE !!!!!!!!!");
        domainPatternRecognition.setOperonHashMap(domainPatternRecognition.queryInputData(domain, true)); // Query all the input files for this domains
        domainPatternRecognition.runFrequentPatternMining(domain);



//        // if not: query around this domain and create the necessary FPM results
//        LOGGER.info("Querying for domain: " + domain);
//        // Creating the right command options to run the main for the specific domain
//        ArrayList<String> args = new ArrayList<>(Arrays.asList("-domain", domain,
//                "-outputDirectory", commandOptions.outputPath,
//                "-inputDirectory", commandOptions.inputDirectory,
//                "-minSupportFPMValues", String.valueOf(commandOptions.networkCutOff),
//                "-bidirectionalGapMinimum", String.valueOf(commandOptions.bidirectionalGapMin),
//                "-bidirectionalGapMaximum", String.valueOf(commandOptions.bidirectionalGapMax),
//                "-geneGap", String.valueOf(commandOptions.geneGap)));
//        if (commandOptions.bidirectional) {
//            args.add("-bidirectional");
//        }
//        if (commandOptions.recursiveInput) {
//            args.add("-recursiveInput");
//        }
//
//        String[] arguments = new String[args.size()];
//        arguments = args.toArray(arguments);
//
//        App.main(arguments);

    }

}
