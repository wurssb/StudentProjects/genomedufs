package nl.wur.ssb.DomainPatternRecognition.commandOptions;

import com.beust.jcommander.Parameter;import com.beust.jcommander.Parameters;
import java.io.File;import java.time.LocalDateTime;

    @Parameters(commandDescription = "Available options: ")
    public class CommandOptionsGeneric {

        public String commandLine;
//        public AnnotationResult annotResult;
        public LocalDateTime starttime = LocalDateTime.now();
//        public Domain domain = Store.createMemoryStore();

        @Parameter(names = "--help")
        public boolean help = false;

        @Parameter(names = {"-i", "-input"}, description = "Input file")
        public File input; // TODO To HDT directly
//        public HDT hdt; // TODO To HDT directly

        @Parameter(names = {"-e", "-endpoint"}, description = "SPARQL endpoint instead of input file")
        public String endpoint;

        @Parameter(names = {"-u", "-update"}, description = "SPARQL endpoint for update statements (default is endpoint address)")
        public String update = endpoint;

        @Parameter(names = {"-user", "-username"}, description = "Username for endpoint")
        public String username;

        @Parameter(names = {"-pass", "-password"}, description = "Password for endpoint")
        public String password;

        @Parameter(names = {"-o", "-output"}, description = "Output file")
        public File output;

        @Parameter(names = "-debug", description = "Debug mode")
        public boolean debug = false;

        public CommandOptionsGeneric() throws Exception {
        }
}
