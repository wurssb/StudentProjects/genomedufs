package nl.wur.ssb.DomainPatternRecognition.commandOptions;

import com.beust.jcommander.*;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Parameters(commandDescription = "Available options: ")
public class CommandOptions extends CommandOptionsGeneric {

    // These are the generic parameters to manage the threads and the input and output
    @Parameter(names = {"-threads"}, description = "Number of threads to use")
    public
    int jobs = 4;

    @Parameter(names = {"-inputDirectory"}, description = "Input file or directory", required = true, listConverter = StringListConverter.class)
    public
    List<String> inputDirectory; // not possible to name this -i or -input due to errors. // todo: make this to a list

    @Parameter(names = {"-recursiveInput"}, description = "Boolean, whether the input directory should be recursively searched for data ")
    public
    boolean recursiveInput = false;

    @Parameter(names = {"-outputDirectory"}, description = "Directory where output of the application will be stored", required = true)
    public
    String outputPath;

    // Parameters that have to do with querying and region / operon extraction
    @Parameter(names = {"-domain"}, description = "Domain of interest around which the first regions will be extracted. When using multiple domains as region criteria, choose least common domains as domain of interest", required = true)
    public
    String domain;

    @Parameter(names = {"-additionalDomains"}, description = "Additional domains that need to be present in the operon like structures", listConverter = StringListConverter.class)
    public
    List<String> additionalDomains = new ArrayList<>();

    @Parameter(names = {"-excludedDomains"}, description = "List of domains that will be excluded from the operons", listConverter = StringListConverter.class)
    public
    List<String> excludedDomains = new ArrayList<>();

    @Parameter(names = {"-db"}, description = "Database of interest")
    public
    String db = "http://gbol.life/0.1/db/pfam";

    @Parameter(names = {"-regionWidth"}, description = "Up and downstream distance around domain of interest")
    public
    int width = 20000;

    @Parameter(names = {"-geneGap"}, description = "Maximum allowed gap between genes to belong to the same operon")
    public
    int geneGap = 50;

    @Parameter(names = {"-provenanceOrigin"}, description = "Origin of provenance")
    public
    String originProv = "http://gbol.life/0.1/Prodigal/2_6_3";

    @Parameter(names = {"-bidirectional"}, description = "If true, the structures of operons will be assumed to lie around bidirectional promotors")
    public
    boolean bidirectional = false;

    @Parameter(names = {"-bidirectionalGapMaximum"}, description = "The maximum allowed gap between a section of genes on the reverse and forward strand to still be considered as the same operon")
    public
    int bidirectionalGapMax = 1000 ;

    @Parameter(names = {"-bidirectionalGapMinimum"}, description = "The minimum gap allowed between a section of genes on the reverse and forward strand to still be considered as the same operon")
    public
    int bidirectionalGapMin = 100 ;


    /**
     * Frequent pattern mining parameters
      */

    @Parameter(names = {"-doFrequentPatternMining"}, description = "If true, Frequent Pattern Mining will be carried out")
    public
    boolean doFrequentPatternMining = false;

    @Parameter(names = {"-minSupportFPMValues"}, description = "Values to be used (e.g. 0.2,0.4,0.6) as minimum support for patterns to be recognized by Frequency Pattern recognition. Order ascending",
            listConverter = DoubleListConverter.class)
    public
    List<Double> minSupportValues = new ArrayList<>(Arrays.asList(0.7));

    @Parameter(names = {"-minConfAssociation"}, description = "Minimum confidence for association rules to be recognized")
    public
    double minCon = 0.8;

    @Parameter(names = {"-getAssociationRules"}, description = "Boolean, whether association rules should be collected")
    public
    boolean doAssocationRules = false;

    @Parameter(names = {"-FPMNetwork"}, description = "Boolean, whether a network should be created from the FPM results of the domain of interest")
    public
    boolean doFPMNetwork = false;

    @Parameter(names ={ "-networkCutOff"}, description = "Double, the cut off value for when there is a significant association between two domains in a network")
    public
    double networkCutOff = 0.7;

    @Parameter(names = {"-saveRawFPMOutput"}, description = "All patterns found by the Frequent Pattern mining will be saved to a .txt file")
    public
    boolean saveRawFPMFiles = false;


    /**
     * Association rules & Trait detection
      */

    @Parameter(names = {"-minNumberOfOperons"}, description = "The minimum number of operons that need to be present before gathering association rules and trait detection." +
            "A low number of operons in the data will result in an extremely large FPMining output and result in out of memory errors.")
    public
    int minNumberOfOperons = 10 ;

    // TODO needs work...
    @Parameter(names = {"-detectTraits"}, description = "Boolean, whether the application should reduce the FPM mining output in to traits. Memory demanding when running on low support values")
    public
    boolean detectTraits = false;

    /**
     *    LDA Clustering
      */

    @Parameter(names = {"-numberLDAClustersFPM"}, description = "The number of clusters that will  be used during LDA clustering the operons using the domains found during frequent pattern mining e.g. 2,3",
            listConverter = IntListConverter.class)
    public
    List<Integer> LDAClusterKFreqPattern = new ArrayList<>(Arrays.asList(2,5)) ;

    // todo: alter this description
    @Parameter(names = {"-LDAVocabSize"}, description = "Values to be used as the vocabulary size (e.g. 10, 20) on which the topics will be clustered during LDA clustering",
            listConverter = IntListConverter.class)
    public
    List<Integer> LDAVocabSize = new ArrayList<>(Arrays.asList(5,10)) ;

    @Parameter(names = {"-NumberLDAClustersOperons"}, description = "The number of clusters that will be used during LDA clustering on the operons e.g. 2,3,4",
            listConverter = IntListConverter.class)
    public
    List<Integer> LDAClustersKOperons = new ArrayList<>(Arrays.asList(2,5));

    @Parameter(names = {"-LDAClusteringOperons"}, description = "Boolean, whether to perform LDA clustering on the operons")
    public
    boolean doLDAClusteringOperons = false;

    @Parameter(names = {"-LDAClusteringFPM"}, description = "Boolean, whether to perform LDA clustering based the domains found by the Frequent Pattern Mining")
    public
    boolean doLDAClusteringFPM = false;





    // TODO this is the default CommandOptions code ...
    public CommandOptions(String args[]) throws Exception {
        try {
            if (args.length == 0)
                throw new ParameterException("No parameter is given");

            new JCommander(this, args);
            this.commandLine = StringUtils.join(args, " ");

            if (this.help) {
                throw new ParameterException("");
            }

        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
    private static class DoubleListConverter implements IStringConverter<List<Double>> {
        @Override
        public List<Double> convert(String numberList) {
            String[] numbers = numberList.split(",");
            List<Double> doubles = new ArrayList<Double>();
            for (String element : numbers) {
                doubles.add(Double.valueOf(element));
            }
            return doubles;
        }
    }

    private static class IntListConverter implements IStringConverter<List<Integer>> {
        @Override
        public List<Integer> convert(String numberList) {
            String[] numbers = numberList.split(",");
            List<Integer> integers = new ArrayList<>();
            for(String element : numbers) {
                integers.add(Integer.parseInt(element)) ;
            }
            return integers ;
        }
    }

    private static class StringListConverter implements IStringConverter<List<String>> {
    @Override
        public List<String> convert(String stringList) {
        String[] strings = stringList.split(",");
        return new ArrayList<>(Arrays.asList(strings));
    }
    }

}
