package nl.wur.ssb.DomainPatternRecognition.traits;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.FPM.FreqPatternsParallel;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.fpm.FPGrowth;

import java.io.*;
import java.util.*;

public class TraitDetector implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(TraitDetector.class.getName());
    private JavaRDD<FPGrowth.FreqItemset<String>> freqItemSet;
    private double supportValue;
    private long frequencyThreshold;

    private HashMap<Integer, JavaRDD<TraitNode>> treeLayersRDD = new HashMap<>();
    private HashMap<String, String> domainDescriptions = new HashMap<>();
    private JavaRDD<TraitNode> significantNodes;
    private JavaRDD<TraitNode> allNodesRDD;

    public TraitDetector(JavaRDD<FPGrowth.FreqItemset<String>> freqItemSet, String domainOfInterest, double supportValue, int numberOfOperons) throws IOException {

        this.freqItemSet = freqItemSet.filter(set -> set.javaItems().contains(domainOfInterest)); // Here only the patterns with the domain of interest are retained.
        this.supportValue = supportValue;
        this.frequencyThreshold = Math.round(numberOfOperons / supportValue);

        detectTraits();

    }

    private void detectTraits() {
        LOGGER.info("Starting trait detection");
        boolean createdSeedNode = createSeedNode();

        if (createdSeedNode) {
            ArrayList<Integer> layerKeys = createPatternTree();
            connectNodesInTree(layerKeys);
            mergeLayerRDDs();
            LOGGER.info("Checking all nodes on significance.");
            significantNodes = allNodesRDD.filter(TraitNode::isSignificant);

            // Start writing the output to a file
            loadDomainDescriptions();
            writeDetectionOutput();
        }
    }


    /**
     * Method to extract the pattern containing only the domain of interest from the frequent item set and to create
     * the seed node.
     */
    private boolean createSeedNode() {
        List<FPGrowth.FreqItemset<String>> seedDomain = freqItemSet.filter(set -> set.javaItems().size() == 1).collect();
        if (seedDomain.size() == 1) {
            FPGrowth.FreqItemset<String> seedItemSet = seedDomain.get(0);
            TraitNode seedNode = new TraitNode(seedItemSet.javaItems(), seedItemSet.freq(), this.frequencyThreshold);
            JavaRDD<TraitNode> tempRDD = FreqPatternsParallel.parallelizeTraitNodeList(new ArrayList<>(Collections.singletonList(seedNode)));
            treeLayersRDD.put(1, tempRDD);

            return true;
        } else {

            System.err.println(">>> FAILED: SeedNode creation failed\n");

            return false;
        }
    }

    /**
     * A method to create the tree structure from the frequent pattern mining results.
     */
    private ArrayList<Integer> createPatternTree() {
        LOGGER.info("Creating the pattern tree");
        ArrayList<Integer> layerKeys = new ArrayList<>();
        layerKeys.add(1);

        boolean nextLevel = true;
        int currentLevel = 2;

        while (nextLevel) {
            nextLevel = createNextLevel(currentLevel);
            if (nextLevel) {
                layerKeys.add(currentLevel);
            }
            currentLevel += 1;
        }
        return layerKeys;
    }


    /**
     * A method to connect the traitnodes in the tree, by giving all of them an RDD with the traitnodes that contain
     * all their 'offspring'. Their Offsping are all the nodes that they are contained in.
     *
     * @param layerKeys an ArrayList of integers representing all the layers (pattern sizes) in the patter tree.
     */
    private void connectNodesInTree(ArrayList<Integer> layerKeys) {
        LOGGER.info("Starting with the connecting of the tree.");
        layerKeys.sort(Collections.reverseOrder());

        for (int layerKey : layerKeys) {
            JavaRDD<TraitNode> currentNodesRDD = treeLayersRDD.get(layerKey);

            if (layerKey == layerKeys.size()) {
                // This means that you are at the bottom layer, where the nodes have no children.
                // This also means that the frequency of these notes is already the correct count

                JavaRDD<TraitNode> updatedCurrentLayer = currentNodesRDD.map(this::correctedCountIsFrequency);
                treeLayersRDD.put(layerKey, updatedCurrentLayer);

            } else {
//                LOGGER.info("Starting with layer: " + layerKey);
                // update the layer so that all nodes contain their children.
                Iterator<TraitNode> layerBelow = treeLayersRDD.get((layerKey + 1)).toLocalIterator();


                // for every pattern in the layer below (so this is a bigger pattern). check whether it might be a child
                // node to the nodes in the current layer.
                while (layerBelow.hasNext()) {
                    TraitNode possibleChild = layerBelow.next();
                    currentNodesRDD = currentNodesRDD.map(node -> {
                        if (possibleChild.getPattern().containsAll(node.getPattern())) {
                            node.addChild(possibleChild); // Add the direct children.
                            // Add nodes to the list containing all "offspring" of a pattern
//                            node.addNodeToAllOffSpring(possibleChild);
//                            node.addSetOfNodesToAllOffSpring(possibleChild.getAllOffSpring());
                            return node;
                        } else {
                            return node;
                        }
                    });
                }

                // todo: here I have found all my children.

                // Now that we have all the child relationships. it is time to calculate the corrected counts for
                // every node.
                currentNodesRDD = currentNodesRDD.map(node -> {
                    // todo: to this function add that it will first get all the distinct nodes.
                    // todo: here add also all the direct children of the node to the RDD

//                    node.gatherAllOffSpring();
//                    printContentRDD(node.getAllOffspring());
                    node.calculateCorrectedCount();
                    return node;
                });
                // The updated nodes are put back into the HashMap.
                treeLayersRDD.put(layerKey, currentNodesRDD);
            }
        }
    }

    /**
     * Method to set the corrected weight of a node to the frequency of the pattern. This should be done when a
     * node has no children.
     *
     * @param traitNode a TraitNode object that has no children
     * @return the TraitNode object in an iterator.
     */
    private TraitNode correctedCountIsFrequency(TraitNode traitNode) {
        traitNode.setCorrectedCount(traitNode.getPatternFrequency());
        traitNode.setEndNote(true);
        return traitNode;
    }

    /**
     * A method to create all nodes from a new level (size) in the patterns tree.
     *
     * @param level integer, the level or pattern size that we are currently working on.
     * @return boolean, this is false when there are no more patterns of this size.
     */
    private boolean createNextLevel(int level) {
        JavaRDD<FPGrowth.FreqItemset<String>> nodesInLevel = freqItemSet.filter(set -> set.javaItems().size() == level);

        if (nodesInLevel.toLocalIterator().hasNext()) {

            // There are new patterns in this next level.
            // Here we create the new nodes.
            JavaRDD<TraitNode> levelNodes = nodesInLevel.map(this::createOneNodeFromSet);

            // Now add these nodes to the right layer of the tree pattern
            this.treeLayersRDD.put(level, levelNodes);
            return true; // This level still contained some nodes, so go on to check for a next level

        } else {
            return false;
            // There are no nodes left of this size, so the end of the tree is reached.
        }
    }

    /**
     * Method to create and return a TraitNode object from a Frequent ItemSet.
     *
     * @param itemSet Frequent item set
     * @return a TraitNode.
     */
    private TraitNode createOneNodeFromSet(FPGrowth.FreqItemset<String> itemSet) {
        TraitNode newNode = new TraitNode(itemSet.javaItems(), itemSet.freq(), this.frequencyThreshold);
        return newNode;
    }

    /**
     * Method to load the domain description HashMap from the serialized file .
     */
    private void loadDomainDescriptions() {
        try {
            FileInputStream fileInputStream = new FileInputStream(DomainPatternRecognition.fileNameDescriptionLookUp);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            this.domainDescriptions = (HashMap) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * A method to merge all the RDDs for one layer into one big RDD that is used to filter out all the nodes that are
     * significant.
     */
    private void mergeLayerRDDs() {
        LOGGER.info("Merging the layer RDDs into one.");
        this.allNodesRDD = FreqPatternsParallel.createEmptyRDD();
        for (int layer : this.treeLayersRDD.keySet()) {
            this.allNodesRDD = FreqPatternsParallel.joinRDDs(this.allNodesRDD, this.treeLayersRDD.get(layer));
        }
    }

    /**
     * Method to write the found significant patterns.
     */
    public void writeDetectionOutput() {
        String fileName = DomainPatternRecognition.outputPath + "/FrequentPatterns_" + supportValue + ".txt";

        // get the domain description look up
        if (domainDescriptions == null) {
            LOGGER.error(">>> FAILED: failed to write output for significant patterns in frequency pattern mining output.\n" +
                    "Unable to load domain descriptions: " + DomainPatternRecognition.fileNameDescriptionLookUp);
        } else {

            try {
                FileWriter fileWriter = new FileWriter(fileName);
                Iterator<TraitNode> traitNodeIterator = significantNodes.toLocalIterator();
                while (traitNodeIterator.hasNext()) {
                    TraitNode traitNode = traitNodeIterator.next();

                    List<String> domainsInPattern = traitNode.getPattern();

                    // Get all the domains from the pattern
                    String domains = String.join(",", domainsInPattern);

                    // Get the descriptions of the domains
                    ArrayList<String> domainDescriptionsList = new ArrayList<>();
                    for (String domain : domainsInPattern) {
                        domainDescriptionsList.add(domainDescriptions.get(domain));
                    }
                    String domainDescriptionsString = String.join(",", domainDescriptionsList);

                    StringBuilder writeLine = new StringBuilder();
                    writeLine.append(domains);
                    writeLine.append("\t");
                    writeLine.append(domainDescriptionsString);
                    writeLine.append("\t");
                    writeLine.append(traitNode.getPatternFrequency());
                    writeLine.append("\n");

                    fileWriter.write(writeLine.toString());
                }

                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // function that can help when debugging.
    private void printTreeStructureRDD() {
        System.out.println("printing the tree structure RDD");
        for (int key : this.treeLayersRDD.keySet()) {
            Iterator<TraitNode> iterator = treeLayersRDD.get(key).toLocalIterator();
            while (iterator.hasNext()) {
                iterator.next().printNode();
            }
        }
    }

    private void printContentRDD(JavaRDD<TraitNode> rdd) {
        Iterator<TraitNode> iterator = rdd.toLocalIterator();
        while (iterator.hasNext()) {
            iterator.next().printNode();
        }
    }
}
