package nl.wur.ssb.DomainPatternRecognition.traits;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;

/**
 * Class to represent a node in the pattern tree from the trait class
 */
public class TraitNode implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(TraitNode.class.getName());
    private ArrayList<String> pattern;
    private long patternFrequency;
    private HashSet<TraitNode> childNodes = new HashSet<>();
    private long correctedCount;
    private boolean endNote;
    private long frequencyThreshold;
    private boolean significant;
    private HashMap <ArrayList<String>, Long> offSpringFreqs = new HashMap<>();


//    private HashSet<TraitNode> allOffSpring = new HashSet<>(); // todo make this field unnecessary
//    private JavaRDD<TraitNode> allOffspring;


    public TraitNode(List<String> pattern, long patternFrequency, long frequencyThreshold){
        this.pattern = new ArrayList<>(pattern);
        Collections.sort(this.pattern); // Always sorting the list makes it possible to compare two lists with an
        // equals() instead of wiht a containsAll(). containsAll() has a worse time complexity.
        this.patternFrequency = patternFrequency;
        this.frequencyThreshold = frequencyThreshold ;
    }

    /**
     * A method to calculate a corrected count for the TraitNode, considering the frequencies of TraitNodes objects
     * of which the pattern of this TraitNode is completely contained.
     */
    // todo: this is not yet correct but it does definitely seems to be al lot faster!!
    public void calculateCorrectedCount() {
        if (this.childNodes.size() != 0) { // There are patterns in which this traitNode is contained.
            for (TraitNode childNode : childNodes) {
                // for every child node, their frequency is put into hashmap as well as their children.
                // overlapping patterns will not be considered here as is appropriate for this situation.
                offSpringFreqs.put(childNode.getPattern(), childNode.getCorrectedCount());
                offSpringFreqs.putAll(childNode.getOffSpringFreqs());
            }

            long sumOffSpring = 0 ;
            for (long frequency : offSpringFreqs.values()) {
                sumOffSpring += frequency;
            }

            this.correctedCount = this.patternFrequency - sumOffSpring;
            significant = this.correctedCount >= this.frequencyThreshold;

        } else { // There are no patterns in which this traitNode is contained. so this makes the node an end node
            // and also immediately makes it significant and means that the offSpringCorrectedValuesSum = 0.
            this.endNote = true;
            this.correctedCount = this.patternFrequency;
            this.significant = true;
        }
    }

    /**
     * Method to add a TraitNode object to the childNote HashSet
     * @param childNode, TraitNode object
     */
    public void addChild(TraitNode childNode) {
        childNodes.add(childNode);
    }


    public void printNode() {
        ArrayList<String> children = new ArrayList<>();
        childNodes.forEach(node -> children.add(node.toString()));

        StringBuilder allChildString = new StringBuilder() ;
        for (TraitNode childNode : childNodes) {
            allChildString.append(childNode.toString());
            allChildString.append(" ");
            allChildString.append(childNode.correctedCount);
            allChildString.append(";");
        }

//        System.out.println("Pattern: " + pattern + "\tChildren: " + String.join(",", children) + "\tcorrected count: " + this.correctedCount + "\tFrequency: " + this.frequency + "\tAll children: " +this.allOffSpring);
        System.out.println(pattern + "\t" + this.correctedCount + "\t" + this.patternFrequency + "\t" + this.childNodes);
    }



    public ArrayList<String> getPattern() {
        return pattern;
    }

    public long getPatternFrequency() {
        return patternFrequency;
    }

    public void setPatternFrequency(long patternFrequency) {
        this.patternFrequency = patternFrequency;
    }

    public String toString(){
        return String.join(" ", this.pattern) ;
    }

    public long getCorrectedCount() {
        return correctedCount;
    }

    public void setCorrectedCount(long correctedCount) {
        this.correctedCount = correctedCount;
    }

    public boolean isEndNote() {
        return endNote;
    }

    public void setEndNote(boolean endNote) {
        this.endNote = endNote;
    }

    // Overwrite the hashcode method.
    @Override
    public int hashCode() {
        return Objects.hash(this.getPattern());
    }


    // Overwrite the equals() method to have two traitNodes to be equal if their patterns are the same
    @Override
    public boolean equals(Object object) {
        // the object is compared to itself
        if (object == this) {
            return true;
        }
        // the object is not an not a TraitNode
        if (!(object instanceof TraitNode)) {
            return false;
        }

        // Compare the patterns here. This only happens when the object is a TraitNode.
        TraitNode traitNode = (TraitNode) object;
        return traitNode.getPattern().equals(this.getPattern()); // Both lists are sorted so an equal is possible.
    }

    public boolean isSignificant() {
        return significant;
    }

    public void setSignificant(boolean significant) {
        this.significant = significant;
    }

    public HashMap<ArrayList<String>, Long> getOffSpringFreqs() {
        return offSpringFreqs;
    }

    public void setOffSpringFreqs(HashMap<ArrayList<String>, Long> offSpringFreqs) {
        this.offSpringFreqs = offSpringFreqs;
    }

    //    public ArrayList<TraitNode> getChildNodes() {
//        return childNodes;
//    }
//
//    public void setChildNodes(ArrayList<TraitNode> childNodes) {
//        this.childNodes = childNodes;
//    }


//    public void addChildren (List<TraitNode> children ) {
//        childNodes.addAll(children);
//    }
//
//    /**
//     * A method to loop through all child nodes to get the corrected frequency of this node.
//     */
//    public void calculateCorrectedCount() {
//
//        if (allOffSpring.size() == 0) {
//            this.endNote = true;
//            this.correctedCount = this.patternFrequency;
//            this.significant = true;
//        } else {
//            this.endNote = false;
//            long childFreqs = sumFrequencyChildren(allOffSpring);
//            this.correctedCount = this.patternFrequency - childFreqs;
//            if (this.correctedCount > 0) {
//                if (this.correctedCount >= this.frequencyThreshold) {
//                    this.significant = true;
//                } else {
//                    this.significant = false;
//                }
//            } else {
//                // there are certain circumstances where the corrected count might become negative.
//            }
//        }
//    }
//
//    private long sumFrequencyChildren(HashSet<TraitNode> allOffSpring) {
//        long sum = 0;
//        for (TraitNode node : allOffSpring) {
//            sum += node.correctedCount;
//        }
//        this.sumAllChildren = sum;
//        return sum;
//    }

//    public void addNodeToAllOffSpring(TraitNode childNode) {
//        this.allOffSpring.add(childNode);
//    }
//
//    public void addSetOfNodesToAllOffSpring(HashSet<TraitNode> traitNodeHashSet) {
//        for (TraitNode traitNode : traitNodeHashSet) {
//            if (!allOffSpring.contains(traitNode)) {
//                this.allOffSpring.add(traitNode);
//            }
//        }
//
////        this.allOffSpring.addAll(traitNodeHashSet);
//    }



//    public HashSet<TraitNode> getAllOffSpring() {
//        return allOffSpring;
//    }
//
//    public void setAllOffSpring(HashSet<TraitNode> allOffSpring) {
//        this.allOffSpring = allOffSpring;
//    }



//    public JavaRDD<TraitNode> getAllOffspring() {
//        return allOffspring;
//    }
//
//    public void setAllOffspring(JavaRDD<TraitNode> allOffspring) {
//        this.allOffspring = allOffspring;
//    }
//
//    public void gatherAllOffSpring() {
//        System.out.println("one Sample ");
//        ArrayList<JavaRDD<TraitNode>> allOffSpringRDDs = new ArrayList<>();
//        allOffSpringRDDs.add(FreqPatternsParallel.parallelizeTraitNodeList(this.childNodes));
//        for (TraitNode childNode : this.childNodes) {
//            allOffSpringRDDs.add(childNode.allOffspring);
//        }
//        // todo: joining the list of RDDs is not going very well.
//
//        // todo: when testing also consider the fact that I have not yet updated the count method so the results will be wrong on that part.
//        this.allOffspring = FreqPatternsParallel.joinListRDDs(allOffSpringRDDs);
////        System.out.println(this + "  printing the allOffsping RDD");
////        Iterator<TraitNode> iterator = this.allOffspring.toLocalIterator();
////        while(iterator.hasNext()) {
////            iterator.next().printNode();
////        }
//
//
//
//    }
}
