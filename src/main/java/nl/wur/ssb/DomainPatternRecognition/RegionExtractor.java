package nl.wur.ssb.DomainPatternRecognition;


import nl.wur.ssb.DomainPatternRecognition.objects.Gene;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.objects.Region;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


/**
 * Class to save all regions found by the SPARQL query.
 * Regions will be saved as Region objects
 * <p>
 * Author: Sanne de Graaf
 * email: sanne.degraaf@wur.nl
 */
public class RegionExtractor {
    private boolean bidirectional;
    private int geneGap;
    private int bidirectionalGapMax;
    private int bidirectionalGapMin;
    private HashMap<String, Region> regionHashMap = new HashMap<>();
    private HashMap<String, Operon> operonHashMap = new HashMap<>();


    /**
     * Constructor for QueryResults
     */
    public RegionExtractor(boolean bidirectional, int geneGap, int bidirectionalGapMax, int bidirectionalGapMin) {
        this.bidirectional = bidirectional;
        this.geneGap = geneGap;
        this.bidirectionalGapMax = bidirectionalGapMax;
        this.bidirectionalGapMin = bidirectionalGapMin;
    }

    /**
     * Method to clean up regions to only retain genes which could be in a possible operon.
     */
    public void extractOperonsFromRawRegions() {
        if (bidirectional) {
            extractBidirectional();
        } else {
            extractUnidirectional();
        }
    }


    // TODO think about a graph solution with distance cutting...
    private void extractBidirectional() {
        for (String region_key : this.getRegionHashMap().keySet()) {
            Region region = this.getRegionHashMap().get(region_key);
            region.orderGenes();

            // Create new operon for the region
            Operon operon = new Operon(region.getInitialGene(), region.getSample(), region.getContig());
            operon.setScientificName(region.getScientificName());
            String baseStrand = region.getInitialGene().getStrand();

            if (baseStrand.contains("Forward")) {
                boolean forwardIn = false;
                // if the seed gene lies on the forward strand, the part of the operon on the reverse lies before it.

                for (Gene gene : region.getGenes()) {

                    // The gene is the seed gene.
                    if (gene.getGeneID().equals(region.getInitialGene().getGeneID())) {
                        operon.setContainsInitialGene(true);
                        operon.addGene(gene);
                        forwardIn = true;
                    }

                    // We find a forward gene and it might be part of the operon.
                    else if (gene.getStrand().contains("Forward")) {
                        operon.addGene(gene);
                        forwardIn = true;
                    }

                    // We have not yet come across the initial gene, there are already some genes in the operon that
                    // are on the forward strand and these now need to be removed
                    else if (gene.getStrand().contains("Reverse") && !operon.getContainsInitialGene() && forwardIn) {
                        operon.clear();
                        forwardIn = false;
                        operon.addGene(gene);
                    }

                    // We have not yet come across the initial gene, but this gene might lie on the reverse part before it.
                    // There are not yet genes in there that are on the forward strand.
                    else if (gene.getStrand().contains("Reverse") && !operon.getContainsInitialGene()) {
                        operon.addGene(gene);
                    }

                    // Initial gene is in the operon and you find a gene that is in the opposite direction.
                    // This is the end of the operon.
                    else if (!gene.getStrand().equals(baseStrand) && operon.getContainsInitialGene()) {
                        break;
                    }
                    ArrayList<String> after = new ArrayList<>();
                    operon.getGenes().forEach(gene1 -> after.add(gene1.getGeneID()));
                }
            }

            if (baseStrand.contains("Reverse")) {
                boolean forwardIn = false;
                for (Gene gene : region.getGenes()) {

                    // The gene is the seed gene.
                    if (gene.getGeneID().equals(region.getInitialGene().getGeneID())) {
                        operon.setContainsInitialGene(true);
                        operon.addGene(gene);
                    }
                    // We find a reverse gene and it might be part of the operon.
                    else if (gene.getStrand().contains("Reverse") && !forwardIn) {
                        operon.addGene(gene);
                    }

                    // We have not yet come across the initial gene, but this gene lies on the forward strand.
                    // all can be removed from the operon
                    else if (gene.getStrand().contains("Forward") && !operon.getContainsInitialGene()) {
                        operon.clear();
                    }

                    // We have the reverse part with the seed gene, now add the forward part
                    else if (gene.getStrand().contains("Forward") && operon.getContainsInitialGene()) {
                        forwardIn = true;
                        operon.addGene(gene);
                    }

                    // both the reverse and forward part of the operon are already present and now a new reverse part
                    // occurs in the region which means that it is the end of the operon.
                    else if (gene.getStrand().contains("Reverse") && forwardIn) {
                        break;
                    }

                }
            }
            operon.orderGenes();
            operon.enforceMaximumGapThresholdBi(geneGap, bidirectionalGapMax, bidirectionalGapMin);
            getOperonHashMap().put(region.getInitialGene().getGeneID(), operon);
        }

    }

    private void extractUnidirectional() {
        for (String region_key : this.getRegionHashMap().keySet()) {
            Region region = this.getRegionHashMap().get(region_key);
            region.orderGenes();

            // Create new operon for the region
            Operon operon = new Operon(region.getInitialGene(), region.getSample(), region.getContig());
            operon.setScientificName(region.getScientificName());
            String baseStrand = region.getInitialGene().getStrand();


            for (Gene gene : region.getGenes()) {

                if (gene.getGeneID().equals(region.getInitialGene().getGeneID())) {
                    operon.setContainsInitialGene(true);
                    operon.addGene(gene);
                    continue;
                }

                // Have not yet reached initial gene, but this gene is on the same strand as intial
                if (gene.getStrand().equals(baseStrand) && !operon.getContainsInitialGene()) {
                    operon.addGene(gene);
                }

                // Gene is not on the same strand and we have not yet reached the gene of interest of the operon.
                else if (!gene.getStrand().equals(baseStrand) && !operon.getContainsInitialGene()) {
                    operon.clear();
                }

                // Initial gene is in operon and you find one that is also on the same strand
                else if (gene.getStrand().equals(baseStrand) && operon.getContainsInitialGene()) {
                    operon.addGene(gene);
                }

                // Initial gene is in the operon and you find a gene that is in the opposite direction.
                else if (!gene.getStrand().equals(baseStrand) && operon.getContainsInitialGene()) {
                    break;
                }
            }
            operon.orderGenes();
            operon.setGenes(operon.enforceMaximumGapThresholdUni(operon.getGenes(), geneGap));
            getOperonHashMap().put(region.getInitialGene().getGeneID(), operon);
        }
    }

    /**
     * Method to merge overlapping operons into one operon
     * Overlapping operons can occur when the domain of interest is present in two neighbouring genes.
     */
    public void mergeOverlappingOperons() {
        HashSet<String> operonsToRemove = new HashSet<>();
        HashSet<Tuple2<String, Operon>> operonsToAdd = new HashSet<>();

        for (String key : operonHashMap.keySet()) {
            Operon baseOperon = operonHashMap.get(key);

            int startBase = baseOperon.getGenes().get(0).getBeginPosition();
            int endBase = baseOperon.getGenes().get(baseOperon.getGenes().size() - 1).getEndPosition();

            // Loop through all operons again
            for (String secondKey : operonHashMap.keySet()) {
                Operon secondOperon = operonHashMap.get(secondKey);

                if (!baseOperon.getInitialGene().equals(secondOperon.getInitialGene()) && // not comparing the exact same operon
                        baseOperon.getContig().equals(secondOperon.getContig()) && // both operons lie on the same contig
                        baseOperon.getSample().equals(secondOperon.getSample()) && // both operons are also in the same samle
                        !(operonsToRemove.containsAll(Arrays.asList(key, secondKey)))) // check if the comparison has not already been made in the other direction
                {

                    int startOther = secondOperon.getGenes().get(0).getBeginPosition();
                    int endOther = secondOperon.getGenes().get(secondOperon.getGenes().size() - 1).getEndPosition();

                    // See whether they might overlap
                    // If either the beginning or end of the other operon is inside the base operon, they overlap and
                    // need to be merged.
                    boolean startContained = startOther >= startBase && startOther <= endBase;
                    boolean endContained = endOther >= startBase && endOther <= endBase;

                    if (startContained || endContained) {
                        // The operons that overlap are here.
                        // put both in the to remove list
                        operonsToRemove.addAll(Arrays.asList(key, secondKey));

                        // Merge the genes from both operons in such a way that there are no two objects reprenting the
                        // same gene.
                        ArrayList<Gene> mergedGenes = new ArrayList<>();
                        mergedGenes.addAll(baseOperon.getGenes());
                        ArrayList<String> geneIDs = new ArrayList<>();
                        baseOperon.getGenes().forEach(gene -> geneIDs.add(gene.getGeneID()));

                        for (Gene gene : secondOperon.getGenes()) {
                            if (!geneIDs.contains(gene.getGeneID())) {
                                mergedGenes.add(gene);
                            }
                        }

                        // create new merged Operon. The information from the baseOperon is used.
                        Operon mergedOperon = new Operon(baseOperon.getInitialGene(), baseOperon.getSample(), baseOperon.getContig());

                        // Add the genes to the new operon
                        for (Gene gene : mergedGenes) {
                            mergedOperon.addGene(gene);
                        }
                        mergedOperon.orderGenes();
                        Tuple2<String, Operon> mergedTuple = new Tuple2<>(baseOperon.getInitialGene().getGeneID(), mergedOperon);
                        operonsToAdd.add(mergedTuple);
                    }
                }
            }
        }
        // remove the necessary operons from the operonhashmap
        for (String operonKey : operonsToRemove) {
            operonHashMap.remove(operonKey);
        }

        // add the newly merged operons.
        for (Tuple2<String, Operon> tuple : operonsToAdd) {
            operonHashMap.put(tuple._1, tuple._2);
        }

    }

    public void printOperonHashMap() {
        for (String operonKey : getOperonHashMap().keySet()) {
            System.out.println("\nOperon Seed: " + operonKey);
            Operon operon = getOperonHashMap().get(operonKey);
            operon.printGenesInOperon();
        }
    }


    public HashMap<String, Region> getRegionHashMap() {
        return regionHashMap;
    }

    public void setRegionHashMap(HashMap<String, Region> regionHashMap) {
        this.regionHashMap = regionHashMap;
    }

    public HashMap<String, Operon> getOperonHashMap() {
        return operonHashMap;
    }

    public void setOperonHashMap(HashMap<String, Operon> operonHashMap) {
        this.operonHashMap = operonHashMap;
    }


}
