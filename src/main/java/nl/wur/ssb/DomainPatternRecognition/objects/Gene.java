package nl.wur.ssb.DomainPatternRecognition.objects;

import java.io.Serializable;

/**
 * Class to contain a Gene and all its information
 * <p>
 * Author: Sanne de Graaf
 * email: sanne.degraaf@wur.nl
 */

public class Gene implements Serializable {
    private String geneID;
    private String strand;
    private int beginPosition;
    private int endPosition;
    private static final long serialVersionUID = 302299641467781752L ;

    /**
     * Constructor for the gene class
     *
     * @param geneID IRI identifying the gene
     */
    public Gene(String geneID) {
        this.geneID = geneID;
    }


    /**
     * Method to add location data of a gene to the object. This includes info on strand and begin and end position.
     *
     * @param strand,        String with IRI containing the strand on which gene is located.
     * @param beginPosition, integer: start of the gene
     * @param endPosition,   integer: end of the gene
     */
    public void addGeneLocationInfo(String strand, int beginPosition, int endPosition) {
        this.setBeginPosition(beginPosition);
        this.setEndPosition(endPosition);
        this.setStrand(strand);
    }

    // GET AND SET METHODS BELOW

    public String getGeneID() {
        return geneID;
    }

    public void setGeneID(String geneID) {
        this.geneID = geneID;
    }

    public String getStrand() {
        return strand;
    }

    public void setStrand(String strand) {
        this.strand = strand;
    }

    public int getBeginPosition() {
        return beginPosition;
    }

    public void setBeginPosition(int beginPosition) {
        this.beginPosition = beginPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }
}
