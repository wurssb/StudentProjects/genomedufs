package nl.wur.ssb.DomainPatternRecognition.objects;

import scala.Tuple2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Class to contain a region surrounding a gene / domain of interest
 * A region can be constructed for every sample (genome of an organism)
 *
 * Author: Sanne de Graaf
 * email: sanne.degraaf@wur.nl
 */
public class Region implements Serializable {
    private ArrayList<Gene> genes = new ArrayList<>() ;
    private Gene initialGene  ;
    private static final long serialVersionUID = 302299641467781752L;
    private String sample ;
    private String contig ;
    private String scientificName;


    public Region(Gene initGene, String sample, String contig) {
        this.setInitialGene(initGene);
        this.sample = sample;
        this.contig = contig;

    }

    /**
     * Method to order the genes in a region on their begin position.
     */
    public void orderGenes() {
        List<Tuple2<Integer, Gene>> sortingList = new ArrayList<>() ;

        for (Gene gene : this.genes) {
            Tuple2<Integer, Gene > tuple = new Tuple2<>(gene.getBeginPosition(), gene) ;
            sortingList.add(tuple) ;
        }

        Comparator<Tuple2<Integer, Gene>> comparator = new Comparator<Tuple2<Integer, Gene>>() {
            @Override
            public int compare(Tuple2<Integer, Gene> tupleA, Tuple2<Integer, Gene> tupleB) {
                return tupleA._1.compareTo(tupleB._1);
            }
        };

        Collections.sort(sortingList, comparator) ;

        ArrayList<Gene> sortedGenes = new ArrayList<>() ;
        for (Tuple2<Integer, Gene> tuple : sortingList) {
            sortedGenes.add(tuple._2);
        }

        this.setGenes(sortedGenes);

    }


    /**
     * Method to append a gene to the region in the genes ArrayList
     *
     * @param gene Gene object to be added to the Region
     */
    public void addGene(Gene gene) {
        this.genes.add(gene) ;
    }

    public void printGenesRegion(){
        for (Gene gene : this.genes) {
            System.out.println(gene.getGeneID() + "\t" +gene.getBeginPosition() + "\t" + gene.getStrand());
        }
    }

    public void removeGene(Gene gene) {this.genes.remove(gene); }



    // GETTERS AND SETTERS
    public ArrayList<Gene> getGenes() {
        return genes;
    }

    public void setGenes(ArrayList<Gene> genes) {
        this.genes = genes;
    }

    public Gene getInitialGene() {
        return initialGene;
    }

    public void setInitialGene(Gene initialGene) {
        this.initialGene = initialGene;
    }


    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public String getContig() {
        return contig;
    }

    public void setContig(String contig) {
        this.contig = contig;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }
}
