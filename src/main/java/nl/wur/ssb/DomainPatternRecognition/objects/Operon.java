package nl.wur.ssb.DomainPatternRecognition.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

/**
 * Class to contain an operon structure. This object stores the genes as gene objects.
 * <p>
 * Author: Sanne de Graaf
 * email: sanne.degraaf@wur.nl
 */
public class Operon implements Serializable {
    private ArrayList<Gene> genes = new ArrayList<>();
    private ArrayList<String> domains = new ArrayList<>();
    private Gene initialGene;
    private Boolean containsInitialGene = false;
    private Boolean leftEndOperon = false;
    private Boolean rightEndOperon = false;
    private String sample;
    private String contig;
    private String scientificName;
    private HashSet<String> uniqueDomainString;
    private static final long serialVersionUID = 302299641467781752L;

    public Operon(Gene initialGene, String sample, String contig) {
        this.setInitialGene(initialGene);
        this.sample = sample;
        this.contig = contig;
    }

    /**
     * Method to add a gene ID to the operon structure
     *
     * @param gene String, ID of the gene belonging to this operon.
     */
    public void addGene(Gene gene) {
        this.genes.add(gene);
    }


    /**
     * Method to clear the arrayList of the operon object containing the genesIDs in operon.
     */
    public void clear() {
        this.genes.clear();
    }


    /**
     * Method to enforce the gap threshold between normal genes and for an operon with a bidirectional promoter.
     * The operons have already been cleaned up by methods from the Region Extractor, namely, extractBidirectional and
     * extractUnidirectional.
     *
     * @param gapThreshold         int, the maximum allowed gap between two genes on the same strand
     * @param promoterGapThresholdMax int, the maximum allowed gap between the last reverse and first forward gene
     */
    public void enforceMaximumGapThresholdBi(int gapThreshold, int promoterGapThresholdMax, int promoterGapThresholdMin) {
        Gene seedGene = findInitialGeneInOperon();

          // check if the operon will be bidirectional. If not, the genes in the 'wrong' directional are removed
        // from the this.genes.
        boolean isBidirectional = enforceBidirectional(seedGene, promoterGapThresholdMax, promoterGapThresholdMin);

        if (isBidirectional) {
            // split the operon into the forward and reverse part, so that these can be handled separately.

            ArrayList<Gene> forwardGenes = new ArrayList<>();
            ArrayList<Gene> reverseGenes = new ArrayList<>();

            for (Gene gene : genes) {
                if (gene.getStrand().contains("Forward")) {
                    forwardGenes.add(gene);
                } else {
                    reverseGenes.add(gene);
                }
            }

            if (seedGene.getStrand().contains("Forward")) {
                // The seed gene lies on the forward strand
                ArrayList<Gene> forwardGenesWithoutGaps = enforceMaximumGapThresholdUni(forwardGenes, gapThreshold);
                ArrayList<Gene> reverseGenesWithoutGaps = new ArrayList<>();

                // now check whether the first forward genes are still in. If so, append the reverse part of the
                // reverse part of the operon to to the genes.
                if (forwardGenesWithoutGaps.get(0).equals(forwardGenes.get(0))) {
                    reverseGenesWithoutGaps = shrinkReverseGenesNonSeed(reverseGenes, gapThreshold);
                }
                genes.clear(); // clear the genes that were in it so far and add the right ones
                genes.addAll(reverseGenesWithoutGaps);
                genes.addAll(forwardGenesWithoutGaps);
            }

            else {
                // The seed gene lies on the reverse strand
                ArrayList<Gene> reverseGenesWithoutGap = enforceMaximumGapThresholdUni(reverseGenes, gapThreshold);
                ArrayList<Gene> forwardGenesWithoutGap = new ArrayList<>();

                // Now check whether the last gene of the reverse is retained. If not the forward part will not have
                // any change to be contained.
                if (reverseGenesWithoutGap.get(reverseGenesWithoutGap.size()-1).equals(reverseGenes.get(reverseGenes.size()-1))) {
                    forwardGenesWithoutGap = shrinkForwardGenesNonSeed(forwardGenes, gapThreshold); 
                }

                genes.clear();
                genes.addAll(reverseGenesWithoutGap);
                genes.addAll(forwardGenesWithoutGap);
            }


        }
        // The operon is not bidirectional and the genes in the opposite direction have been removed from the
        // the gene list in the operon. The function for non bidirectional can be used
        else {
            this.genes = enforceMaximumGapThresholdUni(this.genes, gapThreshold);

        }
    }

    /**
     * Method to check for the gene gap between genes on the forward genes assuming that the seed gene is not in this
     * part of the operon
     *
     * @param forwardGenes Arraylist of Gene objects ordered on their start position
     * @param gapThreshold int, the gap threshold
     * @return ArrayList of genes that do not have gaps between them that exceed the gap threshold
     */
    private ArrayList<Gene> shrinkForwardGenesNonSeed(ArrayList<Gene> forwardGenes, int gapThreshold) {
        if(forwardGenes.isEmpty()) {
            return new ArrayList<>(); // it is possible that there are no forward genes in the operon
        }
        int lastEnd = forwardGenes.get(0).getEndPosition();
        int currentBegin = 0;
        boolean gapToBig = false;
        HashSet<Gene> toRemove = new HashSet<>();

        for (Gene gene : forwardGenes) {
            currentBegin = gene.getBeginPosition();

            if (gapToBig || currentBegin - lastEnd > gapThreshold) {
                gapToBig = true;
                toRemove.add(gene) ;

            }
        }
        forwardGenes.removeAll(toRemove);
        return forwardGenes;
    }


    /**
     * Method to check for the gene gap between genes on the reverse genes assuming that the seed gene is not in this
     * part of the operon
     *
     * @param genes Arraylist of Gene objects ordered on their start position
     * @param gapThreshold int, the gap threshold
     * @return ArrayList of genes that do not have gaps between them that exceed the gap threshold
     */

    private ArrayList<Gene> shrinkReverseGenesNonSeed(ArrayList<Gene> genes, int gapThreshold) {
        if (genes.isEmpty()) { // It is possible that there are no reverse genes.
            return new ArrayList<Gene>();
        }
        // Loop through the genes in backwards and see whether any gap might be to big and remove genes if necessary
        int startLast = genes.get(genes.size()-1).getBeginPosition(); // get the start of the last gene in the reverse part
//        System.out.println("START LAST: " + startLast);
        int endCurrent = 0;
        boolean gapToBig = false ;
        HashSet<Gene> toRemove = new HashSet<>();

        for (int i = genes.size() - 1 ; i >= 0 ; i-- ) {
            endCurrent = genes.get(i).getEndPosition() ;

            if (gapToBig || startLast - endCurrent > gapThreshold) {
                gapToBig = true;
                toRemove.add(genes.get(i));
            }

        }
        genes.removeAll(toRemove);
        return genes;
    }

    /**
     * Method to determine whether an operon meets the conditions to be a bidirectional operon, meaning that the gap
     * between the last reverse and first forward gene does not exceed the given threshold
     * provided at commandOptions.promoterGapThreshold
     *
     * @param seedGene             The gene around which the region was extracted
     * @param promoterGapThresholdMax int, the max number of base pairs that is allowed between to the gap for the
     *                             bidirectional promoter.
     * @return boolean, whether the gap between the last reverse and first forward gene allows the operon to be
     * possibly under the control of a bidirectional promoter.
     */
    private boolean enforceBidirectional(Gene seedGene, int promoterGapThresholdMax, int promoterGapThresholdMin) {
        boolean bidirectional = false;
        // Here start to enforce the promotor Gap threshold if this is different from the
        if (seedGene.getStrand().contains("Forward")) {
            // need to check whether there is a reverse part before the forward gene
            if (genes.get(0).getStrand().contains("Reverse")) {
                int endLastReverse = 0;
                int beginForward = 0;
                // get the last reverse gene before the strand shifts to forward strand
                for (Gene gene : genes) {
                    if (gene.getStrand().contains("Reverse")) {
                        endLastReverse = gene.getEndPosition();
                    } else { // this means that we have come to the forward part as the genes are ordered on position
                        beginForward = gene.getBeginPosition();
                        break;
                    }
                }

                if (beginForward - endLastReverse > promoterGapThresholdMax || beginForward - endLastReverse < promoterGapThresholdMin) {
                    ArrayList<Gene> toRemove = new ArrayList<>();
                    for (Gene gene : genes) {
                        if (gene.getStrand().contains("Reverse")) {
                            toRemove.add(gene);
                        }
                    }
                    genes.removeAll(toRemove);
                    // this means that the operon does not meet the set conditions to be considered bidirectional.
                } else {
                    bidirectional = true;
                }
            }

        } else { // the seed gene is on the reverse strand
            // The forward part lies after the reverse part. so we can loo through the operon until the first
            // forward gene is reached.
            int endLastReverse = 0;
            int beginFirstForward = 0;

            for (Gene gene : genes) {
                if (gene.getStrand().contains("Reverse")) {
                    endLastReverse = gene.getEndPosition();
                } else { // this means that we have found the first forward gene in the operon
                    beginFirstForward = gene.getBeginPosition();
                    break;
                }
            }

            if (beginFirstForward - endLastReverse > promoterGapThresholdMax || beginFirstForward - endLastReverse < promoterGapThresholdMin) {
                // we have to remove all the forward genes from the operon.
                ArrayList<Gene> toRemove = new ArrayList<>();
                for (Gene gene : genes) {
                    if (gene.getStrand().contains("Forward")) {
                        toRemove.add(gene);
                    }
                }
                genes.removeAll(toRemove);
                // this means that the operon does not meet the set conditions to be considered bidirectional.
            } else {
                bidirectional = true;
            }
        }
        return bidirectional;
    }

    /**
     * Method to remove genes from the operon if the gap between two genes is to big.
     * The threshold value is given as a command line variable
     * This method is only used when the application will not consider bidirectional promoters.
     * <p>
     *
     * @param gapThreshold, integer: the maximum distance that is allowed between two genes in the same operon.
     */
    public ArrayList<Gene> enforceMaximumGapThresholdUni(ArrayList<Gene> genes, int gapThreshold) {
        ArrayList<Gene> outputList = new ArrayList<>() ;

        Gene seedGene = findInitialGeneInOperon();
        int indexSeed = genes.indexOf(seedGene); // Get the index of the initial gene
        HashSet<Gene> genesToRemove = new HashSet<>() ; // This set will be used later to remove all the genes.

        // Search downstream from the seed
        for (int index = indexSeed + 1; index < genes.size(); index++) {

            // If there has not yet been any gap that was too large
            if (!leftEndOperon) {
                int endPreviousGene = genes.get(index - 1).getEndPosition();
                int beginCurrentGene = genes.get(index).getBeginPosition();
                int gap = beginCurrentGene - endPreviousGene;

                if (gap >= gapThreshold) {
                    leftEndOperon = true;
                    genesToRemove.add(genes.get(index));
                }
            }
            // There has already been a gap that was too large, so every gene after this, is removed.
            else {
                genesToRemove.add(genes.get(index));
            }
        }



        // Search upstream of the seed
        for (int index = indexSeed - 1; index >= 0; index--) {
            // If there has not yet been any gap that was too large
            if (!rightEndOperon) {
                int beginGeneBefore = genes.get(index + 1).getBeginPosition();
                int endCurrentGene = genes.get(index).getEndPosition();
                int gap = beginGeneBefore - endCurrentGene;
                if (gap >= gapThreshold) {
                    rightEndOperon = true;
                    genesToRemove.add(genes.get(index));
                }
            }
            // There has already been a gap that was too large, so every gene after this, is removed.
            else {
                genesToRemove.add(genes.get(index));
            }
        }


        for (Gene gene : genes) {
            if(!genesToRemove.contains(gene)) {
                outputList.add(gene) ;
            }
        }
        return outputList; // this output list is correct.
    }

    /**
     * Method to find the initial gene in the operon. This method is required as the initial gene and the gene in the
     * operon are not the same object.
     * If the function fails, the initial gene will be returned and the enforceMaximumGapThreshold method will fail.
     *
     * @return
     */
    private Gene findInitialGeneInOperon() {
        for (Gene gene : getGenes()) {
            if (gene.getGeneID().equals(this.initialGene.getGeneID())) {
                return gene;
            }
        }
        return initialGene;
    }

    /**
     * Method to sort the gene list of the operon so that the genes are in ascending order of begin position.
     */
    public void orderGenes() {
        Comparator<Gene> geneBeginPosComparator = Comparator.comparingInt(Gene::getBeginPosition);

        this.genes.sort(geneBeginPosComparator);
    }


    public void printGenesInOperon() {
        for (Gene gene : this.genes) {
            System.out.println(gene.getGeneID() + "\t" + gene.getStrand());
        }
    }

    public String getDomainsOperonString() {
        return String.join(" ", domains);
    }

    // GET AND SET METHODS BELOW
    public ArrayList<Gene> getGenes() {
        return genes;
    }

    public void setGenes(ArrayList<Gene> genes) {
        this.genes = genes;
    }

    public Gene getInitialGene() {
        return initialGene;
    }

    public void setInitialGene(Gene initialGene) {
        this.initialGene = initialGene;
    }


    public Boolean getContainsInitialGene() {
        return containsInitialGene;
    }

    public void setContainsInitialGene(Boolean containsInitialGene) {
        this.containsInitialGene = containsInitialGene;
    }


    public ArrayList<String> getDomains() {
        return domains;
    }

    public void setDomains(ArrayList<String> domains) {
        this.domains = domains;
    }

    public void printDomainsInOperon() {
        System.err.println("Seed Gene: " + this.getInitialGene().getGeneID());
        for (String domain : this.getDomains()) {
            System.out.println(domain);
        }
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public String getContig() {
        return contig;
    }

    public void setContig(String contig) {
        this.contig = contig;
    }


    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public HashSet<String> getUniqueDomainString() {
        return uniqueDomainString;
    }

    public void setUniqueDomainString(HashSet<String> uniqueDomainString) {
        this.uniqueDomainString = uniqueDomainString;
    }
}
