package nl.wur.ssb.DomainPatternRecognition;

import nl.wur.ssb.DomainPatternRecognition.FPM.FreqPatternsParallel;
import nl.wur.ssb.DomainPatternRecognition.FPMNetworkDeprecated.FPMNetwork3;
import nl.wur.ssb.DomainPatternRecognition.LDA.LDAClusteringOperons;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.objects.Gene;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.sparql.InputQuerier;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.parquet.Strings;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Main class for this application.
 * <p>
 * Author: Sanne de Graaf
 * E-mail: sanne.degraaf@wur.nl
 */

public class DomainPatternRecognition {
    private static final Logger LOGGER = Logger.getLogger(DomainPatternRecognition.class.getName());

    private HashMap<String, Operon> operonHashMap;
    public static JavaRDD<Operon> operonRDD;
    private CommandOptions commandOptions;
    private JavaSparkContext sc;
    private InputQuerier inputQuerier;

    public static String lookUpDirectory;
    public static String outputPath;
    public static String fileNameOperonHashMap;
    public static String fileNameDescriptionLookUp; // todo this one is still used in the trait detection but no longer created.
    // todo this need to be fixed.
    public static String filenameDomainFrequencies;
    public static String regionsDirectory ;
    public static String checkPointDirectory;
    public static String storageDirectory = "LookUpFiles" ;



    public DomainPatternRecognition(CommandOptions commandOptions, JavaSparkContext sc) {
        this.commandOptions = commandOptions;
        // Creating all the necessary directories for the application to run.

        createDirectory(storageDirectory);

        lookUpDirectory = storageDirectory + "/domainLookUp" ;
        fileNameDescriptionLookUp = storageDirectory + "/domainLookUp/DomainDescriptionLookUp.ser";
        regionsDirectory = storageDirectory + "/Regions_" + commandOptions.domain + "_searchWidth=" + commandOptions.width;
        checkPointDirectory = "SparkCheckPoints" ;
        createDirectory(checkPointDirectory);
        this.sc = sc;
    }

    /**
     * Queries the resource
     * @param domainToQuery seed domain
     * @param queryThoroughly only operons or also jaccard index, copy number etc...
     * @return hashmap with all operons
     * @throws Exception
     */
    public HashMap<String, Operon> queryInputData (String domainToQuery, boolean queryThoroughly) throws Exception {
        outputPath = commandOptions.outputPath + "/" + commandOptions.domain + "_" + generateHashedOperonHashMapName(commandOptions, domainToQuery);
        createDirectory(outputPath);
        String regionDir = createNameRegionDirectory(domainToQuery, commandOptions.width);
        createDirectory(lookUpDirectory);
        createDirectory(regionDir);

        LOGGER.info("Starting to query the input data");
        inputQuerier = new InputQuerier(domainToQuery, queryThoroughly, sc, commandOptions, regionDir);
        // Executes the query, collects statistical information about domain abundance and collects operons
        HashMap<String, Operon> operons = inputQuerier.queryInputData();
        filenameDomainFrequencies = inputQuerier.getDomainFrequencyHashMapFileName().getAbsolutePath();
//        createStatisticsOperonHashMap(operons);
        writeOperonsToDumpFile(operons);
        return operons;
    }

    /**
     * Method to write the operon hashmap to a dump file. In this file, all domains present in the operons, in the order
     * in which they are found are written as strings.
     * @param operons Operon HashMap
     */
    private void writeOperonsToDumpFile(HashMap<String, Operon> operons) {
        String operonDumpName = outputPath + "/domainsInOperonsDump.txt";
        // If this file does not yet exist, it will be created
        if (! new File(operonDumpName).exists()) {
            try {
                FileWriter fileWriter = new FileWriter(operonDumpName);
                for (Operon operon : operons.values()) {
                    String line = operon.getSample() + "\t" + operon.getInitialGene().getGeneID() + "\t" + Strings.join(operon.getDomains(), ",") + "\n";
                    fileWriter.write(line);
                }
                LOGGER.info("Dumping operon domain composition to " + operonDumpName);
                fileWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public static String createNameRegionDirectory(String domain, int width) {
        return storageDirectory + "/Regions_" + domain + "_searchWidth=" + width ;
    }

    public static boolean checkHDT(File file) throws IOException {
        byte[] buffer = new byte[4];
        InputStream is = new FileInputStream(file);
        if (is.read(buffer) != buffer.length) { // todo this empty if is necessary for the code to run somehow..
            // do something
        }
        is.close();
        return new String(buffer).matches("\\$HDT");
    }

    /**
     * Method to run the frequent pattern mining algorithm
     * @param domain the domain for which the frequent pattern mining will be carried out.
     */
    public void runFrequentPatternMining(String domain) throws Exception {
        if (commandOptions.doFrequentPatternMining) {
            LOGGER.info("Running frequent pattern mining for domain: " + domain);
            FreqPatternsParallel freqPatternsParallel = new FreqPatternsParallel(commandOptions, sc, operonHashMap, inputQuerier.getDomainFrequencies(), domain);
            freqPatternsParallel.runFrequencyPatternMining();
        }
    }


    public void createFPMNetwork() throws Exception {
        if (commandOptions.doFPMNetwork) {
            for (double supportValue : commandOptions.minSupportValues) {
                FPMNetwork3 fpmNetwork3 = new FPMNetwork3(commandOptions, supportValue, sc);
                fpmNetwork3.createNetwork();

//                NetworkFromFPMGraphX networkFromFPMGraphX = new NetworkFromFPMGraphX(commandOptions, supportValue, sc);
//                networkFromFPMGraphX.runFPMNetworkAnalysis();
//                NetworkFromFPM networkFromFPM = new NetworkFromFPM(commandOptions, supportValue, sc);
//                networkFromFPM.runFPMNetworkAnalysis();

            }
        }
    }

    public void doLDAClusteringOperons() throws IOException {
        if (commandOptions.doLDAClusteringOperons) {
            LDAClusteringOperons ldaClusteringOperons = new LDAClusteringOperons(sc, commandOptions, operonHashMap);
            ldaClusteringOperons.runLDAClusteringOperons();
        }
    }


    public static String createLookUpFileName (File lookUpFile) {
        return "HashMap_" + lookUpFile.getName() + ".ser" ;
    }

    public static String retrieveLookUpFileName (File lookUpFile) {
        return "HashMap_Queried_" + lookUpFile.getName() + ".tsv.ser" ;
    }

    /**
     * Method to create the directory which will hold all output of the application.
     * If the directory is not empty a warning will be given.
     */
    private void createDirectory(String path) {
        File newDirectory = new File(path);
        if (!newDirectory.exists()) {
            boolean success = newDirectory.mkdirs();
            LOGGER.info("Directory creation successful:" + success + "; created at: " + newDirectory.getAbsolutePath());
        } else {
            if (newDirectory.list().length > 0) {
                LOGGER.warn("Directory " + newDirectory.getAbsolutePath() + " was not empty. Files might be overwritten");
            }
        }
    }

    /**
     * Method to generate a MD5 hashed file name for output files
     *
     * @param queryName      string, name of query file to be executed
     * @param genomeFileName name of genome file to be exectued.
     * @return String outfile name ending with .ser
     * @throws NoSuchAlgorithmException due to hashing of the string
     * @throws IOException
     */
    public static String generateHashedFileName(String queryName, String genomeFileName, CommandOptions commandOptions)
            throws NoSuchAlgorithmException, IOException {
        String header = Util.readFile("queries/header.txt");
        String content = header + "\n" + Util.readFile("queries/" + queryName);
        content += "\n" +
                "domain of interest:\t" + commandOptions.domain + "\n" +
                "database: " + commandOptions.db + "\n" +
                "provenance: " + commandOptions.originProv + "\n" +
                genomeFileName;

        return getHashedString(content);
    }

    /**
     * A method to create some statistics on how the operons look like
     * Calculates, intergenic distance (average), standard diviation, min/max, bidirectional gap.
     * @param operons
     */
    public boolean createStatisticsOperonHashMap(HashMap<String, Operon> operons) throws IOException {
        JavaRDD<Operon> operonsRDD = sc.parallelize(new ArrayList<>(operons.values()));

        // Analyze the number of genes per operon
        JavaRDD<Integer> numberOfGenesRDD = operonsRDD.map(operon -> operon.getGenes().size());
        double numberOfOperons = operons.size();
        if (numberOfOperons != 0.0) {

            generateSamplesFile(operonsRDD);

            String outputFilePath = outputPath + "/1_" + commandOptions.domain + "-search-parameters-and-operon-architecture.txt";
            FileWriter fileWriter = new FileWriter(outputFilePath);
            writeCommandOptions(fileWriter);

            LOGGER.info("Calculating statistics of operons. Writing to " + outputFilePath);

            double averageNumberOfGenes = numberOfGenesRDD.reduce(Integer::sum) / numberOfOperons;
            int maxGenes = numberOfGenesRDD.reduce(Math::max);
            int minGenes = numberOfGenesRDD.reduce(Math::min);
            JavaRDD<Double> deviationsRDD = numberOfGenesRDD.map(numberGenes -> ((numberGenes - averageNumberOfGenes) * (numberGenes - averageNumberOfGenes) / numberOfOperons));
            double standardDeviationGenes = Math.sqrt(deviationsRDD.reduce(Double::sum));

            DecimalFormat decimalFormat = new DecimalFormat("0.00");

            fileWriter.write("Number of Operons predicted\t"+numberOfOperons);
            fileWriter.write("\nOperon Architecture\n");
            fileWriter.write("About\tAverage\tStandard deviation\tMax\tMin\n");
            fileWriter.write("Genes\t" + decimalFormat.format(averageNumberOfGenes) + "\t" + decimalFormat.format(standardDeviationGenes) + "\t" + maxGenes + "\t" + minGenes + "\n");

            // Next analyze the average gap size between genes on the same strand.
            JavaRDD<Integer> genGapSizesRDD = operonsRDD.flatMap(DomainPatternRecognition::calculateGenGapSize);

            if (!genGapSizesRDD.isEmpty()) {
                long numberOfGaps = genGapSizesRDD.count();
                double averageGeneGap = genGapSizesRDD.reduce(Integer::sum) / (double) numberOfGaps;
                int maxGeneGap = genGapSizesRDD.reduce(Math::max);
                int minGeneGap = genGapSizesRDD.reduce(Math::min);
                JavaRDD<Double> deviationsGeneGap = genGapSizesRDD.map(gapSize -> ((gapSize - averageGeneGap) * (gapSize - averageGeneGap) / numberOfGaps));
                double standardDeviationGeneGap = Math.sqrt(deviationsGeneGap.reduce(Double::sum));
                fileWriter.write("Gene gap\t" + decimalFormat.format(averageGeneGap) + "\t" + decimalFormat.format(standardDeviationGeneGap) + "\t" + maxGeneGap + "\t" + minGeneGap + "\n");
            } else {
                fileWriter.write("Gene gap\tNone\tNone\tNone\tNone\n");
            }

            // Next analyze the bidirectional gap size
            JavaRDD<Integer> biGapSizesRDD = operonsRDD.flatMap(DomainPatternRecognition::calculateBiGapSize);
            if (!biGapSizesRDD.isEmpty()) { // Only do this when there actually are bidirectional operons.
                long numberOfBiGaps = biGapSizesRDD.count();
                double averageBiGap = biGapSizesRDD.reduce(Integer::sum) / (double) numberOfBiGaps;
                int maxBiGap = biGapSizesRDD.reduce(Math::max);
                int minBiGap = biGapSizesRDD.reduce(Math::min);
                JavaRDD<Double> deviationsBiGap = biGapSizesRDD.map(gapSize -> ((gapSize - averageBiGap) * (gapSize - averageBiGap) / numberOfBiGaps));
                double standardDeviationBiGap = Math.sqrt(deviationsBiGap.reduce(Double::sum));
                fileWriter.write("Bidirectional gap\t" + decimalFormat.format(averageBiGap) + "\t" + decimalFormat.format(standardDeviationBiGap) + "\t" + maxBiGap + "\t" + minBiGap + "\n");
                LOGGER.info("Bidirectional operons found");
            } else {
                LOGGER.info("No bidirectional operons found");
                fileWriter.write("Bidirectional gap\tNone\tNone\tNone\tNone\n");
            }

            fileWriter.close();
            return true;
        }
        else {
            LOGGER.warn("Zero operons found using these specific search parameters");
            return false;
        }
    }

    /**
     * A method to create a file that holds the GC-numbers of all the
     * @param operonsRDD JavaRDD containing all operons
     */
    private void generateSamplesFile(JavaRDD<Operon> operonsRDD) throws IOException {
        String outputFilePath = outputPath + "/2_" + commandOptions.domain + "-all_samples.txt";
        FileWriter fileWriter = new FileWriter(outputFilePath);

        List<Tuple2<String, String>> samples = operonsRDD.map(operon -> new Tuple2<String, String>(operon.getSample(), operon.getScientificName())).collect();
        for (Tuple2<String, String> operonInfo : samples) {
//            String speciesName = operonInfo._2.substring(1, operonInfo._2.length() -1);  // todo this creates null pointers on big data set...
            fileWriter.write(operonInfo._1 + "\t" + operonInfo._2 + "\n");
//            fileWriter.write(operonInfo._1 + "\t" + operonInfo._2.substring(1, operonInfo._2.length() - 1) +" \n");

        }

        fileWriter.close();
    }


    /**
     * Method to write the command options variables to a file
     * @param fileWriter filewriter object to which this data will be written.
     */
    private void writeCommandOptions(FileWriter fileWriter) throws IOException {
        fileWriter.write("Domain\t" +commandOptions.domain+ "\n");
        fileWriter.write("Search width\t" + commandOptions.width + "\n");
        fileWriter.write("Bidirectional\t" + commandOptions.bidirectional+ "\n");
        if (commandOptions.bidirectional) {fileWriter.write("Bidirectional gap max\t" + commandOptions.bidirectionalGapMax+ "\n");
        fileWriter.write("Bidirectional gap min\t" + commandOptions.bidirectionalGapMin + "\n");}
        fileWriter.write("Gene gap\t" + commandOptions.geneGap + "\n");
        fileWriter.write("Provenance\t" + commandOptions.originProv + "\n");
        fileWriter.write("Database\t" + commandOptions.db + "\n");
        fileWriter.write("Additional domains\t" + commandOptions.additionalDomains + "\n");
        fileWriter.write("Excluded domains\t" + commandOptions.excludedDomains + "\n");
    }

    private static Iterator<Integer> calculateBiGapSize(Operon operon) {
        ArrayList<Integer> output = new ArrayList<>();
        if (operon.getGenes().get(0).getStrand().contains("Forward") ||
                operon.getGenes().get(operon.getGenes().size() - 1).getStrand().contains("Reverse")){

            // The first gene is already forward so there will not be any reverse part
            // Or the last gene is a reverse so there will not be an forward part afterwards.
            return output.iterator();
        } else {
            // loop through the operon and see if it is bidirectional or not.
            int reverseEnd = -1 ;
            for (Gene gene : operon.getGenes()) {
                if (gene.getStrand().contains("Reverse")) {
                    reverseEnd = gene.getEndPosition();
                } else { // we encounter the first forward gene
                    output.add(gene.getBeginPosition() - reverseEnd);
                    return output.iterator();
                }
            }
        }
        return output.iterator();
    }

    /**
     * A method to get all the sizes of the gaps between genes on the same strand of an operon
     * @param operon Operon object with genes
     * @return an iterator containing all the gap sizes as integers.
     */
    private static Iterator<Integer> calculateGenGapSize(Operon operon) {
        ArrayList<Integer> output = new ArrayList<>();
        if (operon.getGenes().size() > 1) { // Only do this when there are at least to genes in the operon.
            for (int i = 1; i < operon.getGenes().size(); i++) { // We start with the second gene in the list
                Gene previousGene = operon.getGenes().get(i - 1);
                Gene currentGene = operon.getGenes().get(i);

                if (previousGene.getStrand().equals(currentGene.getStrand())) { // only if they lie on the same strand
                    // we should consider the gap between the genes. Otherwise it is the bidirectional gap.
                    int gap = currentGene.getBeginPosition() - previousGene.getEndPosition();
                    output.add(gap);
                }
            }
        }
        return output.iterator();
    }

    /**
     * Method to generate a name for the operon HashMap based on the exact input, the domain of interest and the
     * search width used to extract genes from the region.
     *
     * @return String. A MD5 hashed name
     * @throws IOException              Due to hashing of the name
     * @throws NoSuchAlgorithmException Due to hashing of the name
     * @param commandOptions CommandOptions Object
     */
    public static String generateHashedOperonHashMapName(CommandOptions commandOptions, String domainToQuery)
            throws IOException, NoSuchAlgorithmException {

        List<File> files = new ArrayList<>();
        for (String directoryName : commandOptions.inputDirectory) {
            File directory = new File(directoryName);
            files.addAll(FileUtils.listFiles(directory,null,commandOptions.recursiveInput));
        }

        ArrayList<String> inputHDTFiles = new ArrayList<>();

        for (File file : files) {
            if (!file.getName().endsWith("index.v1-1") && checkHDT(file)) {
                inputHDTFiles.add(file.getAbsolutePath());
            }
        }

        String content =
                domainToQuery + "\n" +
                commandOptions.width + "\n" +
                commandOptions.bidirectional + "\n" +
                commandOptions.geneGap + "\n" +
                commandOptions.bidirectionalGapMax + "\n" +
                commandOptions.bidirectionalGapMin + "\n" +
                commandOptions.additionalDomains + "\n" +
                commandOptions.excludedDomains + "\n" +
                String.join("\n", inputHDTFiles);

        return getHashedString(content);
    }

    /**
     * Method to get the a hased string from an input string
     * @param content Input String
     * @return MD5 hash from the input string
     * @throws NoSuchAlgorithmException due to hashing.
     */
    private static String getHashedString(String content) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(content.getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString() + ".ser";
    }


    public JavaSparkContext getSc() {
        return sc;
    }

    public void setSc(JavaSparkContext sc) {
        this.sc = sc;
    }

    public HashMap<String, Operon> getOperonHashMap() {
        return operonHashMap;
    }

    public void setOperonHashMap(HashMap<String, Operon> operonHashMap) {
        this.operonHashMap = operonHashMap;
    }
}
