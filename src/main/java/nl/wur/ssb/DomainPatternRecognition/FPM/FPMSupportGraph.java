package nl.wur.ssb.DomainPatternRecognition.FPM;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FPMSupportGraph {
    private DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
    private ArrayList<String> domainIDs; 
    private String plotName; 
    private String plotTitle; 

    public FPMSupportGraph(double[] support, ArrayList<String> domainIDs, String plotName, String plotTitle) throws IOException {
        this.domainIDs = domainIDs; 
        this.plotName = plotName;
        this.plotTitle = plotTitle; 
        createDataSet(support);
        JFreeChart chart = createChat();
        clearChart(chart) ;

        ChartUtilities.saveChartAsPNG(new File( DomainPatternRecognition.outputPath + "/" + plotName + ".png"), chart, 800, 600);
    }

    private void clearChart(JFreeChart chart) {

        // Changing the background colors
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
        plot.setOutlineVisible(false);

        // changing the position of the axis
        plot.setDomainAxisLocation(AxisLocation.TOP_OR_RIGHT);
        plot.setRangeAxisLocation(AxisLocation.TOP_OR_RIGHT);
        plot.getRangeAxis().setRange(0,1);
        plot.getDomainAxis().setMaximumCategoryLabelWidthRatio((float) 0.6);

        // format the bars
        ((BarRenderer) plot.getRenderer()).setBarPainter(new StandardBarPainter());
        ((BarRenderer) plot.getRenderer()).setShadowVisible(false);
        ((BarRenderer) plot.getRenderer()).setItemMargin(0.05);

    }

    /**
     * Method to create the categorical data set that is required for the plot.
     * @param support array of doubles containing the support values.
     */
    private void createDataSet(double[] support) {
        for (int i = 0 ; i < support.length ; i++) {
            dataSet.setValue(support[i], "SupportValue", domainIDs.get(i));
        }
    }

    private JFreeChart createChat() {
        return ChartFactory.createBarChart(
                this.plotTitle,
                "",
                "",
                dataSet,
                PlotOrientation.HORIZONTAL,
                false, true, false) ;
    }

}
