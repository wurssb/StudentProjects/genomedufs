package nl.wur.ssb.DomainPatternRecognition.FPM;

import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.fpm.FPGrowth;
import scala.Tuple3;

import java.util.*;

public class JaccardIndex {
    private static final Logger LOGGER = Logger.getLogger(JaccardIndex.class.getName());
    private JavaRDD<FPGrowth.FreqItemset<String>> domains ;
    private static HashMap<String, Operon> operonHashMap ;
    private static HashMap<String, HashMap<String, String>> domainFrequencies ;
    private HashMap<String, HashMap<String, Double>> jaccardIndices = new HashMap<>();

    public JaccardIndex(JavaRDD<FPGrowth.FreqItemset<String>> domains,HashMap<String, Operon> operonHash, HashMap<String, HashMap<String, String>> domainFreqHashMap) {
        LOGGER.info("Calculating the Jaccard Indices.");
        this.domains = domains ;
        operonHashMap = operonHash ;
        calculateJaccardIndices(domainFreqHashMap);
    }

    /**
     * Method to calculate the Jaccard indices for every domain in the domains JavaRDD
     * @param domainFreqHashMap the HashMap containing the frequencies of all domains.
     */
    private void calculateJaccardIndices(HashMap<String, HashMap<String, String>> domainFreqHashMap) {
        // for every domain in the lone domains. look up how often it occurs in the domain frequency hashmap
        // calculate how often it occurs in every operon.
        // do this with a flapmap against the domains rdd.

        domainFrequencies = domainFreqHashMap;

        List<Tuple3<String, Double, Double>> calculatedIndices = domains.flatMap(JaccardIndex::calculate).collect();

        for (Tuple3<String, Double, Double> tuple : calculatedIndices) {

            HashMap<String, Double> innerMap = new HashMap<>();
            innerMap.put("samplesWith", tuple._2());
            innerMap.put("allSamples", tuple._3());

            jaccardIndices.put(tuple._1(), innerMap);
        }


    }

    /**
     * method to calculate the Jaccard Index for one domain
     * @param itemSet A FPGrowth Itemset with one domain
     * @return Iterator with a 3-tuple containing (domain, jaccard index calculated only for the samples with the domain,
     * jaccard index calculated over all samples).
     */
    private static Iterator<Tuple3<String, Double, Double>> calculate(FPGrowth.FreqItemset<String> itemSet) {
        ArrayList<Tuple3<String, Double, Double>> output = new ArrayList<>() ;
        String otherDomain = itemSet.javaItems().get(0) ;

        double otherTotalCountSamplesWith = Double.parseDouble(domainFrequencies.get(otherDomain).get("domainCountWith"));
        double otherTotalCountAllSamples = otherTotalCountSamplesWith + Double.parseDouble(domainFrequencies.get(otherDomain).get("domainCountWithout")) ;

        int otherOperonCount = 0 ; // if a domain is in the operon, it is always together with domain of interest
        for (String key : operonHashMap.keySet()) {
            Operon operon = operonHashMap.get(key);
            otherOperonCount += Collections.frequency(operon.getDomains(), otherDomain);
        }

        double jaccardIndexSamplesWith = otherOperonCount / otherTotalCountSamplesWith ;
        double jaccardIndexAllSamples = otherOperonCount / otherTotalCountAllSamples ;
        Tuple3<String, Double, Double> jaccardOutput = new Tuple3<>(otherDomain, jaccardIndexSamplesWith, jaccardIndexAllSamples);
        output.add(jaccardOutput);

        return output.iterator();
    }

    public HashMap<String, HashMap<String, Double>> getJaccardIndices() {
        return jaccardIndices;
    }

}
