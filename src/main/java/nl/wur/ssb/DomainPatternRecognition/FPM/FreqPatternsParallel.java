package nl.wur.ssb.DomainPatternRecognition.FPM;

import nl.wur.ssb.DomainPatternRecognition.*;
import nl.wur.ssb.DomainPatternRecognition.LDA.LDAClusteringFrequentPatternMining;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import nl.wur.ssb.DomainPatternRecognition.traits.TraitDetector;
import nl.wur.ssb.DomainPatternRecognition.traits.TraitNode;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.fpm.AssociationRules;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowthModel;
import scala.Tuple2;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

public class FreqPatternsParallel extends MachineLearningParallel {

    private static final Logger LOGGER = Logger.getLogger(FreqPatternsParallel.class.getName());
    private final String domain;
    private AssociationRules associationRules;
    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private Iterator<AssociationRules.Rule<String>> ARulesIterator;
    private HashMap<String, Operon> operonHashMap;

    /**
     * Construct for Frequency pattern object
     *
     * @param commandOptions CommandOptions object which holds all provided command options
     * @param sc             Spark Context
     */
    public FreqPatternsParallel(CommandOptions commandOptions, JavaSparkContext sc, HashMap<String, Operon> operonHashMap, HashMap<String, HashMap<String, String>> domainFrequencies, String domain ) {
        this.commandOptions = commandOptions;
        this.sc = sc;
        this.operonHashMap = operonHashMap;
        this.domainFrequencies = domainFrequencies;
        this.domain = domain;
    }


    /**
     * The core of the FPM
     * @throws Exception
     */
    public void runFrequencyPatternMining() throws Exception {

        for (int i = 0; i < commandOptions.minSupportValues.size(); i++) {

            double supportValue = commandOptions.minSupportValues.get(i);
            LOGGER.info("Running frequency pattern mining for support value: " + supportValue);

            FPGrowthModel<String> FPGrowthModel = generateFPGrowhtModel(supportValue);
            JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD = FPGrowthModel.freqItemsets().toJavaRDD();

            // Patterns have been found for this support value.
            this.loneDomains = freqItemsetJavaRDD.filter(set -> set.javaItems().size() == 1);

            if (i == 0) {
               addFrequenciesToDomainFreq();
            }

            // calculate the copy numbers for the necessary domains.
            Iterator<FPGrowth.FreqItemset<String>> domainKeys = loneDomains.toLocalIterator();
            while  (domainKeys.hasNext()) {
                String domainKey = domainKeys.next().javaItems().get(0);
                HashMap<String, String> domainInfo = domainFrequencies.get(domainKey);
                domainFrequencies.put(domainKey, super.calculateCopyAndOrganismNumber(domainInfo));
            }

            JaccardIndex jaccardIndex = new JaccardIndex(loneDomains, operonHashMap, domainFrequencies);
            HashMap<String, HashMap<String, Double>> jaccardIndixes = jaccardIndex.getJaccardIndices();

            if (commandOptions.saveRawFPMFiles) {
            writeRawPatternsOutput(freqItemsetJavaRDD, supportValue); // Be carefull, this can become a very big file.
            }

            writeFrequencyPatternOutput(supportValue, loneDomains, jaccardIndixes);
            // Figure generation
            createSupportGraph(loneDomains, supportValue);

            if (commandOptions.detectTraits) {
                LOGGER.info("Searching for traits in the FPM output. This might cause out of memory errors or stackoverflow errors. Consider increasing the support values if this happens");
                // TODO not working yet...
                new TraitDetector(freqItemsetJavaRDD, domain, supportValue, operonHashMap.size());
            }

            // TODO check if this function is still needed
            // checkCorrectness(freqItemsetJavaRDD, this.domain, operonHashMap.size());

            // TODO this generates a large list.. not sure yet how to proceed
            if (this.operonHashMap.size() >= commandOptions.minNumberOfOperons && commandOptions.doAssocationRules) {
                JavaRDD<AssociationRules.Rule<String>> ARules = generateAssociationRules(freqItemsetJavaRDD);
                gatherConsequencesARules(ARules);
            }

            // perform LDA clustering on with the frequent domains as input to see whether there might be different
            // traits in the frequent domains.
            if (commandOptions.doLDAClusteringFPM) {
                LDAClusteringFrequentPatternMining LDAClustering = new LDAClusteringFrequentPatternMining(sc, commandOptions, operonHashMap, supportValue, jaccardIndixes);
                LDAClustering.runLDAClusteringFP(loneDomains);
            }
        }
    }

    /**
     * A method to create a bar chart with the support values for every domain in the lone domains RDD
     * @param loneDomains JavaRDD with the frequent item sets
     */

    private void createSupportGraph(JavaRDD<FPGrowth.FreqItemset<String>> loneDomains, double supportValue) throws IOException {

        // first get from all the lone domains the support value
        ArrayList<Tuple2<Double, String>> supportValuesDomains = new ArrayList<>();
        Iterator<FPGrowth.FreqItemset<String>> itemsetIterator = loneDomains.toLocalIterator();
        while (itemsetIterator.hasNext()) {
            FPGrowth.FreqItemset<String> itemSet = itemsetIterator.next();
            HashMap<String, String> domainInfo = domainFrequencies.get(itemSet.javaItems().get(0));
            supportValuesDomains.add(new Tuple2<>(Double.parseDouble(domainInfo.get("support")), itemSet.javaItems().get(0) + " - " + domainInfo.get("domainDescription")));
        }

        // Next sort all the domains from highest to lowest support value
        // For this a new comparator is created.
        Comparator<Tuple2<Double, String>> comparator = (tuple1, tuple2) -> tuple2._1.compareTo(tuple1._1);

        supportValuesDomains.sort(comparator);

        // Split the support values and the domains into two separate lists, but keep the right order / connections
        // between the two lists
        double[] supportValues = new double[supportValuesDomains.size()];
        ArrayList<String> domains = new ArrayList<>();

        for (int i = 0 ; i < supportValuesDomains.size() ; i++) {
            Tuple2<Double, String> tuple = supportValuesDomains.get(i);
            supportValues[i] = tuple._1;
            domains.add(i, tuple._2);
        }

        FPMSupportGraph fpmSupportGraph = new FPMSupportGraph(supportValues,domains, domain + "_SupportGraph_" + supportValue,"Support Values for " + domain);

    }



    /**
     * Method to run the PFPGrowth algorithm and generate the FPGrowth model
     * See: http://dx.doi.org/10.1145/1454008.1454027
     */
    private FPGrowthModel<String> generateFPGrowhtModel(double supportValue) {
        LOGGER.info("Generating the FP Growth model");
        // preparing domain data. One operon may only contain unique domains.
        List<List<String>> domainList = new ArrayList<>();
        operonHashMap.forEach((s, operon) -> domainList.add(new ArrayList<>(operon.getUniqueDomainString())));
        // List<String> domainList = prepareDomainsForFPM();

        JavaRDD<List<String>> domainData = sc.parallelize(domainList);
        // JavaRDD<List<String>> domainData = dataDomain.map(line -> Arrays.asList(line.split(" ")));

        int partitions = Math.round(domainList.size() / commandOptions.jobs);
        // Ensuring positive value for partitioning
        LOGGER.info("NUMBER OF PARTITIONS = " + partitions);
        if (partitions < 1) partitions = 1;
        if (partitions > 20000) partitions = 200000; // To many partitions might lead to errors as below
        // Total size of serialized results of 1 tasks (1431.0 MiB) is bigger than spark.driver.maxResultSize (1024.0 MiB)

        FPGrowth fpg = new FPGrowth()
                .setMinSupport(supportValue) // minimum support for an itemset to be considered frequent
                .setNumPartitions(partitions); // number of partitions used to distribute the work
        return fpg.run(domainData);
    }

    /**
     * Auxiliary method for generateFPGrowhtModel to take domain data from operon and
     * create desired format for FPGrowht algorithm
     *
     * @return arraylist with all unique domains per operon.
     */
    private List<String> prepareDomainsForFPM() {

        List<String> domainList = new ArrayList<>();

        for (String operonKey : this.operonHashMap.keySet()) {
            Operon operon = this.operonHashMap.get(operonKey);
//            String domainsString = "";
            Set<String> uniqueDomains = new HashSet<>(operon.getDomains());
            String domainsString = StringUtils.join(uniqueDomains, " ");
//            for (String domainID : uniqueDomains) {
//                domainsString = domainsString.concat(domainID);
//                domainsString = domainsString.concat(" ");
//            }
//            domainsString = domainsString.substring(0, domainsString.length() - 1);
            domainList.add(domainsString);
        }
        return domainList;
    }

    /**
     * method to generate the association rules from the frequency table.
     */
    private JavaRDD<AssociationRules.Rule<String>> generateAssociationRules(JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD) {
        this.associationRules = new AssociationRules().setMinConfidence(commandOptions.minCon);
        return associationRules.run(freqItemsetJavaRDD);
    }

    /**
     * Method to add the frequencies of a domain to the domainFrequencies HashMap
     */
    private void addFrequenciesToDomainFreq() {
        Iterator<FPGrowth.FreqItemset<String>> itemsetIterator = loneDomains.toLocalIterator();
        LOGGER.info("Adding frequencies to domain freq");
        LOGGER.info("If this step takes >10 min, consider rerunning with higher minimum support values");

        // create a paired RDD from the lone domains so that it is easier to look up the

        while (itemsetIterator.hasNext()) {
            FPGrowth.FreqItemset<String> itemset = itemsetIterator.next();
            String pattern = itemset.javaItems().get(0);
            if (domainFrequencies.containsKey(pattern)) {
                domainFrequencies.get(pattern).put("patternFreq", String.valueOf(itemset.freq()));
            }
        }
    }

    /**
     * Method to write all the found frequent patterns to a file
     *
     * @param freqItemsetJavaRDD the rdd with all frequent item sets
     */
    private void writeRawPatternsOutput(JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD, double supportValue) {
        LOGGER.info("Writing the raw frequent pattern output");
        String fileName = DomainPatternRecognition.outputPath + "/" + domain + "_Raw_pattern_output_" + supportValue + "_frequency_patterns.txt";

        try {
            FileWriter fileWriter = new FileWriter(fileName);

            Iterator<FPGrowth.FreqItemset<String>> itemsetIterator = freqItemsetJavaRDD.toLocalIterator();
            while (itemsetIterator.hasNext()) {
                FPGrowth.FreqItemset<String> itemset = itemsetIterator.next();
                String line = String.join(" ", itemset.javaItems()) + "\t" + itemset.freq() + "\n";
                fileWriter.write(line);
            }
            fileWriter.close();

        } catch (IOException e) {
            LOGGER.error(">>> FAILED: UNABLE TO WRITE RAW FREQUENCY OUTPUT TO FILE: " + fileName);
        }


    }

    /**
     * method to write the output of the frequency patterns to a tab delimited text file
     */
    private void writeFrequencyPatternOutput(double supportValue, JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD, HashMap<String, HashMap<String, Double>> JaccardIndices) {
        String filePath = DomainPatternRecognition.outputPath + "/" + this.domain + "_" + supportValue + "_frequency_patterns.txt"; // this is also used in NetworkFromFPM.
        LOGGER.info("Writing frequency pattern output to file: " + filePath);
        try {
            FileWriter fileWriter = new FileWriter(filePath);
            fileWriter.write("#Support Value:\t" + supportValue);
            fileWriter.write("\n");

            String header = "#Domain\tPatternFrequency\tsupport\tJaccardIndex samples with DOI\tJaccardIndex all samples\tCo-occurrence frequency\tFrequency without domain of interest\t" +
                    "Copy number when co-occurring\tCopy number in genomes without\tSamples with domain of interest\t" +
                    "samples without domain of interest\tDomain description\n";
            fileWriter.write(header);

            // Writing the info of Domain of Interest
            HashMap<String, String> domainInfoDOI = domainFrequencies.get(domain);
            domainInfoDOI.put("support" , String.valueOf(1));
            String DOIWriteString = writeDomainInfoString(domain, domainInfoDOI, JaccardIndices);
            fileWriter.write(DOIWriteString);
            
            Iterator<FPGrowth.FreqItemset<String>> itemSets = freqItemsetJavaRDD.toLocalIterator();
            while (itemSets.hasNext()) {
                FPGrowth.FreqItemset<String> itemSet = itemSets.next();

                // Loop through all patterns found by FPMinning

                String domainInfoKey = itemSet.javaItems().get(0);
                double supportOfPattern = (double) itemSet.freq() / operonHashMap.size();

                // Start writing info on all other domains
                if (!domainInfoKey.equals(domain)) {
                    HashMap<String, String> domainInfo = domainFrequencies.get(domainInfoKey);
                    domainInfo.put("support" , String.valueOf(supportOfPattern));
                    String writeString = writeDomainInfoString(domainInfoKey, domainInfo, JaccardIndices);
                    fileWriter.write(writeString);
                }
            }

            fileWriter.close();
        } catch (IOException e) {
            LOGGER.error(">>> FAILED: UNABLE TO WRITE FREQUENCY OUTPUT TO FILE: " + filePath);
            e.printStackTrace();
        }

    }

    /**
     * Method to create the string that will be written to the output file for the frequent pattern mining.
     * Auxiliary method to writeFrequencyPatternOutput
     *
     * @param domainInfo HashMap (String String) created by queryDomainPatternRelevance
     * @return String to be written to the output file.
     */
    private String writeDomainInfoString(String domainID, HashMap<String, String> domainInfo, HashMap<String, HashMap<String, Double>> jaccardIndices) {
        return domainID + "\t"
                + domainInfo.get("patternFreq") + "\t"
                + decimalFormat.format(Double.valueOf(domainInfo.get("support"))) + "\t"
                + decimalFormat.format(jaccardIndices.get(domainID).get("samplesWith")) + "\t"
                + decimalFormat.format(jaccardIndices.get(domainID).get("allSamples")) + "\t"
                + domainInfo.get("domainCountWith") + "\t"
                + domainInfo.get("domainCountWithout") + "\t"
                + decimalFormat.format(Double.valueOf(domainInfo.get("copyNumberWith"))) + "\t"
                + decimalFormat.format(Double.valueOf(domainInfo.get("copyNumberWithout"))) + "\t"
                + domainInfo.get("organismWithCount") + "\t"
                + domainInfo.get("organismWithoutCount") + "\t"
                + domainInfo.get("domainDescription") + "\n";
    }

    /**
     * method to get per domain which occur in association rules.
     */
    public void gatherConsequencesARules(JavaRDD<AssociationRules.Rule<String>> resultAssociationRules) throws IOException {
        LOGGER.info("Gathering association rules...");

        ARulesIterator = resultAssociationRules.toLocalIterator();
        String filePath = DomainPatternRecognition.outputPath + "/" + domain + "_minimumConfidence=" + commandOptions.minCon + "_associationRules.txt";
        FileWriter fileWriter = new FileWriter(filePath);
        fileWriter.write("# Input directory:\t" + commandOptions.inputDirectory + "\n");
        fileWriter.write("# Minimum confidence:\t" + commandOptions.minCon + "\n");
        fileWriter.write("Antecedent\tConfidence\n");

        while (ARulesIterator.hasNext()) {
            AssociationRules.Rule<String> rule = ARulesIterator.next();

//            if (rule.javaConsequent().get(0).equals(domain)) {
                String writeLine = rule.javaAntecedent() + "\t" + rule.confidence() + "\n";
                fileWriter.write(writeLine);
//            }

        }
        fileWriter.close();
    }

//

    /**
     * Method to print the results from the frequency pattern mining
     */
    public void printFPMiningResults(JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD) {
        System.out.println("Printing Frequent Pattern Mining results");
        Iterator<FPGrowth.FreqItemset<String>> itemSets = freqItemsetJavaRDD.toLocalIterator();

        while (itemSets.hasNext()) {
            FPGrowth.FreqItemset<String> itemset = itemSets.next();
            System.out.println("[" + itemset.javaItems() + "], " + itemset.freq());
        }
    }

    /**
     * Method to print the association rules found in the frequency patterns.
     */
    public void printAssociationRules(JavaRDD<AssociationRules.Rule<String>> AssociationRules) {
        System.out.println("Printing association rules");

        Iterator<AssociationRules.Rule<String>> ARules = AssociationRules.toLocalIterator();
        // cannot collect large RDD at once because of memory failing


        while (ARules.hasNext()) {
            AssociationRules.Rule<String> rule = ARules.next();
            System.out.println(
                    rule.javaAntecedent() + " => " + rule.javaConsequent() + ", " + rule.confidence());
        }
    }

    /**
     * Auxiliary methods for trait detection. The java spark context cannot be inside the TraitDetector due to
     * serialization error with the map functions.
     *
     * @param traitNodes ArrayList of TraitNode objects
     * @return a javaRDD with the content of the input list.
     */
    public static JavaRDD<TraitNode> parallelizeTraitNodeList(ArrayList<TraitNode> traitNodes) {
        return sc.parallelize(traitNodes);
    }

    public static JavaRDD<TraitNode> createEmptyRDD() {
        return sc.emptyRDD();
    }

    public static JavaRDD<TraitNode> joinRDDs(JavaRDD<TraitNode> rdd1, JavaRDD<TraitNode> rdd2) {

        return sc.union(rdd1, rdd2);
    }

    /**
     * A method to test some of the functionality of this class. It checks whether every operon contained the
     * domain of interest at least once
     * @param freqItemsetJavaRDD the frequent pattern mining output
     * @param domain the domain for which the frequent pattern mining analysis was performed
     * @param operonHashMapSize, the number of operons analysed.
     */
    private static void checkCorrectness(JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD, String domain, int operonHashMapSize) {

        // Test whether every operon has the domain of interest in it
        List<FPGrowth.FreqItemset<String>> seedPattern = freqItemsetJavaRDD.filter(set -> set.javaItems().contains(domain) && set.javaItems().size() == 1).collect();
        long frequency = seedPattern.get(0).freq();
        if (frequency != operonHashMapSize) {
            LOGGER.error(">>> CAUTION: NOT EVERY REGION SEEMS TO HAVE THE DOMAIN OF INTEREST. Number of operons: " + operonHashMapSize + "; domain of interest frequency: " + frequency);
        }
    }
}