package nl.wur.ssb.DomainPatternRecognition;

import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.File;
import java.util.HashMap;

public class App {
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());


    public static void main(String[] args) throws Exception {

        // Log issues disables everything for spark
        org.apache.log4j.Logger.getLogger("org.apache.spark").setLevel(Level.OFF);

        CommandOptions commandOptions = new CommandOptions(args);

        if (commandOptions.debug) {
            org.apache.log4j.Logger.getLogger("org.apache.spark").setLevel(Level.DEBUG);
        }

        // Initiate the SPARK environment
        // SPARK ENVIRONMENT (this way it accepts multiple folders)
        LOGGER.info("# Initializing SPARK environment");
        SparkConf sConf = new SparkConf();
        sConf.setMaster("local[" + commandOptions.jobs + "]");
        // sConf.setMaster("spark://SSB130062.local:7077");
        sConf.setAppName("Genome DUFs");
        sConf.set("spark.executor.memory", "5g");
        sConf.set("", ""); // todo: check whether this line is necessary. What it does
        // Submits itself with the SPARK job...
        sConf.set("spark.jars", "HDTQuery.jar");
        // Recursively when needed... TODO likely to be removed
        if (commandOptions.recursiveInput) {
            sConf.set("spark.hive.mapred.supports.subdirectories", "true");
            sConf.set("spark.hadoop.mapreduce.input.fileinputformat.input.dir.recursive", "true");
        }
        JavaSparkContext sc = new JavaSparkContext(sConf);
        // Failsafe for graphX for trait detection...
        sc.setCheckpointDir(DomainPatternRecognition.checkPointDirectory);

        boolean correctInput = true;
        // Starting the application
        for (String inputDirName :commandOptions.inputDirectory) {
            File inputDirectory = new File(inputDirName);
            if (!inputDirectory.exists()) {
                correctInput = false;
                LOGGER.error(">>>FAILED: INPUT DIRECTORY " + inputDirName + " DOES NOT EXIST");
            } else if (!inputDirectory.isDirectory()) {
                correctInput = false;
                LOGGER.error(">>>FAILED: INPUT "+ inputDirName +" IS NOT A DIRECTORY");
            }
        }


        if (!correctInput) {
            throw new Exception("Input directory not correct. See error message above");
        }

        // TODO get all files function here with HDT check etc... for the rest of the program to use...

        // Container object used throughout the code
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions, sc);

        HashMap<String, Operon> operonHashMap = domainPatternRecognition.queryInputData(commandOptions.domain, true);
        // todo: @ Jasper From here you could continue with the operons.
        //   when you put the queryThoroughly on false, only the operons will be created and not the
        //   additional information in the domainFrequencyHashMap

        boolean operonsFound = domainPatternRecognition.createStatisticsOperonHashMap(operonHashMap);

        if (operonsFound) {
            domainPatternRecognition.setOperonHashMap(operonHashMap);
            // Checks if needs to perform LDA/FPM or association rules and enables FPM when needed.
            if (commandOptions.doLDAClusteringFPM || commandOptions.doAssocationRules) {
                // In order to do the LDA with the FPM results or the association rules, the FPM has to be executed as
                // well. So making sure that this goes alright.
                commandOptions.doFrequentPatternMining = true;
            }

            domainPatternRecognition.runFrequentPatternMining(commandOptions.domain);
            domainPatternRecognition.doLDAClusteringOperons();

            //todo this part really doesn't function yet...
            domainPatternRecognition.createFPMNetwork();
        }
    }
}