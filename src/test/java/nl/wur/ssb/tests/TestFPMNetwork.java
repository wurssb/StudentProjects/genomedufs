package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.App;
import org.junit.Test;

public class TestFPMNetwork {

    @Test
    public void testFPMNetwork3 () throws Exception {
        String[] args = {"" +
                "" +
                "-domain","PF00465",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/13PDOData_graphXoutput",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-minSupportFPMValues", "0.3",
                "-bidirectional",
                "-bidirectionalGapMinimum", "50", // todo change back to serious value.
                "-bidirectionalGapMaximum", "1000",
                "-geneGap" , "500",
                "-threads", "1",
                "-networkCutOff", "0.5",
                "-FPMNetwork"};

        App.main(args);
    }
}
