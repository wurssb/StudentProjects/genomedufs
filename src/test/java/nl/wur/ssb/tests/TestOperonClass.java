package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.objects.Gene;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class TestOperonClass {

    @Test
    public void testOrderGenes() {
        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("F", 10, 20);

        Operon operon = new Operon(gene1, "sample", "contig");

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("F", 19, 50);
        Gene gene3 = new Gene("gene3");
        gene3.addGeneLocationInfo("F", 60, 100);

        operon.addGene(gene2);
        operon.addGene(gene3);
        operon.addGene(gene1);

        System.out.println("before ordering");
        operon.printGenesInOperon();

        System.out.println("after ordering");

        operon.orderGenes();
        operon.printGenesInOperon();
        ArrayList<Gene> expected = new ArrayList<>(Arrays.asList(gene1, gene2, gene3));
        assert expected.equals(operon.getGenes());

    }
}
