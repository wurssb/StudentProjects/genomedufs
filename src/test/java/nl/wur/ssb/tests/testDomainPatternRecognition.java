package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.objects.Gene;
import nl.wur.ssb.DomainPatternRecognition.objects.Operon;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;

import java.util.HashMap;

public class testDomainPatternRecognition {
    private JavaSparkContext sc;

    private void setUpSC () {
        if (sc == null) {
            SparkConf sConf = new SparkConf();
            sConf.setMaster("local[1]"); // I'M ASSUMING ONE JOB HERE.
            // sConf.setMaster("spark://SSB130062.local:7077");
            sConf.setAppName("SAPP HDT Query");
            sConf.set("spark.executor.memory", "5g");
            sConf.set("", "");
            // Submits itself with the SPARK job...
            sConf.set("spark.jars", "HDTQuery.jar");

            // set input to recursive.
            sConf.set("spark.hadoop.mapreduce.input.fileinputformat.input.dir.recursive", "true");
            sConf.set("spark.hive.mapred.supports.subdirectories", "true");

            sc = new JavaSparkContext(sConf);
            sc.setCheckpointDir(DomainPatternRecognition.checkPointDirectory);
        }
    }

    @Test
    public void testCreateStatisticsOperonHashMap() throws Exception { // todo: to run this test, methods have to be public.
        // todo: and change the method to return the values that you are testing for.
        String[] args = {"-domain","PF00465",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/testOutput",
                "-recursiveInput",
                "-inputDirectory", "./data/dummy",
                "-minSupportFPMValues", "0.9",
                "-bidirectional",
                "-bidirectionalGap", "100",
                "-geneGap", "50",
        };

        CommandOptions commandOptions = new CommandOptions(args);
        setUpSC();
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions, sc);
        domainPatternRecognition.queryInputData("PF00465", true);

        HashMap<String, Operon> operonHashMap =  domainPatternRecognition.getOperonHashMap();

        System.out.println("----------- OperonHashMap -----------");

        for(Operon operon: operonHashMap.values()) {
            System.out.println("-----------------------------------");
            for (Gene gene : operon.getGenes()) {
                System.out.println(gene.getGeneID() + " " + gene.getStrand());
            }
        }

        // Testing the number of genes
//        ArrayList<Integer> numberOfGenes = new ArrayList<>();
//        for (Operon operon : operonHashMap.values()) {
//            numberOfGenes.add(operon.getGenes().size());
//        }
//
//        int sum = genomeDUFs.createStatisticsOperonHashMap();
//
//        int sumOwnCalculation = 0;
//        for (Integer number : numberOfGenes) {
//            sumOwnCalculation += number;
//        }
//        System.out.println("sum: " + sum);
//        assert sum == sumOwnCalculation;



        // Testing for the min and max
//        genomeDUFs.createStatisticsOperonHashMap();

    }

    @Test
    public void testQueryInputDifferentDomain() throws Exception {
        // A test to see whether it is really possible to make the code query for a different domain that the
        // commandOptions.domain
        String[] args = {"-domain","PF02278",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/testOutput",
                "-recursiveInput",
                "-inputDirectory", "./data/test",
                "-minSupportFPMValues", "0.1",
                "-regionWidth", "10000"};

        // Next try to get the GenomeDUF object
        CommandOptions commandOptions = new CommandOptions(args);
        setUpSC();
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions, sc) ;
        domainPatternRecognition.queryInputData("PF00465", true); // all the other parameters should remain the same.
        // like region width and stuff
        

    }
}
