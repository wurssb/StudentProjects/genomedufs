package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.DomainPatternRecognition;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import org.apache.log4j.Level;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;

public class TestInputQuerier {
    private JavaSparkContext sc;

    private void setUpSC () {
        if (sc == null) {
            SparkConf sConf = new SparkConf();
            sConf.setMaster("local[1]"); // I'M ASSUMING ONE JOB HERE.
            // sConf.setMaster("spark://SSB130062.local:7077");
            sConf.setAppName("SAPP HDT Query");
            sConf.set("spark.executor.memory", "5g");
            sConf.set("", "");
            // Submits itself with the SPARK job...
            sConf.set("spark.jars", "HDTQuery.jar");

            // set input to recursive.
            sConf.set("spark.hadoop.mapreduce.input.fileinputformat.input.dir.recursive", "true");
            sConf.set("spark.hive.mapred.supports.subdirectories", "true");

            sc = new JavaSparkContext(sConf);
            sc.setCheckpointDir(DomainPatternRecognition.checkPointDirectory);
        }
    }
    /**
     * Test class to test whether the input querier is functioning. This is the object orientated verion of
     * SPARQLQueryInput.java
     */


    /**
     * Test the class from the GenomeDUF class
     */
    @Test
    public void testFromGenomeDUFs () throws Exception {
        String[] args = {"-domain","PF02278",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/testOutput",
                "-recursiveInput",
                "-inputDirectory", "./data/test",
                "-minSupportFPMValues", "0.1",
                "-regionWidth", "10000"};

        org.apache.log4j.Logger.getLogger("org.apache.spark").setLevel(Level.ERROR);

        // Next try to get the GenomeDUF object
        CommandOptions commandOptions = new CommandOptions(args);
        setUpSC();
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions,sc) ;
        domainPatternRecognition.queryInputData("PF00465", true); // all the other parameters should remain the same.
        // like region width and stuff

    }

    @Test
    public void testInputQuerier2 () throws Exception {
        String[] args = {"" +
                "-domain","PF02278",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/testOutput",
                "-recursiveInput",
                "-inputDirectory", "./data/test",
                "-minSupportFPMValues", "0.5",
                "-regionWidth", "20000"};
        org.apache.log4j.Logger.getLogger("org.apache.spark").setLevel(Level.ERROR);
        CommandOptions commandOptions = new CommandOptions(args);
        setUpSC();
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions,sc);
        domainPatternRecognition.queryInputData("PF02277", true);
    }
}
