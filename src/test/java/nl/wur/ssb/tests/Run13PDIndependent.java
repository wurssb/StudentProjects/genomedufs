package nl.wur.ssb.tests;
import nl.wur.ssb.DomainPatternRecognition.App;
import org.junit.Test;

public class Run13PDIndependent {

    // The PF00465 is already examined in the B12 dependent pathway, but is here included for completeness

    @Test
    public void testPF00465 () throws Exception {
        String[] args = {"-domain","PF00465",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PD_Independent",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "3,4,5",
                "-LDAClustersOperons" , "3,4,5",
                "-minSupportFPMValues", "0.15",
                "-bidirectional",
                "-bidirectionalGap", "500",
                "-geneGap", "50",
        };
        App.main(args);
    }

    // todo: remember that this has a little less priority than writing the results. But would be very nice if I could fix this in a week. (11-11-2019)

    @Test
    public void testPF01228 () throws Exception { // todo: this one is failing due to a null pointer in the write domain info string. That is weird.
        String[] args = {"-domain","PF01228",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PD_Independent",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "3,4,5",
                "-LDAClustersOperons" , "3,4,5",
                "-minSupportFPMValues", "0.2",
                "-bidirectional",
                "-bidirectionalGap", "500",
                "-geneGap", "50",
//                "-detectTraits"
        };
        App.main(args);
    }

    @Test
    public void testPF02901() throws Exception{
        String[] args = {"-domain","PF02901",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PD_Independent",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "3,4,5",
                "-LDAClustersOperons" , "3,4,5",
                "-minSupportFPMValues", "0.2", // todo: find a suitable value for this.
                "-bidirectional",
                "-bidirectionalGap", "500",
                "-geneGap", "50",
//                "-detectTraits" // todo: once I have a proper value, try to detect the traits
        };
        App.main(args);
    }

    @Test
    public void testPF04055() throws Exception{
        String[] args = {"-domain","PF04055",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PD_Independent",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "3,4,5",
                "-LDAClustersOperons" , "3,4,5",
                "-minSupportFPMValues", "0.6", // todo: find a suitable value for this.
                "-bidirectional",
                "-bidirectionalGap", "500",
                "-geneGap", "50",
        };
        App.main(args);
    }
}
