package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.*;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.objects.Gene;
import nl.wur.ssb.DomainPatternRecognition.objects.Region;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class RegionExtractorTest {

    @Test
    public void testBidirectionalRegionExtractionForward1() throws Exception { // todo: working now that I added the right values for the bidrecitonal gaps.
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional"
        , "-bidirectionalGapMaximum", "30", "-bidirectionalGapMinimum", "10"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 50, 30, 10) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();

        Gene seed = new Gene("Seed") ;
        seed.addGeneLocationInfo("Forward", 60, 80);
        Region region1 = new Region(seed, "sample", "contig");
        region1.addGene(seed);

        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Forward", 1, 10);
        region1.addGene(gene1);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Reverse", 11, 15);
        region1.addGene(gene2);

        Gene gene3 = new Gene("gene3");
        gene3.addGeneLocationInfo("Forward", 20, 30);
        region1.addGene(gene3);

        Gene gene4 = new Gene("gene4");
        gene4.addGeneLocationInfo("Reverse", 35, 40);
        region1.addGene(gene4);

        Gene gene6 = new Gene("gene6");
        gene6.addGeneLocationInfo("Forward", 85, 100);
        region1.addGene(gene6);

        Gene gene7 = new Gene("gene7");
        gene7.addGeneLocationInfo("Reverse", 105, 120);
        region1.addGene(gene7);

        regionHashMap.put("testRegion1", region1);

        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();


        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(gene4, seed, gene6)) ;


        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    @Test
    public void testBidirectionalRegionExtractionForward2() throws Exception { // todo: this one is passing
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 50, 30, 10) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();

        Gene seed = new Gene("Seed") ;
        seed.addGeneLocationInfo("Forward", 1, 10);
        Region region1 = new Region(seed, "sample", "contig");
        region1.addGene(seed);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Reverse", 11, 15);
        region1.addGene(gene2);

        Gene gene3 = new Gene("gene3");
        gene3.addGeneLocationInfo("Forward", 20, 30);
        region1.addGene(gene3);

        Gene gene4 = new Gene("gene4");
        gene4.addGeneLocationInfo("Forward", 35, 40);
        region1.addGene(gene4);

        Gene gene6 = new Gene("gene6");
        gene6.addGeneLocationInfo("Forward", 85, 100);
        region1.addGene(gene6);

        Gene gene7 = new Gene("gene7");
        gene7.addGeneLocationInfo("Reverse", 105, 120);
        region1.addGene(gene7);

        regionHashMap.put("testRegion1", region1);

        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();


        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(seed)) ;


        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    @Test
    public void testBidirectionalReverse() throws Exception { // again adjusting the minimum gap threshold fixed this one. So the code might actually be working just fine.
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional"
                , "-bidirectionalGapMaximum", "30", "-bidirectionalGapMinimum", "0"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 50, 30, 0) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();


        // SETTING UP THE REGION AND THE GENES IN THE REGION
        Gene seed = new Gene("Seed");
        seed.addGeneLocationInfo("Reverse", 50, 59);

        Region region = new Region(seed, "sample", "contig");
        region.addGene(seed);

        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Reverse", 1, 20);
        region.addGene(gene1);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Forward", 25, 40);
        region.addGene(gene2);

        Gene gene4 = new Gene("gene4");
        gene4.addGeneLocationInfo("Reverse", 60, 80);
        region.addGene(gene4);

        Gene gene5 = new Gene("gene5");
        gene5.addGeneLocationInfo("Forward", 81, 90);
        region.addGene(gene5);

        Gene gene6 = new Gene("gene6");
        gene6.addGeneLocationInfo("Reverse", 100, 120);
        region.addGene(gene6);

        regionHashMap.put("testRegionReverse", region);
        region.printGenesRegion();

        // PERFORM THE ACTUAL OPERON EXTRACTION
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();

        // CREATE THE EXPECTED OUTPUT
        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(seed, gene4, gene5)) ;

        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    @Test
    public void testBidirectionalReverse2() throws Exception { // passing
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 50, 30, 10) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();


        // SETTING UP THE REGION AND THE GENES IN THE REGION
        Gene seed = new Gene("Seed");
        seed.addGeneLocationInfo("Reverse", 50, 59);

        Region region = new Region(seed, "sample", "contig");
        region.addGene(seed);

        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Reverse", 1, 20);
        region.addGene(gene1);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Forward", 25, 40);
        region.addGene(gene2);

        Gene gene4 = new Gene("gene3");
        gene4.addGeneLocationInfo("Reverse", 41, 45);
        region.addGene(gene4);


        regionHashMap.put("testRegionReverse", region);


        // PERFORM THE ACTUAL OPERON EXTRACTION
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();

        // CREATE THE EXPECTED OUTPUT
        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(gene4, seed)) ;

        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    @Test
    public void testPromoterGapThresholdForward() throws Exception { // test is working fine.
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional"
                , "-bidirectionalGapMaximum", "10", "-bidirectionalGapMinimum", "3"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 50, 10, 3) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();

        // SETTING UP THE REGION AND THE GENES IN THE REGION
        Gene seed = new Gene("Seed");
        seed.addGeneLocationInfo("Forward", 50, 59);

        Region region = new Region(seed, "sample", "contig");
        region.addGene(seed);

        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Forward", 1, 20);
        region.addGene(gene1);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Reverse", 21, 25);
        region.addGene(gene2);

        Gene gene3 = new Gene("gene3");
        gene3.addGeneLocationInfo("Forward", 40, 45);
        region.addGene(gene3);

        Gene gene4 = new Gene("gene4");
        gene4.addGeneLocationInfo("Reverse", 60, 65);
        region.addGene(gene4);

        regionHashMap.put("testRegionReverse", region);
        // PERFORM THE ACTUAL OPERON EXTRACTION
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();

        // CREATE THE EXPECTED OUTPUT
        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(gene3, seed)) ;

        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    @Test
    public void testPromoterGapThresholdReverse() throws Exception {
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional"
                , "-bidirectionalGapMaximum", "10", "-bidirectionalGapMinimum", "5"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 50, 10, 5) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();

        // SETTING UP THE REGION AND THE GENES IN THE REGION
        Gene seed = new Gene("Seed");
        seed.addGeneLocationInfo("Reverse", 20, 40);

        Region region = new Region(seed, "sample", "contig");


        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Forward", 2, 19);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Reverse", 50,60);

        Gene gene3 = new Gene("gene3") ;
        gene3.addGeneLocationInfo("Forward", 100, 110);

        Gene gene4 = new Gene("gene4");
        gene4.addGeneLocationInfo("Reverse", 115, 150);

        region.addGene(gene4);
        region.addGene(gene2);
        region.addGene(gene3);
        region.addGene(gene1);
        region.addGene(seed);

        regionHashMap.put("testRegionReverse", region);
        // PERFORM THE ACTUAL OPERON EXTRACTION
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();

        // CREATE THE EXPECTED OUTPUT
        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(seed, gene2)) ;

        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    // TEST CREATED TODAY (17 OCT)
    @Test
    public void testEnforceMaximumGapThresholdBi1() throws Exception {
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional", "-geneGap", "15"
                , "-bidirectionalGapMaximum", "10", "-bidirectionalGapMinimum", "5"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 15, 10, 5) ;
        HashMap<String, Region> regionHashMap = new HashMap<>();


        // SETTING UP THE REGION AND THE GENES IN THE REGION
        Gene seed = new Gene("Seed");
        seed.addGeneLocationInfo("Forward", 90, 100);

        Region region = new Region(seed, "sample", "contig");
        region.addGene(seed);

        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Reverse", 1, 5);
        region.addGene(gene1);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Reverse", 25, 40);
        region.addGene(gene2);

        Gene gene3 = new Gene("gene3");
        gene3.addGeneLocationInfo("Forward", 45, 50);
        region.addGene(gene3);

        Gene gene5 = new Gene("gene5");
        gene5.addGeneLocationInfo("Reverse", 105, 110);
        region.addGene(gene5);


        regionHashMap.put("testRegionReverse", region);


        // PERFORM THE ACTUAL OPERON EXTRACTION
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();
        region.printGenesRegion();

        // CREATE THE EXPECTED OUTPUT
        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(seed)) ;

        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }

    // TEST CREATED TODAY (17 OCT)
    @Test
    public void testEnforceMaximumGapThresholdBi2() throws Exception {
        // Test the method for when the seed is on the reverse strand
        String[] args = {"-domain", "PF02287", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output", "-recursiveInput", "-inputDirectory", "./data/test", "-bidirectional", "-geneGap", "15"
                , "-bidirectionalGapMaximum", "20", "-bidirectionalGapMinimum", "10"};
        CommandOptions commandOptions = new CommandOptions(args);

        RegionExtractor regionExtractor = new RegionExtractor(true, 15, 20, 10);
        HashMap<String, Region> regionHashMap = new HashMap<>();


        // SETTING UP THE REGION AND THE GENES IN THE REGION
        Gene seed = new Gene("Seed");
        seed.addGeneLocationInfo("Reverse", 50, 60);

        Region region = new Region(seed, "sample", "contig");
        region.addGene(seed);

        Gene gene1 = new Gene("gene1");
        gene1.addGeneLocationInfo("Reverse", 1, 5);
        region.addGene(gene1);

        Gene gene2 = new Gene("gene2");
        gene2.addGeneLocationInfo("Reverse", 25, 40);
        region.addGene(gene2);

        Gene gene3 = new Gene("gene3");
        gene3.addGeneLocationInfo("Reverse", 59, 70);
        region.addGene(gene3);

        Gene gene4 = new Gene("gene4");
        gene4.addGeneLocationInfo("Forward", 75, 90);
        region.addGene(gene4);

        Gene gene5 = new Gene("gene5");
        gene5.addGeneLocationInfo("Reverse", 95, 110);
        region.addGene(gene5);


        regionHashMap.put("testRegionReverse", region);


        // PERFORM THE ACTUAL OPERON EXTRACTION
        regionExtractor.setRegionHashMap(regionHashMap);
        regionExtractor.extractOperonsFromRawRegions();
        region.printGenesRegion();

        // CREATE THE EXPECTED OUTPUT
        ArrayList<Gene> expectedGenesOperon = new ArrayList<>();
        expectedGenesOperon.addAll(Arrays.asList(gene2, seed, gene3)) ;

        ArrayList<String> expectedToPrint = new ArrayList<>() ;
        expectedGenesOperon.forEach((gene -> expectedToPrint.add(gene.getGeneID())));
        System.out.println("Expected: " + expectedToPrint);


        for (String operonkey : regionExtractor.getOperonHashMap().keySet()) {
            ArrayList<Gene> resultsFromCode = new ArrayList<>(regionExtractor.getOperonHashMap().get(operonkey).getGenes());

            ArrayList<String> resultsToPrint = new ArrayList<>();
            resultsFromCode.forEach( (gene) -> resultsToPrint.add(gene.getGeneID()));
            System.out.println("code result: " + resultsToPrint);
            assert expectedGenesOperon.equals(resultsFromCode);
        }
    }


}
