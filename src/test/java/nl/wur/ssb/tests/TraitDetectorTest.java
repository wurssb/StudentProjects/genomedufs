package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.*;
import nl.wur.ssb.DomainPatternRecognition.FPM.FreqPatternsParallel;
import nl.wur.ssb.DomainPatternRecognition.commandOptions.CommandOptions;
import nl.wur.ssb.DomainPatternRecognition.traits.TraitDetector;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.fpm.FPGrowth;
import org.apache.spark.mllib.fpm.FPGrowthModel;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TraitDetectorTest {
    private JavaSparkContext sc;

    private void setUpSC () {
        if (sc == null) {
            SparkConf sConf = new SparkConf();
            sConf.setMaster("local[1]"); // I'M ASSUMING ONE JOB HERE.
            // sConf.setMaster("spark://SSB130062.local:7077");
            sConf.setAppName("SAPP HDT Query");
            sConf.set("spark.executor.memory", "5g");
            sConf.set("", "");
            // Submits itself with the SPARK job...
            sConf.set("spark.jars", "HDTQuery.jar");

            // set input to recursive.
            sConf.set("spark.hadoop.mapreduce.input.fileinputformat.input.dir.recursive", "true");
            sConf.set("spark.hive.mapred.supports.subdirectories", "true");

            sc = new JavaSparkContext(sConf);
            sc.setCheckpointDir(DomainPatternRecognition.checkPointDirectory);
        }
    }

    @Test
    public void testTraitDetector3() throws Exception {
        // Get a spark context etc.
        String[] args = {"-domain","b", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/dummyOutput_remove", "-recursiveInput", "-inputDirectory", "./data/dummy", "-threads", "1"};
        CommandOptions commandOptions = new CommandOptions(args);

        setUpSC();
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions, sc);
        domainPatternRecognition.queryInputData("b", true);
        JavaSparkContext sc = domainPatternRecognition.getSc();

        // create an input data set.
        JavaRDD<String> rddData = sc.textFile("/home/sanne/Documents/genomedufs_spark/src/test/resources/traitDetectionTestInput.txt");

        JavaRDD<List<String>> inputData = rddData.map(line -> Arrays.asList(line.split(" ")));

        FreqPatternsParallel freqPatternsParallel = new FreqPatternsParallel(commandOptions, sc, new HashMap<>(), new HashMap<>(), "b");

        // generate the FP mining.
        FPGrowth fpg = new FPGrowth() ;
        fpg.setMinSupport(0.4);
        fpg.setNumPartitions(1);

        FPGrowthModel<String> model = fpg.run(inputData);
        System.out.println("----------------- THE FREQUENT PATTERNS -----------------");
        for (FPGrowth.FreqItemset<String> itemset: model.freqItemsets().toJavaRDD().collect()) {
            System.out.println(itemset.javaItems() + ", " + itemset.freq());
        }
        // start with the trait detection.
        System.out.println("----------------- TRAIT DETECTION -----------------");
        TraitDetector traitDetector = new TraitDetector(model.freqItemsets().toJavaRDD(), "b", 0.4, 14);
        // make commandOptions.domain public for testing
    }

    @Test
    public void testTraitDetector3_2() throws Exception {
        // Get a spark context etc.
        String[] args = {"-domain","b", "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/dummyOutput_remove", "-recursiveInput", "-inputDirectory", "./data/dummy", "-threads", "1"};
        CommandOptions commandOptions = new CommandOptions(args);
        setUpSC();
        DomainPatternRecognition domainPatternRecognition = new DomainPatternRecognition(commandOptions, sc);
        domainPatternRecognition.queryInputData("b", true);
        JavaSparkContext sc = domainPatternRecognition.getSc();

        // create an input data set.
        JavaRDD<String> rddData = sc.textFile("/home/sanne/Documents/genomedufs_spark/src/test/resources/traitDetectionTestInput2.txt");

        JavaRDD<List<String>> inputData = rddData.map(line -> Arrays.asList(line.split(" ")));

        FreqPatternsParallel freqPatternsParallel = new FreqPatternsParallel(commandOptions, sc, new HashMap<>(), new HashMap<>(), "b");

        // generate the FP mining.
        FPGrowth fpg = new FPGrowth() ;
        fpg.setMinSupport(0.4);
        fpg.setNumPartitions(1);

        FPGrowthModel<String> model = fpg.run(inputData);
        System.out.println("----------------- THE FREQUENT PATTERNS -----------------");
        for (FPGrowth.FreqItemset<String> itemset: model.freqItemsets().toJavaRDD().collect()) {
            System.out.println(itemset.javaItems() + ", " + itemset.freq());
        }
        // start with the trait detection.
        System.out.println("----------------- TRAIT DETECTION -----------------");
        TraitDetector traitDetector = new TraitDetector(model.freqItemsets().toJavaRDD(), "b", 0.3, 9);
        // make commandOptions.domain public for testing

    }

}
