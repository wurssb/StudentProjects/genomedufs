package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.App;
import org.junit.Test;

public class appRuns2020 {

    @Test
    public void testRun13PD_dependent_reductive() throws Exception {

        String [] args = {
                "-domain","PF02287",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/13PDO-allDomains-geneGapExperiment",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-minSupportFPMValues", "0.5",
                "-regionWidth", "20000" ,
                "-bidirectional",
                "-geneGap", "30",
                "-LDAClusteringOperons", // this one is working
                "-LDAClusteringFPM", // this on works
//                "-getAssociationRules", // This on is running and working
                "-additionalDomains", "PF02288,PF02286,PF00465,PF03928",
                "-NumberLDAClustersOperons", "3,4",
                "-numberLDAClustersFPM", "3,4"
//                "-detectTraits" // This gives out of memory errors
                // using the default bidirectional stuff.
        } ;
        App.main(args);
    }

    @Test
    public void testRunPF00465() throws Exception {
        String [] args = {
                "-domain","PF00465",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/13PD-data-PF00465",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-minSupportFPMValues", "0.05",
                "-regionWidth", "20000" ,
                "-bidirectional",
                "-geneGap", "100",
                "-LDAClusteringOperons", // this one is working
                "-LDAClusteringFPM", // this on works
                "-getAssociationRules", // This on is running and working
                "-NumberLDAClustersOperons", "2",
                "-numberLDAClustersFPM", "2"
        };
        App.main(args);
    }

    @Test
    public void testExclusion() throws Exception {
        String [] args = {
                "-domain","PF02287",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/PF02286-7-8-exclusionTest",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-minSupportFPMValues", "0.4",
                "-regionWidth", "20000" ,
                "-bidirectional",
                "-geneGap", "100",
                "-LDAClusteringOperons", // this one is working
                "-LDAClusteringFPM", // this on works
//                "-getAssociationRules", // This on is running and working
                "-additionalDomains", "PF02288,PF02286,PF08841",
                "-NumberLDAClustersOperons", "3,4",
                "-numberLDAClustersFPM", "3,4",
                "-excludedDomains" , "PF00465"
        };
        App.main(args);
    }

}
