package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.App;
import org.junit.Test;


public class runsForPaper {

    private static void runForDomain(String domain, String FPMiningSupports) throws Exception {
        String[] args = {"-domain", domain ,
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/DomainsAroundPF00465",
//                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/testOutput",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
//                "-inputDirectory", "./data/dummy",
                "-minSupportFPMValues", FPMiningSupports,
                "-bidirectional",
                "-bidirectionalGapMinimum", "100",
                "-bidirectionalGapMaximum", "1000",
                "-geneGap", "500",
        };

        App.main(args);
    }

    // Domains found in the 1,3 PD pathway
    // TODO: MAKE SURE TO HAVE THE PROPER OUTPUT PATH.

    @Test
    public void testPF00465() throws Exception { // queried
        runForDomain("PF00465", "0.1");
    }

    @Test
    public void testPF02286() throws Exception { // queried.
        runForDomain("PF02286", "0.7");
    }

    @Test
    public void testPF02287() throws Exception { // queried
        runForDomain("PF02287", "0.7");
    }

    @Test
    public void testPF02288() throws Exception { // queried
        runForDomain("PF02288", "0.7");
    }

    @Test
    public void testPF08841() throws Exception{
        runForDomain("PF08841", "0.7");
    }

    @Test
    public void testPF02734() throws Exception { // queried
        runForDomain("PF02734", "0.7");
    }

    @Test
    public void testPF02733() throws Exception { // queried
        runForDomain("PF02733", "0.7");
    }

//    @Test
//    public void testPF13684() throws Exception { // queried
//        runForDomain("PF13684", "0.4");
//    }
//
//    @Test
//    public void testPF01228() throws Exception { // queried
//        runForDomain("PF01228", "0.2");
//    }
//
//    @Test
//    public void testPF02901() throws Exception { // queried
//        runForDomain("PF02901", "0.2");
//    }
//
//    @Test
//    public void testPF04055() throws Exception { // working on
//        runForDomain("PF04055", "0.1");
//    }

    // domains found in the PF00465 output. Using this to see what kind of patterns I could get.

    // TODO: CHANGE THE OUTPUT PATH WHEN RUNNING THESE.
    @Test
    public void testPF00005() throws Exception { // queried // redoing the regions from there with the bidirectional stuff fixed
        runForDomain("PF00005", "0.7");
    }

    @Test
    public void testPF00171() throws Exception { // queried
        runForDomain("PF00171", "0.7");
    }

    @Test
    public void testPF12833() throws Exception { // queried
        runForDomain("PF12833", "0.7");
    }

    @Test
    public void testPF00072() throws Exception { // todo failing to write the domain info string
        runForDomain("PF00072", "0.7");
    }

    @Test
    public void testPF02518() throws Exception { // queried
        runForDomain("PF02518", "0.7");
    }

    @Test
    public void testPF07690() throws Exception { // queried
        runForDomain("PF07690", "0.7");
    }

    @Test
    public void testPF03466() throws Exception { // queried
        runForDomain("PF03466", "0.7");
    }

    @Test
    public void testPF00126() throws Exception { // queried
        runForDomain("PF00126", "0.7");
    }

    @Test
    public void testPF00528() throws Exception{ // queried
        runForDomain("PF00528", "0.7" );
    }

    @Test
    public void testPF00936() throws Exception { // queried
        runForDomain("PF00936", "0.7");
    }

    @Test
    public void testPF01923() throws Exception{ // queried
        runForDomain("PF01923", "0.7");
    }

    @Test
    public void testPF03319() throws Exception{ // queried
        runForDomain("PF03319", "0.7");
    }

    @Test
    public void tesPF07992() throws Exception{ // queried
        runForDomain("PF07992", "0.7");
    }

    @Test
    public void testPF00155() throws Exception{ // queried
        runForDomain("PF00155", "0.7");
    }

    @Test
    public void testPF00486() throws Exception{ // queried
        runForDomain("PF00486", "0.7");
    }

    @Test
    public void testPF04055_2() throws Exception { // queried
        runForDomain("PF04055", "0.7");
    }

    @Test
    public void testPF00512() throws Exception{ // queried
        runForDomain("PF00512", "0.7");
    }

    @Test
    public void testPF00356() throws Exception{ // queried
        runForDomain("PF00356", "0.7");
    }

    @Test
    public void testPF13561() throws Exception{ // working on
        runForDomain("PF13561", "0.7");
    }


    /**
     * This is not really a test but more the code necessary to run this analysis.
     */
    @Test
    public void testRunForPapers() {

    }





}
