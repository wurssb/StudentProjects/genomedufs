package nl.wur.ssb.tests;

import nl.wur.ssb.DomainPatternRecognition.App;
import org.junit.Test;

public class runCodeForDomains {

    @Test
    public void testMainPaperDataPF02288() throws Exception {

        String[] args = {"-domain","PF02288",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PDO",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
//                "-LDAClustersFP" , "2,3", // original values
//                "-LDAClustersOperons" , "2,3",
//                "-minSupportFPMValues", "0.7,0.75,0.8",
                "-LDAClustersFP" , "2",
                "-LDAClustersOperons" , "2",
                "-minSupportFPMValues", "0.7", // smaller for testing
                "-bidirectional",
                "-bidirectionalGap", "100",
                "-geneGap", "50",
        };
        App.main(args);
    }

    @Test
    public void testMainPaperDataDomainPF02286() throws Exception {
        String[] args = {"-domain","PF02286",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PDO",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "2,3", // original values
                "-LDAClustersOperons" , "2,3",
                "-minSupportFPMValues", "0.7",
//                "-LDAClustersFP" , "2",
//                "-LDAClustersOperons" , "2",
//                "-minSupportFPMValues", "0.7", // smaller for testing
                "-bidirectional",
                "-bidirectionalGap", "100",
                "-geneGap", "50",
        };
        App.main(args);
    }

    @Test
    public void testMainPaperDataDomainPF02287() throws Exception {
        String[] args = {"-domain","PF02287",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PDO",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "2,3", // original values
                "-LDAClustersOperons" , "2,3",
                "-minSupportFPMValues", "0.7",
//                "-LDAClustersFP" , "2",
//                "-LDAClustersOperons" , "2",
//                "-minSupportFPMValues", "0.7", // smaller for testing
                "-bidirectional",
                "-bidirectionalGap", "100",
                "-geneGap", "50",
        };
        App.main(args);
    }

    @Test
    public void testMainPaperDataDomainPF00465() throws Exception {
        String[] args = {"-domain","PF00465",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PDO",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "5",
                "-LDAClustersFP" , "3", // original values
                "-LDAClustersOperons" , "5",
                "-minSupportFPMValues", "0.15",
//                "-LDAClustersFP" , "2",
//                "-LDAClustersOperons" , "2",
//                "-minSupportFPMValues", "0.7", // smaller for testing
                "-bidirectional",
                "-bidirectionalGap", "100",
                "-geneGap", "50",
//                "-detectTraits"
        };
        App.main(args);
    }

    @Test
    public void testPF02287LowerSupport() throws Exception {
        String[] args = {"-domain","PF02287",
                "-outputDirectory", "/home/sanne/Documents/genomedufs_spark/Output13PDO",
                "-recursiveInput",
                "-inputDirectory", "./13PDOData",
                "-LDAVocabSize" , "10",
                "-LDAClustersFP" , "2,3", // original values
                "-LDAClustersOperons" , "2,3",
                "-minSupportFPMValues", "0.5",
                "-bidirectional",
                "-bidirectionalGap", "100",
                "-geneGap", "50",
                "-detectTraits"
        };
        App.main(args);
    }
}
