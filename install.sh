#!/bin/bash
#============================================================================
#title          :SAPP Conversion
#description    :EMBL/FASTA <-> RDF installation script for SAPP
#author         :Jasper Koehorst
#date           :2016
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

if [ "$1" == "test" ]; then
	gradle build -b "$DIR/build.gradle" --info
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test --stacktrace
fi

cp $DIR/build/libs/*jar $DIR/

# java -jar $DIR/Conversion.jar --help

