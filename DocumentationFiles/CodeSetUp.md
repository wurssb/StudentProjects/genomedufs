# Application

[TOC]

# To do list



- [x] General set up of the app
  - [x] querying
  - [x] FPM
  - [x] Assocation rules
  - [x] Networks 
  - [x] LDA
- [x] App
- [ ] Command Options -> **UPDATE THIS AFTER THE LAST ALTERATIONS**
- [ ] Spark Environment and set up
- [x] Genome DUFs
- [x] InputQuerier
- [x] Frequent pattern mining
- [x] LDA clustering
  - [x] The algorithm
  - [x] The implementation 
- [x] FPM Network
- [x] Dependencies
- [x] Supporting classes and objects
  - [x] Jaccard index
  - [x] Operons
  - [x] Genes
  - [x] region extractor
  - [x] region
- [x] Queries
  - [x] GenesAroundDomain
  - [x] ProteinDomainInGene
  - [x] QueryAllDomainInfoForGenome
- [ ] Rearrange the order of the sections. Dependencies should be a bit higher up I think. Or maybe even in the ReadMe. Not sure here. 

# General concept of the application

This application takes annotated genome files in RDF saved in .hdt format to construct operon-like-structures (operons for further reference) around one or more user-specified protein domains. These operons can be analyzed with the following algorithms.

## Frequent pattern mining

Frequent Pattern Mining looks at which protein domains co-occur often in the operons. 

To enable the Frequent Pattern Mining functionality run the code with the option "-doFrequentPatternMining" and define the minimum support values for the definition of *frequent* with "-minSupportValues ". A list of values can be provided (e.g. 0.7,0.8). The support  value defines how often a pattern should relatively occur to picked up (when a pattern occurs in 6 out of 10 operons, the support value is 0.6). 

Currently the application will process only patterns of size 1 (meaning a single domain). The output will be a list of all domains that occur in the operons above the minimum support value. 

Using the option "-saveRawFPMOutput", all the significantly supported patterns will be written to a .txt file.

Frequent Pattern mining does **not** look at the order of domains or their copy number in the operon. 

For details on implemented algorithm, the code, the required input and output see [this section](##Frequent Pattern Mining - details).

## Association rules 

From the Frequent Pattern Mining results, [association rules](https://en.wikipedia.org/wiki/Association_rule_learning) can be derived. Association rules can be used to discover relationships between protein domains. 

Association rules identify which domains are predictive for others. Example: [a,b,c] -> [d] 0.8. This means that when domain a, b and c are in an operon, in 80% of the cases 'd' is also present.

When there are relatively few operons (like less than 10), the amount of association rules can be extremely high (all possible permutations)

These output from this algorithm is not (yet) processed. The raw results are written to a .txt file. More info on the exact algorithm and the code can be found in [this section](#Association rules - details)

## Latent Dirichlet Allocation

[Latent Dirichlet Allocation](https://en.wikipedia.org/wiki/Latent_Dirichlet_allocation) is a topic model. This statistical model is used to discover hidden topics in the operons. It clusters the operons based on the protein domains and can be applied to find whether there might be multiple biological functions in the operons surrounding the domain(s) of interest. 

To enhance the clustering quality, a vocabulary of domains is used on which the clustering will happen. The filtering of the domains can be done in two ways.

1. With the option: "-LDAClusteringOperons". The LDA clustering be performed once while retaining all domains in the operons. Next, from every created topic, the "-LDAVocabSize" number of domains which were assigned the heaviest weight, will be selected. The domain lists of the operons are filtered to only contain these domains. The clustering is redone based on these new domain lists. The number of clusters can be defined using "-NumberLDAClustersOperons". 
2. With the option: "-LDAClustersFPM". The application will use all domains that are found by the frequent pattern mining with the specified support value, to filter the domain lists and to perform the clustering. The number of clusters can be defined with "-numberLDAClustersFPM".

For details on the implemented algorithm and the code see [this section](#LDA clustering - details).

## FPM network analsys

***This part of the code is still under construction and does not yet function***

The general idea behind this part is to run the frequent pattern mining with a more common protein domain (domain a). The frequent pattern mining will also be performed on all the domains that are frequently found together with domain a. All these results can be combined in a network analysis. Where connections can be made between two domains when they are found to be frequent in operons constructed surrounding them. This could help illuminate the different biological function / domain composition of different operons constructed around a more common domain. 

**todo: maybe insert an image to illustrate this part?**

# Code set up

This is a step by step walk through of the code. Explanations of the process, the fields and variables, reasoning and points of improvement will provided.

## App

App is the main application containing one static main function. Main() takes in the a String-array with the command line arguments. The main method does the following

- It sets the Apache Spark Loggers to OFF
- Create a [CommandsOptions](##command-options) object containing and parsing the provided command line arguments
- Set up the [SPARK Environment](##SPARK environment) and context
- Check the provided input directory and terminate the program when it does not exist
- Create a [GenomeDUFs object](##genome-dufs) providing the command options and the spark context. 
  - [Query the input data](#inputquerier) 
  - [run frequent pattern mining](##frequent-pattern-mining) 
  - [do LDA clustering](##lda-clustering) 



## Command Options 

The Command Options class holds and parses all the parameters provided through the command line by the user.

### Parameters

The parameters that can be defined.

| Parameter              | Descriptions / explanation                                   | Default                             | Required |
| ---------------------- | ------------------------------------------------------------ | ----------------------------------- | -------- |
| jobs                   | The number of threads that the application will distribute the work over | 4                                   |          |
| inputDirectory         | The input directory or a list of directories separated by commas. There should be .hdt files in this directory | -                                   | True     |
| recursiveInput         | Boolean. whether the application should go recursively through the input directory | False                               |          |
| outputPath             | Directory to which the output will be written                | -                                   | True     |
| domain                 | Domain identifier around which the regions / operon like structures will be created. Should match to the database that is used. | -                                   | True     |
| additionalDomains      | Additional domains that need to be present in the regions. When prior knowledge is available, set the 'domain' variable to the most specific domain and provide the more generic domains after this parameter. Domains should be seperated only by a comma, so no white spaces (e.g. PF00005,PF00465) | empty ArrayList                     |          |
| db                     | The database that should be used to identify protein domains. Default is PFAM. This database should match the identifiers that are provided by 'domain' and 'additionalDomains' | http://gbol.life/0.1/db/pfam        |          |
| width                  | The number of base pairs before and after the seed gene that will be scanned to construct the operons | 20000                               |          |
| geneGap                | The maximum amount of base pairs that is allowed as the intergenic distance between two genes to be considered to belong to the same operon | 50                                  |          |
| originProv             | Provenance or origin of the genome annotation. To make sure that all genes used by the application are predicted in the same manner. | http://gbol.life/0.1/Prodigal/2_6_3 |          |
| bidirectional          | Boolean, whether [bidirectional operons](####bidirectional-operon-detection) should be considered. If True, the application will create operons that consist of a forward and a reverse part, with a separating space defined by bidirectionalGapMax and bidirectionalGapMin | False                               |          |
| bidirectionalGapMax    | The maximum amount of base pairs that is allowed between the reverse and forward part of one operon | 1000                                |          |
| bidirectionalGapMin    | The minimum amount of base pairs that is allowed between the reverse and forward part of one operon | 100                                 |          |
| minSupportValues       | The minimal support values for [frequent pattern mining](##frequent-pattern-mining) patterns. Multiple values can be provided separated by a comma (e.g. 0.7,0.8) | 0.7                                 |          |
| saveRawFPMOutput       | Whether all patterns found by the Frequent Pattern mining will be saved to a .txt file | false                               |          |
| minCon                 | Minimal confidence for the [association rules](###association rules) to be written to a file | 0.8                                 |          |
| LDAClusterKFreqPattern | The number of clusters used for applying [LDA clustering](###lda-clustering) on the operons using the domains resulting from the frequent pattern mining as the [vocabulary](fpm-selected-domains) | 2,5                                 |          |
| LDAClustersKOperons    | The number of clusters used when performing [LDA clustering](###lda-clustering) based on the top most explanatory domains as [vocabulary](###most-explanatory-domains) | 2,5                                 |          |
| LDAVocabSize           | The number of domains [most explanatory domains](####most-explanatory-domains) used to select the vocabulary on which the domains are clustered based on LDA. | 10                                  |          |
| minNumberOfOperons     | Minimum number of operons that need to be present in the data, before trying to gather the association rules. With a low number of operons, there can be billions of association rules (all permutations of list of domains) | 10                                  |          |
| detectTraits           | Boolean. Whether the application will try to clean up the frequent pattern mining output by removing patterns that are only in the results do to containment in bigger patterns. [Trait detection](###trait-detection). This can result in *Out of Memory* errors. | False                               |          |
| doLDAClusteringOperons | Boolean. Whether the application will perform LDA clustering on the operons using the top most explanatory domains as the [vocabulary](###vocabulary-selection) | False                               |          |
| doLDAClusteringFPM     | Boolean. Whether the application will perform [LDA clustering](##lda-clustering) on the operons using the | False                               |          |
| doFPMNetwork           | Boolean. Whether to code will try to create a [network](##fpm-network) based on frequent pattern mining results | False                               |          |
| networkCutOff          | The [cutoff support value](###cut-off-value) that is minimal required to create a connection between two domains  based on the frequent pattern mining | 0.7                                 |          |

## Spark environment

To initialize a SparkContex, you start with creating a SparkConf object. By setting the setMaster to local, you define that the application will run in a single JVM, meaning that it can be executed in the IDE itself. When you want to run the application on a Spark Cluster, you need to set the master to a Spark Master URL **Figure out how this would work -> should be in chapter 6 of that book**

One application can only contain one Spark Context and this is mandatory. 

```java
SparkConf sConf = new SparkConf();
sConf.setMaster("local[" + commandOptions.jobs + "]");
```

**To do: make this a bit more elaborate. Start to understand this myself a bit better**

## Genome DUFs

The *GenomeDUFs* object is the main object of the application. It guides the querying and machine learning. The most important objects gathered by this class are the [domain frequency hashmap](###domain-frequency-hashmap) and the [operon hashmap](###operon-hashmap). The machine learning algorithms provide their output as files. 

### Initializing GenomeDUFs

Parameters:

- CommandOptions
- JavaSparkContext

Upon initialization of the domainPatternRecognition the following directories are created if they do not yet exist

| Directory name                                               | Purpose                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| LookUpFiles                                                  | General directory to save all intermediate files created by the application. By saving intermediate results of the queries, not all queries have to be repeated when some parameters are changed |
| LookUpFiles/domainLookUp                                     | Contains per input genome the count of all the domains that occur in that genome and it contains [domain frequency hashmaps](###domain-frequency-hashmap). These hashmaps are specific for one domain |
| LookUpFiles/Regions\_*domainID* \_searchWidth\_*SearchWidthParameter* | Per input genome, there is file containing all the genes that are found surrounding a gene containing the domain of interest. It also contains [operon hashmaps](###operon-hashmap) for specific search parameters |
| SparkCheckPoints                                             | This directory can be used by the Spark Context whenever checkpoints are necessary (mainly for the GraphX application, with the [network analysis](##fpm-network)) |

### Operon HashMap

The Operon HashMap is the hashmap where all extracted operons are stored. 

key = ID of initial gene containing the domain of interest
value = [Operon](###operon) object

The Operon HashMap object is saved as a serialized file, in the Look Up directory in the region-subdirectory specific for the domain of interest around which it was created. 

The name of the operon hashmap is generated using the GenomeDufs.generateHashedOperonHashMapName(). This method generates a hashed name based on all the variables that make this operon hashmap unique. 

### Domain Frequency HashMap

HashMap that contains all the counts of domains and in which species they occur and whether that species also contains the domain of interest. 

Key = a domain ID - Every domain that occurs at least once in any of the input files is included.

Value = HashMap with the following keys:

- domainDescription - functional description linked to the domain ID
- sampleOrganismWithout - String with comma separated all the species that do contain the key domain but **not** the domain of interest
- sampleOrganismWith - String with comma separated all the species that do contain the key domain and the domain of interest
- domainCountWithout - How often the domain occurs without also having the domain of interest in the genome
- domainCountWith - How often the domain occurs together with the domain of interest in the genome. 

The Domain Frequency HashMap object is saved as a serialized object in the [LookUpFiles/domainLookUp directory](###Initializing-genomedufs). During the frequent pattern mining, this hashmap is used to save average copy numbers etc. 

### Querying input data - queryInputData (String *domainToQuery*, boolean *queryThoroughly*)

The public method queryInputData() takes care of the complete querying process with the help of auxiliary methods. The *domainToQuery* variable allows this function to also be applied for any other domain that the commandOptions.domain. When queryThoroughly is true, the domain frequency hash-map will also be constructed specifically for the *domainToQuery*.

First the output directory, the lookup directory and the region directory (seed domain specific) will be created if they do not exist yet. 

To execute the queries and to extract the operons and the domain frequencies, an [InputQuerier](##inputquerier) object is initialized and the queryInputData() method is executed.

After the querying was successful and the operon like structures are gathered and the following statistics are calculated and written to the file: 1\_*domainID*-search-parameters-and-operon-architecture.txt

- Average, min and max number of genes in one operon and the standard deviation
- Average, min and max gap between two genes on the same strand in the operon and the standard deviation
- Average, min and max gap between the forward and reverse part of the operon and the standard deviation

### Machine learning algorithms

The machine learning algorithms both have guiding methods

runFrequentPatternMining() will run the [frequent pattern mining](##frequent-pattern-mining)

doLDAClusteringOperons() will perform the [LDA clustering](##lda-clustering)

## InputQuerier

This class takes care of all the input files and creates the [Operon HashMap](###operon-hashmap) and the [Domain Frequency HashMap](###domain-frequency-hashmap), using one method, namely *queryInputData()*. All other methods are auxiliary to this the *queryInputData()*.

This method returns the Operon HashMap and creates a serialized file for both the operon HashMap as well as the Domain Frequency HashMap.

### Gathering all files

First step, is to gather all the input HDT files from the input directory and to parallelize them into one JavaRDD. This RDD is then filtered to only retain the HDT files that are in the input directory, as the directory might as well contain .index files or other non-hdt files. Only files are retained that:

- do **not** end with 'index.v1-1'
- do **not** end with '.index'
- the file contend starts with ''$HDT' (*GenomeDUFs.checkHDT*)

This Files RDD is used to process every file in parallel and to extract all the necessary information from the HDT files. On this Files-RDD the static queryOneInputFile() method is mapped.

### Internal checks for already existing intermediate results

To prevent unnecessary computations, the application will check whether there are already any intermediate or end results present. The first step in this checking is creating the [Operon HashMap](###operon-hashmap) and [Domain Frequency HashMap](###domain-frequency-hashmap) file names. These are MD5 hashed names based on the relevant search parameters and the input files. These unique names are always generated with the same procedure. This ensures that - only when the exact same conditions are meet - the application can just get the results from previous runs, instead of having to do identical computations. 

After every run of the application, the Operon HashMap and the Domain Frequency HashMap are saved with these MD5 hashed names.

There are four scenarios concerning the presence of the end results of previous runs:

1. Both the Operon HashMap and the Domain Frequency HashMap have already been created in a previous runs and can just be loaded in. The input files will not be processed.
2. The operon HashMap does **not** exist, but the Domain Frequency HashMap does. The Domain Frequency HashMap will be loaded in and the files will just be queried only to [obtain the operons](###creating-the-operon-hashmap). 
3. The Operon HashMap exists, but the Domain Frequency HashMap does **not**. The Operon HashMap will be loaded in and the input files will be queried to [create Domain Frequency HashMap](###creating-the-domain-frequency-hashmap). 
4. **Neither** exists and the input files will be queried to obtain both. 

When any processing of the input files is necessary (either for the Operon HashMap or the Domain Frequency HashMap) the *queryOneInputFile()* method is flatMapped against the [Files RDD](###gathering-all-files). 

### Creating the Operon HashMap

The first step is creating the file objects for where the query results for the regions will be saved ([fileRegionsInGenome](###GenesAroundDomain.sparql)) and one for the domain counts in one genome ([fileGenomeLookUp](###queryalldomaininfoforgenome.sparql)). These names are systematically created to be able to reuse files from previous runs. 

*queryOneInputFile()* is flatMapped against the [Files RDD](###gathering-all-files) and returns an iterator with [Operon](###operon) objects. If the Operon HashMap already exists, this iterator will be empty. 

When the Operon HashMap still needs to be created, the necessary resources for the queries will be mounted, namely the hdt, the graph and the model, which are created from the input HDT file. These resources are also closed once finished. 

The static *getOperons(fileRegionsInGenome, model)* method is then called to return a HashSet of Operon objects that are extracted from one genome file. 

#### Get operons around a domain

**Step 1** *, createRegionQueryResults()*: If the *fileRegionsInGenome* does not yet exists, the [GenesAroundDomain.sparql](###GenesAroundDomain.sparql) is executed and the results from this query are saved as a .tsv file as the *fileRegionsInGenome*.

**Step 2**, *processRegionsToHashMap(fileRegionsInGenome)*: This method reads the *fileRegionsInGenome* and parses all the regions from the list of genes and returns as HashMap with [Region](###region) objects. A new [Region](###region) Object is created for every seed gene (a gene containing the domain of interest) and every gene around this seed gene is added as a [Gene](###gene) object, with the information on the location (position and strand) of the gene. All regions are put into a HashMap. Key = seed gene, Value = Region object. In the case that the seed gene contains multiple copies of the domain of interest, the region will only be added once due to the overlapping keys. 

**Step 3** *,extractOperons(regionHashMap)*: The method takes the Region HashMap and creates a [RegionExtractor](###regionExtractor) Object. The public method *RegionExtractor.extractOperonsFromRawRegions()* can be called and it will filter the genes in the regions, depending on whether the user wants bidirectional operons or not. 

##### Bidirectional operon detection

A bidirectional operon contains first a stretch of genes on the reverse strand, followed by a stretch of genes the forward strand. 

1. The genes in the region are ordered based on their begin position. 
2. An [Operon](###operon) object is created, with info on the seed gene, like the strand and position
3. Looping through the ordered genes of the region, genes are added to the operon in such a way that there is a reverse and forward part, where the seed gene is contained in either one of them. 
4. These genes are then re-evaluated so that the genes. The gap between the reverse and forward strands has to be within the range provided by the user. If the gap does not suffice, the strand not containing the seed genes is discarded. 
5. The gap between the genes on the same strand may not exceed the gap threshold. If there is a gap exceeding this limit, only the genes on 'the seed gene side of the gap' are retained in the operon. 
6. The operon is put into a hashmap with the seed gene ID as key

##### Non bidirectional operon detection

A non bidirectional operon means that all the genes in the operon lie on the same strand. To extract such an operon from the region, the genes are look through from the seed gene. Whenever genes on the opposite strands are found, these and all further genes are removed from the operon. Next, step 5 en 6 are repeated from the [bidirectional detection](####bidirectional-operon-detection). 

**Step 4**, [*mergeOverlappingOperons()*](####merge-overlapping-operons): this method from the region extractor, merges all operons that contain (partly) the same genes in to one operon. Overlapping operons can be created when the domain of interest is present in two neighboring genes.

Two operons that lie are found in the same sample and on the same contig are compared to each other to check whether they overlap. The start and end position of both are compared and if there is an overlap a new Operon is created containing the distinct genes from both original operons. Both the original Operons are removed from the Operon HashMap. The genes in the merged Operon are again ordered and than added to the Operon HashMap. 

**Step 5**, is to extract the domains form the operons found in one genome HDT file. The method *extractDomainsFromOperons* will for every gene in the operon extract all the protein domains and add these to the Operon object. The [ProteinDomainsInGene.sparql](###ProteinDomainsInGene.sparql) query is used for this. 

If the user provided additional domains that need to be present in the operon, this is the step where they will be filtered. Every operon that does not contain all the domains will be kicked out. Here the code changes the ArrayList of domains to a HashSet to have a more efficient time complexity for the containsAll() method. 

### Creating the domain frequency hashmap

If the GenomeLookUp File does not yet exists and the *queryThoroughly* boolean is True, the *queryGenomeLookUpFile()* method will be called to prepare for the calculations for the Domain Frequency HashMap. The domain frequency hashmap content depends on the exact input data and on the seed domain.

The [QueryAllDomainInfoForGenome.sparql](###QueryAllDomainInfoForGenome.sparql) is executed on every HDT file and the results are saved to a .tsv file. These .tsv files are then analyzed to extract the domain frequencies by the *calculateDomainFrequencies(JavaRDD<File>)* method. 

This method first calls a flatmapToPair with the *getFrequenciesPerDomain()* static method on the files RDD which contains all the input files. *getFrequenciesPerDomain()* first get the right .tsv file that belongs to the input  genome. This file is that parsed by *parseGenomeLookUpFile(File)* into a HashMap. Every key is a domain that occurs in the genome and the value is a Tuple containing the functional description of the domain and the count. Also once the organism name is added to this hashmap with the key "Organism" and a count of "-1". 

The raw parsed data of every is then further processed. Here it is taken into account whether the genome also contains the seed domain. When it does the counts are added to *domainCountWith* and the organism name to *sampleOrganismWith* keys. Otherwise they are added to the *without* counterparts. The FlatMap gives a PairedRDD with the domain counts from every genome. 

From this PairedRDD<domainID, HashMap<String, String> all same keys that are the same (meaning all the domains that occur in more than one genome) are merged into one by a *reduceByKey* with the *reduceHashMaps* static method. Every hashmap that belongs to one domain ID are merged to yield per domain one hashmap containing the keys *domainCountWith* (also having the seed domain in the genome), *domainCountWithout* (having the seed domain in the genome), *sampleOrganismWith*, *sampleOrganismWithout*, *domainDescription*. After this manipulation of the PairedRDD it now contains for every domain once an entry containing the counts and description of the each domain.

The next step is to check that hashmap of every domain contains all the keys as mentioned above. For example, it is possible that a domain always occurs together in a genome with the seed domain. Therefore, it is still for the rest of the code essential that the hashmap also contains the *domainCountWithout* and the *sampleOrganismWithout* keys. 

Now the frequencies of the domains have been gathered and next this is all put in one hashmap. Now this is done by iterating through the RDD and putting it into a hashmap. **<u>Todo: this step should be optimized and could definitely be a speed improvement</u>**. Unfortunately, the the a *collectAsMap()* fails with an out of memory error. At the end, the domain frequency hashmap is serialized and saved for reuse when possible. 

## Frequent Pattern Mining - details

This class is a child of the [MachineLearningParallel](### machinelearningparallel) class that contains some of the basic methods that are also shared with the class for the Latent Dirichlet allocation. 

### The Algorithm

The Frequent Pattern growth algorithm is described in [this paper](https://link.springer.com/article/10.1023/B:DAMI.0000005258.31418.83). The parallel implementation available in Spark MlLib is described in [this paper](https://static.googleusercontent.com/media/research.google.com/nl//pubs/archive/34668.pdf). The implemented algorithm has a near linear time complexity. 

In very short how the algorithm works:

1. Sharding: The database is divided into successive parts and stored on different ‘computers’.
2. Parallel counting: count the support values of all items that appear in DB. This implicitly gets all unique items in the database (I). This is stored in an F-list. 
3. Grouping items: Dividing all items (I) into Q groups. The list of groups is called the group-list (G-list) and has a unique ID (gid). 
4. Parallel FP-growth: *mapper*: Each mapper get a shard, but before processing it, it reads the G-list. It output one or more key-value pairs, where ech key is a group-id and its corresponding value is generated group-dependent transaction. 
    *Reducer*: when the mappers are done, for each group-id, the corresponding group-dependent transactions are made into a shard. For each shard the reducer instance builds a local fp-tree and growth its condition fp-trees recursively.

The PFP-growth algorithm gives the frequency of co-occurrence of a certain sets of items / domains in an operon.  

### The implementation

The main function that is used to run the frequent pattern mining: *runFrequencyPatternMining()*

This method has one big loop that does the same for the list of provided support values for which the user wants to run the frequent pattern mining. 

The first called method is *generateFPGrowthModel*. Which creates the input data. 

### Input data format

To run the frequent pattern mining algorithm, the data must be presented in a a JavaRDD of Strings, where all the items of one item set are in one string separated by a delimiter (white space in this case). Every item set can only contain unique items (so duplicate domains have to be removed). 

The *prepareDomainsForFPM()* goes through the operon hashmap and get the domain lists. The domain list is converted to a set to eliminate duplicate domains and the remaining domains are put into a string separated by white space. These strings are returned in the form of a list. 

["a b c", "a c d", ... , "a e f"] would be the list of strings that would be parallelized into a JavaRDD of Strings.

### FP growth model

The FP growth model is generated by running the FPgrowth object with an assigned number of partitioned and a minimum support value. This is fairly easy. The running of the FPGrowth results in an FPGrowthModel. From this model the FrequentItemSet<String> are taken for further analysis. 

```java
FPGrowth fpg = new FPGrowth()
        .setMinSupport(supportValue) // mimimum support for an itemset to be considered frequent
        .setNumPartitions(partitions); // number of partitions used to distribute the work
return fpg.run(domainData);
```

Every frequent item set contains the .javaItemSet(). This is set that represents that pattern. the set.frequency() gives the frequency with which the item set occurs.

### Processing of the frequent item set

The Frequent items sets are firstly processed by filtering to only retain the item sets of size 1 (containing only one domain), meaning just the domains that occur frequent. For these domains, the frequency of the domains is added to the domainFrequencyHashmap. by the *addFrequeciesToDomainFreq()* method. 

### Calculating Jaccards indexes 

To calculate the Jaccards indexes a [JaccardIndex object](###jaccard-index) is created. The *JaccardIndex.getJacccardIndices()* method returns a HashMap <domain ID, HashMap <samplesWith / allSamples, jaccard index>>. For the details of the calculations see [this section](###jaccard-index).

### Generating the output

Two forms of output are created. A .txt file with the single domain patterns and the Jaccards index, copy number, etc. and a bar chart with the support values of the single domain patterns. 

The .txt file is created by the method *writeFrequencyPatternOutput*. This method takes the support value of the current FPM run, the single domains RDD (loneDomains) and the hashmap containing the Jaccards indexes. This method takes the info from the domainFrequencyHashMap for every of the single domain patterns. 

Second, a bar chart is created when calling the *createSupportGraph()* method.  For all the single domain patterns (lone domains) the support values are extracted and put into an ArrayList of Tuple2<domainID, SupportValue). This arrayList is sorted on the support value from high to low values and than split into two lists, One containing all domainIDs and one containing the support values. These are given to a FPMSupportGraphobject that creates a bar chart. 

All patterns discovered by the FPM can also be written to an output file by the *writeRawPatternsOutput()* when the command option saveRawFPMFiles is enabled. 

### Trait Detection

The [TraitDetector](### traitdetector) is dedicated to cleaning up the raw FPM output to find the largest patterns in the output that are still supported above the support value threshold. For example the pattern 'a b' and 'a b c' can have both a frequency of 10. This means that 'a b' never occurs without 'c'. Therefore, the trait detection would only retain 'a b c' as a pattern with a frequency of 10. This part of the code is not functioning properly yet. It gives out of memory errors very quickly. See the [TraitDetector section](###traitdetector) for details on improvement.

### Association rules - details

An association rule looks like:  [a,b] (antecedent) -> [c] (consequent) 0.8 (confidence). Meaning that if a and b are present, in 80% of the cases, c is also present. 

The association rules can be gathered from the FPM output. This is only done when there is some minimum amount of operons present (default = 10). With a very small amount of operons, there can be million to billions of association rules as every permutation of the present domains can yield an association rule. 

The *GenerateAssocationRules()* method uses the Frequent Item Set from the FPGrowth model to crate the association rules. This is a very easy application. From all the association rules, only the rules that have the seed domains as a consequent 

```Java
private JavaRDD<AssociationRules.Rule<String>> generateAssociationRules(JavaRDD<FPGrowth.FreqItemset<String>> freqItemsetJavaRDD) {
        this.associationRules = new ssociationRules().setMinConfidence(commandOptions.minCon);
        return associationRules.run(freqItemsetJavaRDD);
    }

```



## LDA clustering - details

Latent Dirichlet allocation is a statistical topic model that tries to uncover hidden topics (a.k.a. biological function) in the list of operons

The whole process of the LDA clustering is divided over three classes. A parent *LDAClustering* with two child classes, namely *LDAClusteringFrequentPatternMining* and *LDAClusteringOperons*. The parent contains the overlapping methods between the two children. The child classes take care of the two types of [vocabulary selection](###vocabulary-selection). *LDAClustering* is again a child of the *MachineLearningParallel* class. 

### Input format

The LDA algorithm a list of counts of the words in every document in a JavaRDD of String, where the numbers are separated by white spaces. 

| Operon        | LDA input   |
| ------------- | ----------- |
|               | a b c d e f |
| a b c b d d e | 1 2 1 2 1 0 |
| b c c c f d   | 0 1 3 1 0 1 |

The list that is parallelized into an RDD for the LDA would be: ["1 2 1 2 1 0", "  0 1 3 1 0 1"]. To later translate the weights assigned to the domains back, the order of the domains as they are counted needs to be memorized somewhere else. 

The creation of the LDA model is done by the *generateLDAModel()*. This methods needs the number of topics that you expect and the input count list. The JavaRDD<String> is made into an RDD of [dense Vectors](https://spark.apache.org/docs/1.1.0/mllib-data-types.html), this is just a different form of a double array.  This JavaRDD<Vector> is made into a JavaPairRDD by having an index value as the key and the vector as the value. To generate the LDA model:

```java
JavaPairRDD<Long, Vector> corpusDomain =
                JavaPairRDD.fromJavaRDD(parsedDomainData.zipWithIndex().map(Tuple2::swap));
corpusDomain.cache();
LDAModel ldamodel = LDA().setK(k).run(corpusDomain) // k is the number of topics
```

### Vocabulary selection

To boost the performance of the LDA, a selection of the domains is made to limit the the vocabulary. The aim is to remove domains from the analysis that will not have any explanatory or discriminative power anyway. Topics are described by the distribution of weights over the domains. The total weights sum to 1. So, having less explanatory domains allows for the weights to be distributed in a more meaningful way. 

#### FPM selected domains

The *LDAClusteringFrequentPatternMining* class takes care of the vocabulary selection based on the frequent pattern mining and the processing of the results into a more human interpretable format. 

The code starts by making a JavaRDD<Operon> from the operon hashmap. On this JavaRDD a flatmap is performed with *getDomainsFromOperons* to get a list with all the domains present in the operons. This list is made into a HashSet to only retain the unique domains. Two ArrayLists with the same order of the domains are created to act as reference guides, namely the *domainsToClusterOn* and the *ReferenceDomainList*. 

Only for the domains present in the *domainsToClusterOn* list the frequencies are counted and made into the right format for the LDA algorithm by the *createLDAInput()* method.

#### Most explanatory domains

The class *LDAClusteringOperons* is used for this method of vocabulary selection. 

The idea behind this method of vocabulary selection is to use a preliminary LDA analysis to see which domains seem to have discriminative power between topics and use only these to redo the LDA analysis. So, first a LDA model object is created and the method *shrinkUsedVocab()* is used to provide a list of the domains that should be used as the vocabulary. 

The  *shrinkUsedVocab()* goes through all the topics as described in the LDA model and it takes the *vocabsize* heaviest domains of each topic. The vocabulary size is a parameter provided by the user. This method then returns a list of the domains. These domains are then used to go through the same process as for the FPM selection method to generate the LDA model. 

### Processing the LDA output

Two forms of output are generated to make the results suitable for interpretation. A Chart is created using the BarChardLDA class. Also a .txt file is created where the exact weights assigned to the topics are presented together with the functional description of the domains. 

## FPM network

This part of the code is not functional. There are quite some errors still in here. The idea is to create a network from the domain by connecting them using frequent pattern mining. Whenever two domains bidirectionally show up in their respective FPM output, a connection is made. This is supposed to help understand the biological functions in which a more generic domain could be involved. 

The three classes that have been involved in the set up are *FPMNetwork3*, *FPMNetworkEdge* and *FPMNetworkNode*. the *FPMNetwork3* is intended to guide the whole process of creating the network. The *createNetwork()* method first loads in the FPM results that belong to the seed domain. For all the domains that are frequent, the whole querying has to be performed for these domains and the FPM results for all these domains have to be gathered. 

All the FPM output files for all the domains that were present in the FPM of the seed domain. The next step would be to make a graph object from these domains and draw edges when there is a bidirectional hit in the FPM output of the domains, defined by a support value cut off. 

The main memory errors occur due the fact that for every domain, the whole input data needs to be queried. The error is thrown because of the garbage collection in Java. A lot of calculations need to be done and relatively little information is gathered from that. The error state that more than 98% of the power is going to the garbage collection, which eventually means that there are some memory leaks. One way to prevent this, is by running the application multiple times manually and to create the necessary FPM output files (the .txt files). There should be a check to see whether the necessary FPM output file already exists. When it does, there is not need to query the input data again and again. 

When this is fixed, the creation of the network should be possible using Spark GraphX, but this is still mainly unexplored territory. 

The most important things for the *FPMNetworkNode* and *FPMNetworkEdge* are the IDs that were necessary to make a graph. 

## Supporting classes

These are supporting classes that are mainly used to hold information and some are used to make specific calculations. Details that are deemed to be quite obvious are left out of the documentation

### Gene

The Gene object is Serializable to be able to serialize them in the hashmaps and to be able to use these objects in *.map()* and *.flatMap()* methods of the RDD. The [serialVersionUID](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/Serializable.html) of this object is used to make sure that the serializing and deserializing of the objects does not throw an *InvalidClassException*. the serialVersionUID can be automatically calculated, but this can be sensitive to the specific compilers that are used. By explicitly stating these, the exception can be prevented. 

The field strand holds the GBOL IRI for the strand location throughout this application. 

### Region

The region is also Serializable. See the [Gene section](###gene) for info on the serialVersionUID field. The regions are used to save the raw query results with all the genes that fall within the range as provided by the user. No attempts have been made to gather operon like structures from the list of genes yet. 

The initial gene of a region is the gene that contained the seed domain and around which the other genes have been extracted. 

### RegionExtractor

The main method in the RegionExtractor class is *extractOperonsFromRawRegions()*. There are two paths this method can take. Both for unidirectional and for bidirectional the principle getting the stretch of genes that contain the seed gene without any (forbidden) interruptions of genes on the opposite strand is the same. 

#### Extract unidirectional

An operon is created. Loop through all the genes in the regions. The code keeps track on whether the seed gene has already been found. Genes are collected that lie on the same strand as the seed. When a gene on the opposite strand is encountered, but the seed gene is **not** yet found, the genes will not belong to the operon and are discarded. When a gene on the opposite strand than the seed gene is encountered **and** the seed gene is already collected, the stretch of genes up to that point are kept as possible parts of the operon and the code stops appending new genes. 

#### Extract bidirectional

Again an operon is created and the code loops through the genes. Different adding and removing conditions are applied for when the seed gene is located on the forward or reverse gene. First a reverse part needs to added to the operon and next the forward part. Genes are constantly added and removed depending on switches from the genes of lying on the reverse and forward strand. 

#### Merge overlapping operons

It is possible that one gene contains the seed domain multiple times, or that more than one gene in the same operon structure contain the seed domain. To prevent the same operon being present in the analysis more than once, the method *mergeOverlappingOperons* from the region extractor goes through the all the operons in the operon hahsmap and compares all to all. 

Two operons checked to overlap if there are four conditions met:

1. It is not the exact same operon. The initial gene is not the same. If it would be the case that the seed domain was present more than once in the seed gene, the operon would still only be present in the operon hashmap once as the initial gene is the key used in the operon hashmap.
2. The two operons lie on the same contig
3. The two operons are found in the same sample
4. Both of the operons are not yet in the list to be removed. It is possible that the comparison has already been made into the other direction and it has been found that they can be merged, the operons are not again compared. **Note that the comparison of the operons is repeated if the operons should not be merged. This could be improved to save some computation time.**

The start and end position of the two operons are gathered and it is checked whether the operons at least partly overlap. If they overlap, both original operons are added to a to remove list and a new operon is create. To this new operon, all genes are added that are part of one of the two operons. Next, the genes from the second operon are added if they are not yet there. 

The original operons are removed from the hashset and the new operons are added after all operons have been evaluated. The genes in the operons are in later steps ordered again, as the merging of the operons can disrupt the order. 

### Operon

The Operon class holds the genes that are present in the operon, a list with all the domains that are present and methods to enforce the gap threshold that are provided by the command options. Operons are serializable and have a serialVersionUID field (see [Gene](###gene)). When going from a region to an operon, all the genes are put into the operon and next the necessary methods from the operon object are called to enforce the gap thresholds that make the difference between the regions and the operons. 

There are two main gap enforcing methods depending on whether bidirectional operons are allowed. Before these gap enforcing methods are applied, the regions have already been cleaned up. When unidirectional, the operon only contains genes that are on the same strand as the seed gene without any interruptions of genes on the opposite strands. When bidirectional, it contains a forward and reverse part without any interruptions. 

#### Unidirectional operons

The criteria for an operon are that all genes need to be on one strand and that the gap between two consecutive genes is not bigger than the *geneGap*, that is defined in the command line options.   

To enforce the gap threshold between genes in a unidirectional operon the *enforceMaximumGapThresholdUni()* method will be called. This method first finds the initial gene of the operons and the index of that gene and a HashSet is created that will keep track of all the genes that need to be removed from the gene list. 

From the seed gene, the gene gaps are evaluated both up and downstream from the initial seed gene. The gaps between two consecutive genes are evaluated. When one gap is found that exceeds the threshold, all other genes that follow after the break are also put into the *toRemove* HashSet. The last step is the removal of these genes from the operons. 

#### Bidirectional operons

The criteria for a bidirectional operon are that there is a forward and revere part allowed with a gap with a size within a range between a user defined minimum and maximum value. The gap between genes on the same strand is not allowed to exceed the *geneGap* threshold. 

The method *enforceMaximumGapThresholdBi()* is called for this process. First, the seed gene is identified using the *findInitialGeneInOperon()* method. This method loops through the genes in the operon and returns the one that matches the InitialGene field of the operon. 

Next, the method *enforceBidirectional()* is called. This method returns a boolean whether there is actually a bidirectional operon (meaning that it checks whether the operon does contain a forward and reverse part that meet the criteria). *EnforceBidirectional()* checks the whether the operon holds to the criteria for the bidirectional gap. 

1. Check on which strand the seed gene lies
2. Check whether there are also genes that lie on the opposite strand. If so,
3. Find the two genes where the strand switches from reverse to forward. 
4. Check the size of this gap. If this gap is not within the threshold range.
5. Remove the genes that lie on the opposite strand as the seed gene. 

If the operon is found to be bidirectional, the operon is split into the forward and reverse. First, the strand that contains the seed gene is checked on whether it has gaps that exceed the *geneGap* threshold and the appropriate genes are removed. Next, it is checked whether the enforcing of the gene gaps in the seed gene part has already removed the possibility of having the genes on the other strand as part of the operon. If yes, the part of the other strand is removed from the operon. If no, the methods *shrinkForwardGenes* or *shrinkReverseGenes* are respectively called. These methods enforce the gene gap on the other strand. It start enforcing the gaps from the side that is attached to operon part that contains the seed gene. 

### Jaccard Index

To calculate the Jaccard indexes for the, a JaccardIndex object can be created and the constructor calls the method *calculateJaccardIndices()* providing it with the domain frequency hashmap.

A flatmap is called on the JavaRDD that contains the frequent itemsets, which is filtered to only contain the item sets of size 1. For these domains, it looks up in the domainFrequencyHashMap how often the domain occurs in the samples that also contain the seed domain (*otherTotalSampelsWith*) and how often it occurs when all samples are considered (*otherTotalCountAllSamples*). Here the *other* significes that we are not talking about the seed domain around which the operons are extracted.  

Next, it is calculated how often the domain occurs in the operons. This frequency is then divided by the *otherTotal...* counts. 

For every domain, a Tuple3 is returned containing the domain ID, the jaccard index when the domain frequencies of only samples with the seed domain are taken into account, the jaccard index when the domain count of all samples is taken into account no matter whether it contains the seed domain or not.  These tuples are parsed and put into a HashMap, named the *jaccardIndices*. 

## Queries

### GenesAroundDomain.sparql

This SPARQL query finds a gene (seed) which contains the domain of interest and returns all genes that lie within a certain range of the seed (including the seed itself). This query uses the GBOL prefix: http://gbol.life/0.1/.

1. The query defines a sample and a contig
2. On this contig there is a seed gene which codes for a protein that contains the protein domain of interest
   - From this seed gene, also the begin and end position and the strand.
3. On the same contig is an other gene (gene2), which has a begin position in a certain range within the begin position of the seed gene.
4. Both these genes have the same prediction provenance. 
5. All the resulting gene2's are then ordered on their begin position. 

#### In- and output variables

| Input variable | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| %1$s           | ?db. The database from which the domain IDs will be gathered, like Pfam |
| %2$s           | ?accession. The domain ID around which the region will be established. This should be an identifier from from the database provide at %1$s |
| %3$s           | The search width from which genes will be gathered around the domain of interest |
| %4$s           | ?provOrigin. The provenance of the gene prediction in the genome. |

| Output Variable | Description                                              |
| --------------- | -------------------------------------------------------- |
| ?seed           | A Gene instance, which has the domain of interest        |
| ?gene2          | A Gene instance, that lies within the range around ?seed |
| ?strand         | The strand on which ?seed lies (forward or reverse)      |
| ?strand2        | The strand on which ?gene2 lies (forward or reverse)     |
| ?beginpos       | The begin position of ?seed                              |
| ?beginpos2      | The begin position of ?gene2                             |
| ?endpos         | The end position of ?seed                                |
| ?endpos2        | The end position of ?gene2                               |
| ?sample         | The sample identifier where both genes are found         |
| ?contig         | The contig identifier on which both genes are found      |

### ProteinDomainsInGene.sparql

This query retrieves all the protein domains that are present in a specified gene. 

| Input variables | Description                                                  |
| --------------- | ------------------------------------------------------------ |
| %1$s            | Gene ID for the gene of which the domain IDs need to be retrieved |
| %2$s            | The data base from which the domain IDs will be gathered     |

| Output variables | Description                                                  |
| ---------------- | ------------------------------------------------------------ |
| ?PfamID          | The database identifier. This can be the Pfam ID but it does not have to be. It corresponds with the data base provided with input variable %2$s. |
| ?beginDomain     | The begin position of the domain specified by defined by the ?PfamID variable |



### QueryAllDomainInfoForGenome.sparql

This query retrieves all the domains that are present in a genome file, the organism to which the genome belongs, the description of the domain function and how often this domain occurs. It is assured that all the annotation have the same provenance. 

| **Input variables** | Description                                                  |
| ------------------- | ------------------------------------------------------------ |
| %1$s                | The data base from which the domain IDs will be gathered. When not database is provided at the command options, Pfam is the default |
| %2$s                | Provenance of the structural and functional annotation. This ensure that gathered data has the same origin to ensure a fairer comparison and to prevent biases. |

| **Output variables** | **Description**                                              |
| -------------------- | ------------------------------------------------------------ |
| ?domainID            | The identifier of the protein domain                         |
| ?organismName        | The name of the organisms that is coupled to the sample / genome |
| ?domainDescription   | The functional description coupled to the identifier of the protein domain |
| ?domainCount         | The count of how often this domain occurs in the complete sample / genome |



## Dependencies

Java 1.8 is required and forced in the Gradle build

```
apply plugin: 'java' 
sourceCompatibility = 1.8 
targetCompatibility = 1.8 
```

Dependencies are managed using Gradle.

Apache Spark dependencies run on Scala version 2.11 versions 2.4.4:

org.apache.spark spark-core_2.11, version 2.4.4 https://mvnrepository.com/artifact/org.apache.spark/spark-core

org.apache.spark spark-mllib_2.11, version 2.4.4 https://mvnrepository.com/artifact/org.apache.spark/spark-mllib

org.apache.spark spark-graphx-2.11, version 2.4.4 https://mvnrepository.com/artifact/org.apache.spark/spark-graphx

org.apache.spark spark-sql_2.11, version 2.4.4 https://mvnrepository.com/artifact/org.apache.spark/spark-sql

Other dependencies are:

log4j, version 1.2.17 

jfreechart, version 1.0.13 https://mvnrepository.com/artifact/jfree/jfreechart/1.0.13

jcommander, version 1.78 https://mvnrepository.com/artifact/com.beust/jcommander

hdt-jena, version 2.0 https://mvnrepository.com/artifact/org.rdfhdt/hdt-jena

```bash
# Clone the git hdt repository and to install version 2.0
ls ~/.m2/repository/org/rdfhdt/hdt-jena/ 
# Skip tests as they fail but code seem to work 
mvn -DskipTests=true install 
```

```java
// All dependencies
dependencies {

    // Use JUnit test framework
    testImplementation 'junit:junit:4.12'

    // Logger
    compile group: 'log4j', name: 'log4j', version:'1.2.17'

    // https://mvnrepository.com/artifact/jfree/jfreechart/1.0.13
    compile group: 'jfree', name: 'jfreechart', version: '1.0.13'

    // https://mvnrepository.com/artifact/com.beust/jcommander
    compile group: 'com.beust', name: 'jcommander', version: '1.78'

    // https://mvnrepository.com/artifact/org.rdfhdt/hdt-jena
    compile group: 'org.rdfhdt', name: 'hdt-jena', version: '2.0'

    // https://mvnrepository.com/artifact/org.apache.spark/spark-core
    compile group: 'org.apache.spark', name: 'spark-core_2.11', version: '2.4.4'

    // https://mvnrepository.com/artifact/org.apache.spark/spark-mllib
    compile group: 'org.apache.spark', name: 'spark-mllib_2.11', version: '2.4.4'

    // https://mvnrepository.com/artifact/org.apache.spark/spark-graphx
    compile group: 'org.apache.spark', name: 'spark-graphx_2.11', version: '2.4.4'

    // https://mvnrepository.com/artifact/org.apache.spark/spark-sql
    compile group: 'org.apache.spark', name: 'spark-sql_2.11', version:'2.4.4'

    // https://mvnrepository.com/artifact/graphframes/graphframes
    compile group: 'graphframes', name: 'graphframes', version: '0.7.0-spark2.4-s_2.11'

}
```

# iRODS stuff -> Did I do anything with this?

irods_user_name - ...
irods_zone_name - tempZone
irods_port - 1247
irods_host - wurk-icat.irods.surfsara.nl