# Applied algorithms

- [ ] Update the frequent pattern mining
- [ ] Update the LDA
- [ ] Update the Association rules
- [ ] Update the LDA
- [ ] Update the Jaccard indexes 

## FreqPatterns (Inherits from MachineLearning)

This class contains all code concerning Frequent Pattern Mining and Association Rules

These algorithms do not consider the order of domains or repeats of domains in an operon.

The minimum support of a pattern becomes very important when a domain is relatively rare. If a domain occurs very infrequent, the FP growht object starts taking up to much memory. Even if a pattern is only found once, 0.2 might be enough support for it to be counted as frequent enough. This causes the FP growth to allow patterns that occur only once. This kind of explodes on your memory. 



### Frequent mining patterns

Association rule learning algorithms usually do not consider the order of items / words in the data.  

Spark.mllib provides a parallel implementation of FP-growth, a popular algorithm to mining frequent item-sets.  

The FP-growth algorithm is described in the following paper: https://link.springer.com/article/10.1023/B:DAMI.0000005258.31418.83

The parallel implementation as implemented in spark.mllib is described in paper the paper: PFP: Parallel FP-Growth for Query Recommendation.  



### FP-growth in very short

It is used for discovering frequently co-occurrent items (or protein domain is an operon structure).
 It is a divide and conquer based algorithm. 
 Time complexity is at least polynomial.  

The steps:
 \1. Calculate item frequencies and identify frequent items. Sort these by descending order (most frequent items first). 
 \2. In the second scan, the database is compressed into an FP-tree (prefix tree: https://en.wikipedia.org/wiki/Suffix_tree)
 \3. The FP-growth starts mining the FP-tree for each item that has a support above a defined threshold. If an item appears 3 out of 5, it has a support of 3/5=0.6. Mining is performed recursively.  

### Parallel FP-growth – the improved version

Near linear time complexity.  -> This is the algorithm that is actually implemented

\1. Sharding: The database is divided into successive parts and stored on different ‘computers’.
 \2. Parallel counting: count the support values of all items that appear in DB. This implicitly gets all unique items in the database (I). This is stored in an F-list. 
 \3. Grouping items: Dividing all items (I) into Q groups. The list of groups is called the group-list (G-list) and has a unique ID (gid). 
 \4. Parallel FP-growth: *mapper*: Each mapper get a shard, but before processing it, it reads the G-list. It output one or more key-value pairs, where ech key is a group-id and its corresponding value is generated group-dependent transaction. 
 *Reducer*: when the mappers are done, for each group-id, the corresponding group-dependent transactions are made into a shard. For each shard the reducer instance builds a local fp-tree and growth its condition fp-trees recursively.

The PFP-growth algorithm gives the frequency of co-occurrence of a certain sets of items / domains in an operon.  

### Association rules

An association rule learning algorithm: https://en.wikipedia.org/wiki/Association_rule_learning

From the FP growth model association rules can be generated, like:

[t, s, y] => [x], 1.0

Meaning that if you encounter t, s and y you will with a confidence of 1 [0:1] find x. The confidence gives the number of times that the if-then statements are found to be true. (if we see t, s, and y, then we find x)

Can be used to see which domains predict the presence of x, which can be a DUF.  

https://searchbusinessanalytics.techtarget.com/definition/association-rules-in-data-mining

Implementation from mllib only gives the rules that have a single item/domain as a consequence. They have not implemented to see how often a and b mean that x and y will be present  

[a,b] => [x,y].

Could still be interesting to see though. > could be extracted from the rules from this algorithm  or use other tool / algorithm for finding these rules in the data.  

### Input data format

For the frequent pattern mining algorithm the data must be in the format of a JavaRDD<List<String>>. Every region / operon must be converted to a string where the domainIDs are separated by an empty space. This string may only contain unique domainIDs. Duplicate domains are not allowed. When all operons are converted to strings are placed in one list, the code below makes the javaRDD. 

```java
// prepareDomainsForFPM() creates the List of String where every operon is a String
List<String> domainList = prepareDomainsForFPM(regionExtractor);
JavaRDD<String> dataDomain = this.sc.parallelize(domainList); // sc = java spark context. 
JavaRDD<List<String>> domainData = dataDomain.map(line -> Arrays.asList(line.split(" ")));
```



### Trait finding

Possible traits are extracted from the patterns that are frequently found. In essence, this is a clean up of the found patterns by removing the patterns that are contained in larger patterns. To illustrate, an example:

The pattern [a,b] occur a 100 times. [a,b,c] and [a,b,d] both occur 40 times. This means that the pattern [a,b] on its own only occurs 20 times, 80 times it is counted as part of bigger pattern. Only if the pattern with a 'true count' of 20 is still above the support value, it will be retained as a frequent pattern. 

**Note**: it is possible that an out of memory error occurs during the processing of the patterns to find the traits. This can happen when the domain of interest occurs infrequent in the genomes or when the support value is very low. In both cases the error occurs because there are to many patterns found above the support value to be processed. Increasing the support value might solve this issue. 

## LDA (Inherits from MachineLearning)

### The basic principles

Latent Dirichlet allocation (https://en.wikipedia.org/wiki/Latent_Dirichlet_allocation). It is an example of a **topic** **model**: Statistical model for discovering abstract topics that occur in a collection of documents. So, it might look at domains that represent a topic in all regions found in all the genomes. "a model that allows sets of observations to be explained by [unobserved](https://en.wikipedia.org/wiki/Latent_variable) groups that explain why some parts of the data are similar." ~ wikipedia - Latent Direchlet Allocation. 

It is an unsupervised clustering algorithm.

LDA describes each document by a distribution of topics and each topics be be described by a distribution of words. 

Collection of documents == the domain list from an operon surrounding the domain of interest.
Topic might domains that are associated in function. Topics are clusters of similar words

In LDA each document is viewed as a mixture of various topics and each document is considered to have a set of topics that are assigned via the LDA. LDA assumes that the topic distribution to have a sparse Dirichlet prior. Dirichlet = type of distribution. prior = prior probability distribution ~ what one beliefs about the distribution before some evidence is taken into account? The consequence of this is that it encodes the intuition that documents cover only a small set of topics and that topics use only a small set of words frequently

Topics are identified on the basis of automatic detection of the likelihood of term co-occurrence. 

<u>Each document is assumed to be characterized by a particular set of topics.</u> It is possible that the domain of interest is involved in different functions. So, there might be different 'topics' or traits surrounding the domain of interest. 

**Do I expect that if a domain of interest occurs multiple times in a genome, that that would include multiple topics / functions?** might be true for some domains, but I can imagine that there are also domains that will always be associated with the same words / other domains. This will become clear from the weight values. 

### Statistical background

LDA is a generative statical model. LDA is called latent, because the underlying topics are hidden. In advance, the topics are not known. 

If you have an observable variable X and a target, a generative model is a model of the joint probability distribution (the probability of finding two ore more 'events' or variables together. It gives the intersect of events).  

The weight of the words for each topics are real numbers on an interval from (0,1) and sum to 1. 

### The implemented algorithm

https://spark.apache.org/docs/latest/mllib-clustering.html#latent-dirichlet-allocation-lda

- Topics correspond to cluster centers, and documents correspond to examples (rows) in a dataset.
- Topics and documents both exist in a feature space, where feature vectors are vectors of word counts (bag of words).
- Rather than estimating a clustering using a traditional distance, LDA uses a function based on a statistical model of how text documents are generated.

LDA takes in a collection of documents as vectors of word counts and parameters:

### Input format

Input for the LDA model needs to be a counting of the words present in each document. A short guide to how this is done.

First created a method to extract the vocabulary (which domains are present) --> extractDomainVocabulary()

For all of these words, count how often the occur in each document / sample.  --> countDomainFreqOperons. A count is kept for the frequency of a domain in all samples together and for every sample separate. 

A selection criteria is used to select which words will be used for the actual clustering. In this code, this is just a specified number of most frequent domain. **This will be updated / changed in the future.**

For every document / sample, create a string of the counts separated by spaces. For every document, the count for the same word should be located at the same index. You yourself need to keep track of which words are counted at which indexes. 

From these strings create a List. The following code is used to than generate the LDA model. 

countString is of type List<String>

sc = Java Spark Context.

```java
JavaRDD<String> domainData = this.sc.parallelize(countString);
JavaRDD<Vector> parsedDomainData = domainData.map(s -> {    String[] sarray = s.trim().split(" ");
double[] values = new double[sarray.length];    for (int i = 0; i < sarray.length; i++) {        values[i] = Double.parseDouble(sarray[i]);    }  ``  return Vectors.dense(values);});
JavaPairRDD<Long, Vector> corpusDomain =       JavaPairRDD.fromJavaRDD(parsedDomainData.zipWithIndex().map(Tuple2::swap));
corpusDomain.cache();
LDAModel ldaModel = new LDA().setK(k).run(corpusDomain);
```

## Jaccard index





https://en.wikipedia.org/wiki/Jaccard_index



## Spark GraphX exploration

### Creating a GraphFrame





### GraphFrames



#### Connecting components

It is necessary to set up a spark session together with your Spark context. For this, a 





































